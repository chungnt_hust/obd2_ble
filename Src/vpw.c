#include "vpw.h"
#include "string.h"
#include "brutal_timing.h"

static char tmpStr[256];
uint8_t vpw_pu8TxBuf[12]={0x68,0x6A,0xF1,0x01,0x00};
uint8_t vpw_pu8TxPulseBuf[255];
uint16_t vpw_u16TxPulseCount;

//cac ham truyen
extern int u32CountBit,u32dem;
extern uint16_t u16PulseToSAE[255];
static uint8_t u8DataSend[12]={0x68,0x6A,0xF1,0x01,0x00};

static bool bVPW_InitedOK = false;

bool SAE_VPW_InitedOK()
{
    return bVPW_InitedOK;
}

void SAE_VPW_TxProcessTask(void)
{
    __HAL_TIM_SET_COMPARE(&TxTIMER,TIM_CHANNEL_2,0);//Dat gia tri dau tien khi xay ra ngat OutputCompare o 0
    u8DataSend[5]=j1850_crc(u8DataSend,5);
    CalPulseToSAEBus(u8DataSend,6);
    u32dem=0;
    __HAL_TIM_CLEAR_FLAG(&TxTIMER, TIM_SR_CC2OF);
    HAL_TIM_OC_Start_IT(&TxTIMER, TIM_CHANNEL_2);
}
static void CalPulseToSAEBus(uint8_t* pu8Data,uint16_t u16Leng)//do dai toi da cua ban tin la 12 bytes
{
    if (u16Leng>0&&u16Leng<=12)
    {
        u16PulseToSAE[0]=StartOF;
        u32CountBit=1;
        for (int i=0;i<u16Leng;i++)
        {
            CalPulseByte(pu8Data[i]);
        }
        u16PulseToSAE[u32CountBit]=EndOF;
        u32CountBit++;
    }
}
static void CalPulseByte(uint8_t u8Data)
{
    int a;
    uint8_t BitCheck;
    for (a=0;a<8;a++)
    {
        BitCheck=u8Data&(0x80>>a);
        u16PulseToSAE[u32CountBit]=BitToPulse(BitCheck,a);
        u32CountBit++;
    }
}
static uint8_t BitToPulse(uint8_t u8BitCheck, int index)
{
    if ((index%2)==0)//PASSIVE BIT
    {
        if (u8BitCheck==0) return SHORT;
        else return LONG;
    }
    if ((index%2)==1)//ACTIVE BIT
    {
        if (u8BitCheck==0) return LONG;
        else return SHORT;
    }
}
//cac ham nhan
uint16_t u16RxBitCount=0;
uint16_t* pu16RxBitPulse;
uint8_t* pu8RxBitValue;
uint16_t u16NumberOfBytes;
uint8_t* pu8VPWRxByteValue;
uint16_t vpw_u16RxPulseCount;
uint16_t vpw_u16RxByteCount;
void SAE_VPW_StopReceive(void)
{
		//u16RxBitCount = 0;
    HAL_TIM_IC_Stop_IT(&RxTIMER, TIM_CHANNEL_1);
    HAL_TIM_IC_Stop_IT(&RxTIMER, TIM_CHANNEL_2);
}
void SAE_VPW_StartReceive(void)
{
    //u16RxBitCount = 0;
    //__HAL_TIM_SetCounter(&RxTIMER,0);
	__HAL_TIM_CLEAR_FLAG(&RxTIMER, TIM_SR_CC1IF);
	__HAL_TIM_CLEAR_FLAG(&RxTIMER, TIM_SR_CC2IF);
    HAL_TIM_IC_Start_IT(&RxTIMER, TIM_CHANNEL_1);
    HAL_TIM_IC_Start_IT(&RxTIMER, TIM_CHANNEL_2);
}
uint16_t SAE_VPW_RxProcessTask(void)
{
    //DebugPutString("Semaphore OK\r\n");
    int indexSAERxTask = 0;
    sprintf(tmpStr,"\r\n VPW N bits count = %d\r\n",u16RxBitCount);
    DebugPutString(tmpStr);
		uint8_t u8Bit;
		int idx;
    for (idx=0;idx<u16RxBitCount;idx++)
    {
        if ((idx%2)==0) {
					u8Bit = CheckEvenBit(pu16RxBitPulse[idx]);
					if (u8Bit != 0xFF) 
						pu8RxBitValue[indexSAERxTask++]= u8Bit;
				}
        else {
					u8Bit = CheckOddBit(pu16RxBitPulse[idx]);
					if (u8Bit != 0xFF) {
						pu8RxBitValue[indexSAERxTask++]=u8Bit;
					}
				}
//				sprintf(tmpStr, "\r\n VPWM[%d] = %d, bit = %d", idx, pu16RxBitPulse[idx], u8Bit);
//				DebugPutString(tmpStr);
    }
    u16NumberOfBytes=(u16RxBitCount+1)/8;
    //memset (pu8VPWRxByteValue,0,12);
		sprintf(tmpStr, "\r\nVPW Received %d Bytes: \r\n", u16NumberOfBytes);
    DebugPutString(tmpStr);
    for (indexSAERxTask=0;indexSAERxTask<u16NumberOfBytes;indexSAERxTask++)
    {
        pu8VPWRxByteValue[indexSAERxTask] =	BitsToByte(&pu8RxBitValue[indexSAERxTask*8]);
        sprintf(tmpStr," 0x%02x", pu8VPWRxByteValue[indexSAERxTask]);
        DebugPutString(tmpStr);
    }
    return u16NumberOfBytes;
}
static uint8_t CheckEvenBit(uint16_t u16Pulse) // ACTIVE BIT
{
    if (MIN_SHORT<u16Pulse && u16Pulse<=MAX_SHORT) return 1;
    if (MIN_LONG<u16Pulse && u16Pulse<=MAX_LONG)	return 0;
		return 0xFF;
}
static uint8_t CheckOddBit(uint16_t u16Pulse)	// PASSIVE BIT
{
    if (MIN_SHORT<u16Pulse && u16Pulse<=MAX_SHORT) return 0;
    if (MIN_LONG<u16Pulse && u16Pulse<=MAX_LONG)	return  1;
		return 0xFF;
}
static uint8_t BitsToByte(uint8_t* pu8Value)
{
    uint8_t FinalValue=0;
    for (int i=0;i<8;i++)
    {
        FinalValue+=(pu8Value[i]<<(7-i));
    }
    return FinalValue;
}



void SAE_VPW_SetPIDMode(uint8_t mode, uint8_t pid)
{
    u8DataSend[SAE_VPW_MODE_IDX] = mode;
    u8DataSend[SAE_VPW_PID_IDX] = pid;
}

void SAE_VPW_Transmit(uint8_t mode, uint8_t pid, uint8_t u8PidBytes)
{
    SAE_VPW_SetPIDMode(mode, pid);
    __HAL_TIM_SET_COMPARE(&TxTIMER,TIM_CHANNEL_2,0);//Dat gia tri dau tien khi xay ra ngat OutputCompare o 0
    if (u8PidBytes > 1)
    {
        sprintf(tmpStr,"\r\n VMP unsupported number of bytes %d", u8PidBytes );
        return;
    }
    uint8_t u8Len = 4 + u8PidBytes;

    u8DataSend[u8Len]=j1850_crc(u8DataSend,u8Len);
    CalPulseToSAEBus(u8DataSend,u8Len+1);
    u32dem=0;
    __HAL_TIM_CLEAR_FLAG(&TxTIMER, TIM_SR_CC2OF);
    HAL_TIM_OC_Start_IT(&TxTIMER, TIM_CHANNEL_2);
}

////////////////////////////////// brutal VPW //////////////////////////////////////////////////

void brutalVpwInit(void){
	GPIO_InitTypeDef GPIO_InitStruct;
	
  /*Configure GPIO pins : PA6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;// GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

void brutalVpwDeinit(void){
	HAL_GPIO_DeInit(GPIOA,GPIO_PIN_6);
}

uint16_t vpw_calculate_pulse_with_sof(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf){
	pu8PulseBuf[0] = VPW_SOF;
	uint16_t ret = vpw_calculate_pulse(pu8Data,u16Leng,&pu8PulseBuf[1])+1;
	return ret;
}

uint16_t vpw_calculate_pulse(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf){
	for (int i=0; i<u16Leng; i++){
		vpw_calculate_pulse_byte(pu8Data[i],&pu8PulseBuf[i*8]);
	}
	return u16Leng*8;
}

void vpw_calculate_pulse_byte(uint8_t u8ByteData, uint8_t *pu8PulseBuf){
	int i; uint8_t BitCheck;
	for (i=0;i<8;i++){
		BitCheck = u8ByteData&(0x80>>i);
		vpw_bit_to_pulse(BitCheck,i,&pu8PulseBuf[i]);
	}
}

void vpw_bit_to_pulse(uint8_t u8BitCheck, int index, uint8_t* pu8PulseBuf) {
    if ((index%2)==0) { //PASSIVE BIT
        if (u8BitCheck==0){
					pu8PulseBuf[0]=VPW_SHORT_PULSE;
				} else {
					pu8PulseBuf[0]=VPW_LONG_PULSE;
				}
    }else if ((index%2)==1)  { //ACTIVE BIT
        if (u8BitCheck==0) {
					pu8PulseBuf[0]=VPW_LONG_PULSE;
				} else {
					pu8PulseBuf[0]=VPW_SHORT_PULSE;
				}
    }
}

VPW_SEND vpw_send(uint8_t* pu8PusleBuf, uint16_t u16Leng){
	int i=0;
	SAE_OUT_HIGH();
	while (1){
		__delay_us(pu8PusleBuf[i++]);
		if (HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_6)==GPIO_PIN_SET) {
			return VPW_SEND_NOK;
		}
		SAE_OUT_LOW();
		if(i>=u16Leng) break;
		__delay_us(pu8PusleBuf[i++]);
		if (HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_6)==GPIO_PIN_RESET) {
			return VPW_SEND_OVER_WRITE;
		}
		SAE_OUT_HIGH();
	}
	return VPW_SEND_OK;
}

VPW_SEND vpw_send_command(uint8_t u8Mode, uint8_t u8PID){
	memset(vpw_pu8TxPulseBuf,0x00,255);
	vpw_u16TxPulseCount = 0 ;
	vpw_pu8TxBuf[3] = u8Mode;
	vpw_pu8TxBuf[4] = u8PID;
	vpw_pu8TxBuf[5] = j1850_crc(vpw_pu8TxBuf,5);
	vpw_u16TxPulseCount = vpw_calculate_pulse_with_sof(vpw_pu8TxBuf,6,vpw_pu8TxPulseBuf);
//	for (int i=0; i<vpw_u16TxPulseCount; i++) {
//		sprintf(tmpStr,"\r\nVPW pulse number %u = %u", i, vpw_pu8TxPulseBuf[i]);
//		DebugPutString(tmpStr);
//	}
	return vpw_send(vpw_pu8TxPulseBuf,vpw_u16TxPulseCount);
}

VPW_SEND SAE_VPW_brutal_transmit(uint8_t mode, uint8_t pid, uint8_t u8PidBytes){
    if (u8PidBytes > 1) {
        return VPW_SEND_NOK ;
    }
    uint8_t u8Len = 4 + u8PidBytes;
		vpw_pu8TxBuf[3] = mode;
		vpw_pu8TxBuf[4] = pid;
    vpw_pu8TxBuf[u8Len]=j1850_crc(vpw_pu8TxBuf,u8Len);
    vpw_u16TxPulseCount = vpw_calculate_pulse_with_sof(vpw_pu8TxBuf,u8Len+1,vpw_pu8TxPulseBuf);
		taskENTER_CRITICAL();
    return vpw_send(vpw_pu8TxPulseBuf,vpw_u16TxPulseCount);
}

VPW_RECEIVE vpw_read_pulse(void){
	// change vpw_pu8RxPulseBuf to pu16RxBitPulse
	int count = 0;
	memset(pu16RxBitPulse,0x00,1700);
	__delay_us(20);
	while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_6)==GPIO_PIN_SET){
		__delay_us(1);
		if(count++>4000){
			return VPW_RECEIVE_NOTHING;
		}
	}
	vpw_u16RxPulseCount = 0;
	while (1) {
		count = 0;
		while (HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_6)==GPIO_PIN_RESET){
			__delay_us(1);
			if(count++>2000){
				return VPW_RECEIVE_NOK;
			}
		}
		//vpw_pu8RxPulseBuf[vpw_u16RxPulseCount++] = count;
		pu16RxBitPulse[vpw_u16RxPulseCount++] = count;
		count = 0;
		while (HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_6)==GPIO_PIN_SET) {
			__delay_us(1);
			if(count++>2000){
				return VPW_RECEIVE_OK;
			}
		}
		//vpw_pu8RxPulseBuf[vpw_u16RxPulseCount++] = count;
		pu16RxBitPulse[vpw_u16RxPulseCount++] = count;
	}
}
void vpw_read_byte(void){
	// change vpw_pu8RxPulseBuf to pu16RxBitPulse & vpw_pu8RxByte to pu8VPWRxByteValue
	int i=1;
	vpw_u16RxByteCount = 0;
	memset(pu8VPWRxByteValue,0x00,255);
	if(80<pu16RxBitPulse [0] && pu16RxBitPulse [0]<120 ){
		while (i<vpw_u16RxPulseCount) {
			pu8VPWRxByteValue[vpw_u16RxByteCount++] = vpw_pulse_to_byte(&pu16RxBitPulse[i]);
			i+=8;
			if(pu16RxBitPulse [i]>80){
				if (80<pu16RxBitPulse [i+1] && pu16RxBitPulse [i+1]<120){
					i+=2;
				} else {
					return;
				}
			}
		}
	}
	
}
void vpw_read_inform_to_debug(void){
	sprintf(tmpStr,"\r\nVPW read %u byte\r\n",vpw_u16RxByteCount);
	DebugPutString(tmpStr);
	for (int i=0; i< vpw_u16RxByteCount; i++){
		sprintf(tmpStr,"0x%02x\t",pu8VPWRxByteValue[i]);
		DebugPutString(tmpStr);
	}
}
uint8_t vpw_pulse_to_byte(uint16_t* pu8PulseBuf){
	int i=0;
	uint8_t byteValue = 0;
	while(i<8){
		if (50<pu8PulseBuf[i] && pu8PulseBuf[i]<=80){
			byteValue = (0x80>>i)|byteValue;
		} 
		i++;
		if (17<pu8PulseBuf[i] && pu8PulseBuf[i]<=45) {
			byteValue = (0x80>>i)|byteValue;
		}
		i++;
	}
	return byteValue;
}

uint16_t vpw_rx_byte_number(void){
	VPW_RECEIVE iRead = vpw_read_pulse();
	taskEXIT_CRITICAL();
	if( iRead == VPW_RECEIVE_OK ){
		vpw_read_byte();
		vpw_read_inform_to_debug();
		return vpw_u16RxByteCount;
	} else return 0;
}

