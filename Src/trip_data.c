#include "trip_data.h"
#include "debug.h"
#include "ble.h"

//#define TEST_EFLASH

uint16_t TripBuffer[TRIP_BUFFER_SIZE] = {0};

static char tmpStr[100];

uint8_t u8TripActive;
uint8_t	u16TripIndex;

void TRIP_Init(){
	EF_Init();
	u8TripActive = NOT_ACTIVE;
	u16TripIndex = 0;
	u32TripDistance = 0;
	u32TripStopTime = 0;
	
	uint8_t ret = EF_ReadVariable(TripBuffer, TRIP_BUFFER_SIZE);
	if(ret==EFLASH_OK) {// DebugPutString("Read variable ok\r\n");
		u8TripActive = TripBuffer[TRIP_ACTIVE];
		u16TripIndex = (TripBuffer[TRIP_INDEX]<<8) + TripBuffer[TRIP_INDEX+1];
		if(u8TripActive == ACTIVE){
			u32TripDistance = ((uint32_t)TripBuffer[TRIP_DISTANCE]<<24) + ((uint32_t)TripBuffer[TRIP_DISTANCE+1]<<16) 
												+ ((uint32_t)TripBuffer[TRIP_DISTANCE+2]<<8) + (uint32_t)TripBuffer[TRIP_DISTANCE+3];
			u32TripStopTime = ((uint32_t)TripBuffer[TRIP_WAIT_TIME]<<24) + ((uint32_t)TripBuffer[TRIP_WAIT_TIME+1]<<16) 
												+ ((uint32_t)TripBuffer[TRIP_WAIT_TIME+2]<<8) + (uint32_t)TripBuffer[TRIP_WAIT_TIME+3];
			u8Speed = TripBuffer[TRIP_SPEED];
			f_TripDistance = (float)u32TripDistance;
			f_TripStopTime = (float)u32TripStopTime;
			f_TripDistanceSaved = (float)u32TripDistance;
			
			DEBUG(tmpStr,"f_TripDistance %f\r\n",f_TripDistance);
			
			b_TripStart = true;
		}
	}
	else if (ret == EFLASH_LEN_ERROR)	DebugPutString("Error in LEN\r\n");
	else if (ret == EFLASH_EMPTY)	DebugPutString("Error Empty\r\n");
	else	DebugPutString("Error Unknow\r\n");
}

void TRIP_SaveData(void){
	DebugPutString("TRIP_SaveData\r\n");
#ifndef TEST_EFLASH
	TripBuffer[TRIP_DISTANCE] = u32TripDistance>>24;
	TripBuffer[TRIP_DISTANCE+1] = (u32TripDistance>>16) & 0x000000FF;
	TripBuffer[TRIP_DISTANCE+2] = (u32TripDistance>>8 ) & 0x000000FF;
	TripBuffer[TRIP_DISTANCE+3] = u32TripDistance & 0x000000FF;
	TripBuffer[TRIP_WAIT_TIME] = u32TripStopTime>>24;
	TripBuffer[TRIP_WAIT_TIME+1] = (u32TripStopTime>>16) & 0x000000FF;
	TripBuffer[TRIP_WAIT_TIME+2] = (u32TripStopTime>>8)  & 0x000000FF;
	TripBuffer[TRIP_WAIT_TIME+3] = u32TripStopTime & 0x000000FF;
	TripBuffer[TRIP_SPEED] = u8Speed;
	TripBuffer[TRIP_INDEX] = u16TripIndex>>8;
	TripBuffer[TRIP_INDEX+1] = u16TripIndex;
	TripBuffer[TRIP_ACTIVE] = u8TripActive;
	
	uint8_t ret = EF_WriteVariable(TripBuffer, TRIP_BUFFER_SIZE);
	if(ret==EFLASH_OK);// DebugPutString("Write variable ok\r\n");
	else if (ret == EFLASH_ERASE_ERROR) DebugPutString("Error in ERASE\r\n");
	else if (ret == EFLASH_LEN_ERROR) DebugPutString("Error in LEN\r\n");
	else if (ret == EFLASH_WRITE_ERROR) DebugPutString("Error in WRITE\r\n");
	else if (ret == EFLASH_EMPTY) DebugPutString("Error Empty\r\n");
	else DebugPutString("Error Unknow\r\n");
#else
	uint32_t tick = HAL_GetTick();
	
	TripBuffer[0] = (uint8_t)(tick>>24);
	TripBuffer[1] = (uint8_t)(tick>>16);
	TripBuffer[2] = (uint8_t)(tick>>8);
	TripBuffer[3] = (uint8_t)(tick);
	
	DEBUG(tmpStr,"\t\tTick write \t0x%08x\r\n", tick);
//	DEBUG(tmpStr,"Trip Buffer 0 0x%04x\r\n", TripBuffer[0]);
//	DEBUG(tmpStr,"Trip Buffer 1 0x%04x\r\n", TripBuffer[1]);
//	DEBUG(tmpStr,"Trip Buffer 2 0x%04x\r\n", TripBuffer[2]);
//	DEBUG(tmpStr,"Trip Buffer 3 0x%04x\r\n", TripBuffer[3]);
	
	uint8_t ret = EF_WriteVariable(TripBuffer, TRIP_BUFFER_SIZE);
	if(ret==EFLASH_OK);// DebugPutString("Write variable ok\r\n");
	else if (ret == EFLASH_ERASE_ERROR) DebugPutString("Error in ERASE\r\n");
	else if (ret == EFLASH_LEN_ERROR) DebugPutString("Error in LEN\r\n");
	else if (ret == EFLASH_WRITE_ERROR) DebugPutString("Error in WRITE\r\n");
	else if (ret == EFLASH_EMPTY) DebugPutString("Error Empty\r\n");
	else DebugPutString("Error Unknow\r\n");
#endif
}

void TRIP_ReadData(void){
#ifndef TEST_EFLASH
	
#else
	uint8_t ret = EF_ReadVariable(TripBuffer, TRIP_BUFFER_SIZE);
	if(ret==EFLASH_OK);// DebugPutString("Read variable ok\r\n");
	else if (ret == EFLASH_ERASE_ERROR) DebugPutString("Error in ERASE\r\n");
	else if (ret == EFLASH_LEN_ERROR) DebugPutString("Error in LEN\r\n");
	else if (ret == EFLASH_WRITE_ERROR) DebugPutString("Error in WRITE\r\n");
	else if (ret == EFLASH_EMPTY) DebugPutString("Error Empty\r\n");
	else DebugPutString("Error Unknow\r\n");
	
	uint32_t tick = ((uint32_t)TripBuffer[0]<<24) + ((uint32_t)TripBuffer[1]<<16) +((uint32_t)TripBuffer[2]<<8) + (uint32_t)TripBuffer[3];
	
	DEBUG(tmpStr,"\t\tTick read \t0x%08x\r\n\r\n", tick);
#endif
}



