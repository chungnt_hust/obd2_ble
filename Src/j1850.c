/**
 * @file j1850.c
 * @brief J1850 VPW & PWM for stm32f1 (odbi adapter)
 * @date May 04, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */
#include "j1850.h"
#include "vpw.h"
#include "pwm.h"
#include "j1850pwm.h"
#define J1850_PWM_OLD 0
extern osSemaphoreId g_ODB2_Semid;
extern osSemaphoreId g_BLESemid;
extern ODB2_PROTOCOL g_ODB2Protocol;
extern ODB2Message g_ODB2Msg;
// global variable 
SAE_BUS_STATUS CPU_STATUS = BUS_IDLING;
volatile SAE_TRANSMIT_FRAME_STATUS CHECK_EOF_BIT=SAE_TRANSMIT_STOP;
static J1850_PROTOCOL j1850_protocol = J1850_PROTOCOL_NONE;
// xu ly ngat truyen
int u32CountBit,u32dem;
uint16_t u16PulseToSAE[255];
uint16_t u16compare;
static char tmpStr[256];
// rx buffer
uint8_t j1850_RX_Pulse_Buf[1700];
//uint8_t j1850_Rx_Bits_Buf[1700];
uint8_t j1850_RX_Byte_Buf[255];
uint8_t* j1850_RX_Pulse = j1850_RX_Pulse_Buf; //[2048];
//uint8_t* j1850_Rx_Bits = j1850_Rx_Bits_Buf;//[2048];
uint8_t* j1850_RX_Buf = j1850_RX_Byte_Buf;//[512];// = NULL;

uint16_t u8j1850_Rx_Frame_Count = 0;
uint16_t u8j1850_Rx_Start_Byte[J1850_MAX_RX_FRAME_COUNT + 1];
uint16_t u8j1850_Rx_Stop_Byte[J1850_MAX_RX_FRAME_COUNT + 1];


void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance==TxTIMER.Instance)
    {
        //ham truyen VPW
        //		if (htim->Channel==HAL_TIM_ACTIVE_CHANNEL_2)
        //		{
        //			if (u32dem==(u32CountBit-1))
        //			{
        //				HAL_TIM_OC_Stop_IT(&TxTIMER,TIM_CHANNEL_2);
        //				CPU_STATUS=BUS_IDLING;
        //				VPWStartReceive();
        //				DebugPutString("Tx complete, Start Rx process\r\n");
        //				//osSemaphoreRelease(SAERxSemid);
        //			}
        //			if (u32dem<(u32CountBit-1))//u32CountBit-1 vi bit cuoi cung la bit EOF (PASSIVE bit) khong can dao
        //			{
        //				__HAL_TIM_SET_COMPARE(&TxTIMER,TIM_CHANNEL_2,u16PulseToSAE[u32dem++]);
        //				__HAL_TIM_SET_COUNTER(&TxTIMER,0);
        //			}
        //		}



        // ham truyen PWM
        if (htim->Channel==HAL_TIM_ACTIVE_CHANNEL_2)
        {
            if (j1850_protocol == J1850_PROTOCOL_VPW) {
                if (u32dem==(u32CountBit-1))
                {
                    HAL_TIM_OC_Stop_IT(htim,TIM_CHANNEL_2);
                    CPU_STATUS=BUS_IDLING;

                    SAE_VPW_StartReceive();
                    //DebugPutChar('*');
                    //DebugPutString("Tx complete, Start Rx process\r\n");
                }
                else if (u32dem<(u32CountBit-1))
                {
                    __HAL_TIM_SET_COMPARE(&TxTIMER,TIM_CHANNEL_2,u16PulseToSAE[u32dem++]);
                    __HAL_TIM_SET_COUNTER(&TxTIMER,0);
                    //DebugPutChar('&');
                }
            } else {
                j1850_pwm_output_compare_handler();
            }
        }




    }
}
// Xu ly ngat nhan
extern uint16_t u16RxBitCount;
extern uint16_t* pu16RxBitPulse;
extern volatile uint16_t u16PWMRxPulseCount;
extern uint16_t* pu16PWMRxBitPulse;
int C3=0, C4=0;
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
    static uint16_t u16ReadPulse;
    if (htim->Instance==RxTIMER.Instance)
    {
        // ngat nhan cua VPW
        if (htim->Channel==HAL_TIM_ACTIVE_CHANNEL_2)
        {
            u16ReadPulse = 0;//__HAL_TIM_GetCompare(htim,TIM_CHANNEL_2);
            //__HAL_TIM_SetCounter(htim,0);
            CHECK_EOF_BIT = SAE_TRANSMIT_CONTINUE;
            if (CPU_STATUS==BUS_IDLING)
            {
                //DebugPutChar('@');
                u16RxBitCount=0;
            }
            else if (CPU_STATUS==BUS_READING)
            {
                //DebugPutChar('#');
                pu16RxBitPulse[u16RxBitCount]= u16ReadPulse;
                u16RxBitCount++;
            }
            //            sprintf(tmpStr, "\r\nu16ReadPulse = %d", u16ReadPulse);
            //            DebugPutString(tmpStr);
        }
        if (htim->Channel==HAL_TIM_ACTIVE_CHANNEL_1)
        {
            u16ReadPulse = 0;//__HAL_TIM_GetCompare(htim,TIM_CHANNEL_1);
            //__HAL_TIM_SetCounter(htim,0);
            if (CPU_STATUS==BUS_IDLING)
            {
                //DebugPutChar('%');
                pu16RxBitPulse[u16RxBitCount]=u16ReadPulse;
                if (u16ReadPulse>MIN_SOF&&u16ReadPulse<=MAX_SOF)
                {
                    //DebugPutChar('^');
                    CPU_STATUS=BUS_READING;
                    u16RxBitCount++;
                }
            }
            else if (CPU_STATUS==BUS_READING)
            {
                //DebugPutChar('[');
                pu16RxBitPulse[u16RxBitCount]=u16ReadPulse;
                u16RxBitCount++;
                if ((u16RxBitCount%8)==1)
                {
                    //DebugPutChar(']');
                    CHECK_EOF_BIT = SAE_TRANSMIT_OVER;
                    //__HAL_TIM_SET_COUNTER(&EOFTIMER,240);
                    //HAL_TIM_Base_Start_IT(&EOFTIMER);
                }
            }
            //            sprintf(tmpStr, "\r\nu16ReadPulse = %d", u16ReadPulse);
            //            DebugPutString(tmpStr);
        }

        /*
        *
        *
        *
        *
        */
        //ngat nhan cua PWM
        // Rising Edge
        if (htim->Channel==HAL_TIM_ACTIVE_CHANNEL_3)
        {
            //            DebugPutChar('!');
            //            __HAL_TIM_SetCounter(htim,0);
            //            CHECK_EOF_BIT = SAE_TRANSMIT_CONTINUE;
            //            if (CPU_STATUS==BUS_IDLING)
            //            {
            //                DebugPutChar('@');
            //                u16PWMRxPulseCount=0;
            //            }
            //            else if (CPU_STATUS==BUS_READING)
            //            {
            //                DebugPutChar('#');
            //                pu16PWMRxBitPulse[u16PWMRxPulseCount]=__HAL_TIM_GetCompare(htim,TIM_CHANNEL_3);
            //                u16PWMRxPulseCount++;
            //            }
            //__HAL_TIM_SetCounter(htim,0);
            j1850_pwm_input_capture_rising_handler();
        }
        if (htim->Channel==HAL_TIM_ACTIVE_CHANNEL_4)
        {
            //            DebugPutChar('$');
            //            if (CPU_STATUS==BUS_IDLING)
            //            {
            //                DebugPutChar('%');
            //                pu16PWMRxBitPulse[u16PWMRxPulseCount]=__HAL_TIM_GetCompare(htim,TIM_CHANNEL_4);
            //                if (pu16PWMRxBitPulse[u16PWMRxPulseCount]>=MIN_TP7&&pu16RxBitPulse[u16PWMRxPulseCount]<=MAX_TP7)
            //                {
            //                    DebugPutChar('^');
            //                    CPU_STATUS=BUS_READING;
            //                    u16PWMRxPulseCount++;
            //                }
            //            }
            //            else if (CPU_STATUS==BUS_READING)
            //            {
            //                DebugPutChar('(');
            //                pu16PWMRxBitPulse[u16PWMRxPulseCount]=__HAL_TIM_GetCompare(htim,TIM_CHANNEL_4);
            //                u16PWMRxPulseCount++;
            //                if ((u16PWMRxPulseCount%16)==1)
            //                {
            //                    DebugPutChar(')');
            //                    CHECK_EOF_BIT = SAE_TRANSMIT_OVER;
            //									__HAL_TIM_SET_COUNTER(&EOFTIMER,MIN_TP3);
            //									HAL_TIM_Base_Start_IT(&EOFTIMER);
            //                }
            //            }
            //__HAL_TIM_SetCounter(htim,0);
            j1850_pwm_input_capture_falling_handler();
        }
    }
}
/*
*
*
*
*/
uint8_t j1850_crc(uint8_t *msg_buf, int8_t nbytes)
{
    uint8_t crc_reg=0xff,poly,byte_count,bit_count;
    uint8_t *byte_point;
    uint8_t bit_point;

    for (byte_count=0, byte_point=msg_buf; byte_count<nbytes; ++byte_count, ++byte_point)
    {
        for (bit_count=0, bit_point=0x80 ; bit_count<8; ++bit_count, bit_point>>=1)
        {
            if (bit_point & *byte_point)	// case for new bit = 1
            {
                if (crc_reg & 0x80)
                    poly=1;	// define the polynomial
                else
                    poly=0x1c;
                crc_reg= ( (crc_reg << 1) | 1) ^ poly;
            }
            else		// case for new bit = 0
            {
                poly=0;
                if (crc_reg & 0x80)
                    poly=0x1d;
                crc_reg= (crc_reg << 1) ^ poly;
            }
        }
    }
    return ~crc_reg;	// Return CRC
}

int j1850_init(void)	{
		brutalPwmInit();
    if (j1850_pwm_init()) {
        return J1850_PROTOCOL_PWM;
    } else {
			brutalPwmDeinit();
		}
		osDelay(2000);
		brutalVpwInit();
    if (j1850_vpw_init()) {
        return J1850_PROTOCOL_VPW;
    } else {
			brutalVpwDeinit();
		}
    return J1850_PROTOCOL_NONE;
}


int j1850_vpw_init(void) {
		VPW_SEND 			iSend = VPW_SEND_NOK;
		VPW_RECEIVE 	iRead = VPW_RECEIVE_NOK;
    DebugPutString("\r\nVPW init");
    ENABLE_VPW();
		osDelay(10);
    pu8VPWRxByteValue = j1850_RX_Byte_Buf;
    pu16RxBitPulse = (uint16_t*)j1850_RX_Pulse_Buf;
    j1850_protocol = J1850_PROTOCOL_VPW;
    CPU_STATUS=BUS_WRITING; 
		taskENTER_CRITICAL();
		iSend = vpw_send_command(0x01,0x00);
		if (iSend == VPW_SEND_OK) {
			iRead = vpw_read_pulse();
		}
		taskEXIT_CRITICAL();
		vpw_read_byte();
		vpw_read_inform_to_debug();
		if(iSend == VPW_SEND_OK){
			DebugPutString("\r\nVPW send ok");
		}
		if (iRead == VPW_RECEIVE_OK){
			DebugPutString("\r\nVPW receive ok");
			return true;
		}
		return false;
}

int j1850_pwm_init(void){
		int atemp =0;
		PWM_RECEIVE iRead = PWM_RECEIVE_NOK;
		PWM_SEND 		iSend = PWM_SEND_NOK;
    DebugPutString("\r\nPWM New init");
    ENABLE_PWM();
		HAL_Delay(2000);
    j1850_protocol = J1850_PROTOCOL_PWM;
		j1850_pwm_setup();
		taskENTER_CRITICAL();
		pwm_again: iSend = pwm_send_cmd(0x01,0x00);
		if (iSend == PWM_SEND_OK)	{
			iRead = pwm_read_pulse();
			taskEXIT_CRITICAL();
			pwm_read_byte();
		} else { 
			if(atemp++ == 10){
				taskEXIT_CRITICAL();
				DebugPutString("\r\nPWM send NOK");
				return false;
			} else { 
				__delay_us(1000);
				goto pwm_again;
			}
		}
		if(iSend == PWM_SEND_OK){
			DebugPutString("\r\nPWM send OK");
		}
		if(iRead == PWM_RECEIVE_OK) {
			DebugPutString("\r\nPWM receive OK");
			return true;
		}
		return false;
}

void j1850_check_recv_status()
{
    if (CHECK_EOF_BIT == SAE_TRANSMIT_OVER)
    {
        CPU_STATUS=BUS_IDLING;
        CHECK_EOF_BIT=SAE_TRANSMIT_STOP;
        //osSemaphoreRelease(SAERxSemid);
    }
}


/**
 * @brief ODB2_j1850_ProcessBleRequest
 *        - request ODB2 mode and pid in g_ODB2Msg
 *        - if received correct response
 *          + set correct response length data g_ODB2Msg.u8RespLen
 *          + copy response data to g_ODB2Msg.pu8Data
 *          + set g_ODB2Msg.u8Status b = ODB2_STATUS_PROCESS_END
 *
 * @return
 */
int ODB2_j1850_ProcessBleRequest()
{
    // TODO: process g_ODB2Msg
    uint8_t u8Mode = g_ODB2Msg.u8Mode;
    uint8_t u8Pid = g_ODB2Msg.u8PID;
    uint16_t u16RxBytes;
    uint8_t u8CRC;
    g_ODB2Msg.u8RespLen = 0;
    uint16_t u16Idx = 0;
    uint8_t u8FrameIdx = 1;
    uint8_t u8StartByte;
    uint8_t u8StopByte;
		int i;
    // Check g_ODB2Msg mode & pid to send via j1850
    if (u8Mode != 0x03 && u8Mode != 0x04) {
        //ODB2_j1850_Transmit(u8Mode, u8Pid, 1);
				u16RxBytes = OBD2_j1850_brutal_transmit(u8Mode, u8Pid, 1);
    } else {
        //ODB2_j1850_Transmit(u8Mode, u8Pid, 0);
				u16RxBytes = OBD2_j1850_brutal_transmit(u8Mode, u8Pid, 0);
    }
		//
		sprintf(tmpStr,"\r\nJ1850 read %u byte\r\n",u16RxBytes);
		DebugPutString(tmpStr);
		for (i=0; i< u16RxBytes; i++){
			sprintf(tmpStr,"0x%02x\t",j1850_RX_Buf[i]);
			DebugPutString(tmpStr);
		}
    //u16RxBytes = ODB2_j1850_RxProcessTask();
		//u16RxBytes = ODB2_j1850_brutal_RxProcessTask();
    if (u16RxBytes > 3) {
        // ====================Mode is NOT 03, 04 (number pid = 1)==========================
        if (u8Mode != 0x03 && u8Mode != 0x04) {
            // ====================VPW Process (Mode is NOT 03, 04)======================
            if (j1850_protocol == J1850_PROTOCOL_VPW) {
                // Check crc
                u8CRC = j1850_crc(j1850_RX_Buf, u16RxBytes - 1);
                if (u8CRC == j1850_RX_Buf[u16RxBytes-1]) {
                    // process correct rx buffer here
                    // check for pid support:
                    if (j1850_RX_Buf[4] == u8Pid && j1850_RX_Buf[3] != 0x7F) {
                        // 0x48 0x6b 0x10 0x41 0x0d 0x23 0x6a
                        g_ODB2Msg.u8RespLen = u16RxBytes - 4;// 3byte header + 1 crc
                        memcpy(g_ODB2Msg.pu8Data, &j1850_RX_Buf[3], g_ODB2Msg.u8RespLen);
                        g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
                    } else {
                        sprintf(tmpStr, "\r\n Wrong pid %d, sent pid %d", j1850_RX_Buf[4], u8Pid);
                        DebugPutString(tmpStr);
                    }
                } else {
                    DebugPutString("\r\n vpm received wrong crc buffer");
                }
            }
            // ====================PWM Process (Mode is NOT 03, 04)======================
            else {
                sprintf(tmpStr, "\r\n u8j1850_Rx_Frame_Count = %d", u8j1850_Rx_Frame_Count);
                DebugPutString(tmpStr);
                while (u8FrameIdx <= u8j1850_Rx_Frame_Count) {
                    u8StartByte = u8j1850_Rx_Start_Byte[u8FrameIdx];
                    u8StopByte = u8j1850_Rx_Stop_Byte[u8FrameIdx];
                    u8CRC = j1850_crc(&j1850_RX_Buf[u8StartByte], u8StopByte - u8StartByte);
                    if (u8CRC == j1850_RX_Buf[u8StopByte]) {
                        sprintf(tmpStr, "\r\n pwm mode %d correct crc at start %d stop %d",u8Mode, u8StartByte, u8StopByte);
                        DebugPutString(tmpStr);
                        memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], &j1850_RX_Buf[u8StartByte+3], u8StopByte - u8StartByte-3);
                        g_ODB2Msg.u8RespLen += u8StopByte - u8StartByte-3;
                        break; // only take one frame
                    } else {
                        sprintf(tmpStr, "\r\n pwm mode %d wrong crc at start %d stop %d",u8Mode, u8StartByte, u8StopByte);
                        DebugPutString(tmpStr);
                    }
                    u8FrameIdx++;
                }
                if (g_ODB2Msg.u8RespLen >= 0) {
                    g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
                }
//								g_ODB2Msg.u8RespLen = u16RxBytes - 4;// 3byte header + 1 crc
//                memcpy(g_ODB2Msg.pu8Data, &j1850_RX_Buf[3], g_ODB2Msg.u8RespLen);
//                g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
            }
        } else {
            // process for VPW 03 Mode
            DebugPutString("\r\n j1850 Mode 03 reply process");
            if (j1850_protocol == J1850_PROTOCOL_VPW) {
                while (u16Idx < u16RxBytes) {
                    u8CRC = j1850_crc(&j1850_RX_Buf[u16Idx], 10);
                    if (u8CRC == j1850_RX_Buf[u16Idx+10]) {
                        // correct crc frame
                        memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], &j1850_RX_Buf[u16Idx+4], 6);
                        g_ODB2Msg.u8RespLen += 6;
                    } else {
                        sprintf(tmpStr, "\r\nvpw mode 03 wrong crc at idx %d", u16Idx);
                        DebugPutString(tmpStr);
                    }
                    u16Idx += 11;
                }
                if (g_ODB2Msg.u8RespLen >= 0) {
                    g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
                }
            }
            // ===============process for PWM 03,04 Mode ==================
            else {
                while (u8FrameIdx <= u8j1850_Rx_Frame_Count) {
                    u8StartByte = u8j1850_Rx_Start_Byte[u8FrameIdx];
                    u8StopByte = u8j1850_Rx_Stop_Byte[u8FrameIdx];
                    u8CRC = j1850_crc(&j1850_RX_Buf[u8StartByte], u8StopByte - u8StartByte);
                    if (u8CRC == j1850_RX_Buf[u8StopByte]) {
                        sprintf(tmpStr, "\r\n pwm mode 03 correct crc at start %d stop %d",u8StartByte, u8StopByte);
                        DebugPutString(tmpStr);
                        memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], &j1850_RX_Buf[u8StartByte+4], u8StopByte - u8StartByte-4);
                        g_ODB2Msg.u8RespLen += u8StopByte - u8StartByte-4;
                    } else {
                        sprintf(tmpStr, "\r\n pwm mode 03 wrong crc at start %d stop %d",u8StartByte, u8StopByte);
                        DebugPutString(tmpStr);
                    }
                    u8FrameIdx++;
                }
                    g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
                if (g_ODB2Msg.u8RespLen >= 0) {
                }
            }
        }
    }
    DebugPutString("\r\nODB2_j1850_ProcessBleRequest Done");
//		HAL_Delay(10);
}

void ODB2_j1850_Transmit(uint8_t u8Mode, uint8_t u8Pid, uint8_t u8PidBytes)
{
    if (g_ODB2Protocol == ODB2_PROTOCOL_VPW)
    {
        CPU_STATUS=BUS_WRITING;
        SAE_VPW_StopReceive();
        SAE_VPW_Transmit(u8Mode, u8Pid, u8PidBytes);
        timerOnceStart(J1850_VPW_TRX_TIMEOUT);
        while (!isTimeOut()) {};

    } else {
#if J1850_PWM_OLD
        CPU_STATUS=BUS_WRITING;
        SAE_PWM_StopReceive();
        SAE_PWM_Transmit(u8Mode, u8Pid, u8PidBytes);
        timerOnceStart(J1850_PWM_TRX_TIMEOUT);
        while (!isTimeOut()) {};
#else
        j1850_pwm_transmit(u8Mode, u8Pid, u8PidBytes);
        timerOnceStart(J1850_PWM_TRX_TIMEOUT);
        while (!isTimeOut()) {};
        //j1850_pwm_print_rx_pulse();
#endif
    }
}

uint16_t OBD2_j1850_brutal_transmit(uint8_t u8Mode, uint8_t u8Pid, uint8_t u8PidBytes){
		if (g_ODB2Protocol == ODB2_PROTOCOL_VPW)	{
				if (SAE_VPW_brutal_transmit(u8Mode, u8Pid, u8PidBytes) == VPW_SEND_OK){
					return  vpw_rx_byte_number();
				} else {
					taskEXIT_CRITICAL();
				}
        //
    } else {
				LOOP:
				int atemp = 0;
				if ( j1850_pwm_brutal_transmit(u8Mode, u8Pid, u8PidBytes)== PWM_SEND_OK ){
					return pwm_rx_byte_number();
				} else {
						taskEXIT_CRITICAL();
						DebugPutString("\r\nPWM_SEND_NOK");
						if (atemp++>10){
							return 0;
						}
						goto LOOP;
				}
    }
		return 0;
}

uint16_t ODB2_j1850_RxProcessTask()
{
    if (g_ODB2Protocol == ODB2_PROTOCOL_VPW)
    {
        return SAE_VPW_RxProcessTask();
    } else {
#if J1850_PWM_OLD
        return SAE_PWM_RxProcessTask();
#else
        return j1850_pwm_process_rx_task();
#endif
    }
}

void j1850_brutal_init(void){
	GPIO_InitTypeDef GPIO_InitStruct;
	
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin : PA4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
 
  /*Configure GPIO pins : PB7 */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
}

uint16_t ODB2_j1850_brutal_RxProcessTask(void){
	if (g_ODB2Protocol == ODB2_PROTOCOL_VPW) {
        return SAE_VPW_RxProcessTask();
    } else {
        return j1850_pwm_process_rx_task();
    }
}

