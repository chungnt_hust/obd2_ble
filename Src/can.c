/**
 * @file can.c
 * @brief can task for stm32f1 (odbi adapter)
 * @date April 20, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */
#include "can.h"
#include "debug.h"
#include "utils.h"
#include "protocol.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "ble.h"

/* Private variables */
static char tmpStr[256];
static CAN_FilterConfTypeDef sFilterConfigAny;
static CAN_FilterConfTypeDef sFilterConfig11Bit;
static CAN_FilterConfTypeDef sFilterConfig29Bit;
extern CAN_HandleTypeDef hcan;
extern osSemaphoreId g_ODB2_Semid;
extern osSemaphoreId g_BLESemid;
extern ODB2_PROTOCOL g_ODB2Protocol;
extern ODB2Message g_ODB2Msg;
/**
 * @brief ODB2_CAN_Init
 * @param highspeed
 * @param extended
 * @return 0 if failed,  CAN_STANDARD = 1 for standard OK, CAN_EXTENDED = 2 for extended
 */
int ODB2_CAN_Init(int speed)
{
    ODB2_CAN_Setup_Speed(speed, CAN_MODE_NORMAL);
    uint32_t stdMsgID = 0x7DF;
    uint32_t extMsgID = 0x18DB33F1;// response = 0x18DB11F1;
    HAL_StatusTypeDef status;
		if(g_ODB2Protocol == ODB2_PROTOCOL_START || g_ODB2Protocol == ODB2_PROTOCOL_CAN11_250 || g_ODB2Protocol == ODB2_PROTOCOL_CAN11_500){
			/* Try Standard CAN 11 bit */
			ODB2_CAN_InitFilter11bit(true, CAN_FIFO0);
			ODB2_CAN_Transmit(stdMsgID, 0x01, 0x00, CAN_STANDARD);
			status = HAL_CAN_Receive(&hcan, CAN_FIFO0, 1000);
			if(status != HAL_OK){
					//Reception Error
					sprintf(tmpStr, "\r\n CanStd tried error 0x%02X ", (int)status);
					DebugPutString(tmpStr);
			} else {
					printCANmsg(hcan);
					if(	hcan.pRxMsg->IDE==CAN_ID_STD && hcan.pRxMsg->Data[0]>=0x06 && hcan.pRxMsg->Data[1]==0x41 && hcan.pRxMsg->Data[2]==0x00) {
						if(	hcan.pRxMsg->Data[3]!=0x00 || hcan.pRxMsg->Data[4]!=0x00 || hcan.pRxMsg->Data[5]!=0x00 || hcan.pRxMsg->Data[6]!=0x00)
							return CAN_STANDARD;
					} else if (	hcan.pRxMsg->IDE==CAN_ID_STD && 
											hcan.pRxMsg->Data[0]==0x07 &&	hcan.pRxMsg->Data[1]==0xFF && hcan.pRxMsg->Data[2]==0xFF &&	hcan.pRxMsg->Data[3]==0xFF && 
											hcan.pRxMsg->Data[4]==0xFF &&	hcan.pRxMsg->Data[5]==0xFF && hcan.pRxMsg->Data[6]==0x25 && hcan.pRxMsg->Data[7]==0x07){
						return CAN_REQUEST_BURNIN;
					}
			}
			ODB2_CAN_InitFilter11bit(false, CAN_FIFO0); // Disable Filter 11 Bit
		}
    if(g_ODB2Protocol == ODB2_PROTOCOL_START || g_ODB2Protocol == ODB2_PROTOCOL_CAN29_250 || g_ODB2Protocol == ODB2_PROTOCOL_CAN29_500){
			/* Try Extended CAN 29 bit */
			//ODB2_CAN_InitFilterAny(true);
			osDelay(1000);
			ODB2_CAN_InitFilter29bit(true,CAN_FIFO0);
			ODB2_CAN_Transmit(extMsgID, 0x01, 0x00, CAN_EXTENDED);
			status = HAL_CAN_Receive(&hcan, CAN_FIFO0, 1000);
			if(status != HAL_OK){
					//Reception Error
					sprintf(tmpStr, "\r\n CanEx tried error 0x%02X ", (int)status);
					DebugPutString(tmpStr);
			} else {
					printCANmsg(hcan);
					if(	hcan.pRxMsg->IDE==CAN_ID_EXT && hcan.pRxMsg->Data[0]>=0x06 && hcan.pRxMsg->Data[1]==0x41 && hcan.pRxMsg->Data[2]==0x00){
						if(	hcan.pRxMsg->Data[3]!=0x00 || hcan.pRxMsg->Data[4]!=0x00 || hcan.pRxMsg->Data[5]!=0x00 || hcan.pRxMsg->Data[6]!=0x00)
							return CAN_EXTENDED;
					}else if (	hcan.pRxMsg->IDE==CAN_ID_EXT && 
											hcan.pRxMsg->Data[0]==0x07 &&	hcan.pRxMsg->Data[1]==0xFF && hcan.pRxMsg->Data[2]==0xFF &&	hcan.pRxMsg->Data[3]==0xFF && 
											hcan.pRxMsg->Data[4]==0xFF &&	hcan.pRxMsg->Data[5]==0xFF && hcan.pRxMsg->Data[6]==0x25 && hcan.pRxMsg->Data[7]==0x07){
						return CAN_REQUEST_BURNIN;
					}
			}
			ODB2_CAN_InitFilter29bit(false,CAN_FIFO0);
		}
    return 0;
}
void printCANmsg(CAN_HandleTypeDef can)
{
    //DebugPutString("ReceiveCanMessage\r\n");
    uint32_t msgID;
    if (can.pRxMsg->IDE == CAN_ID_EXT) {
        DebugPutString("\r\nReceived CAN EXT, ");
        msgID = can.pRxMsg->ExtId;
    }
    else {
        DebugPutString("\r\nReceived CAN STD, ");
        msgID = can.pRxMsg->StdId;
    }

    sprintf(tmpStr, " StdMsgid: 0x%03X, extdMsgID = 0x%X, IDE = %d, data:\r\n ",
            can.pRxMsg->StdId, can.pRxMsg->ExtId, can.pRxMsg->IDE);
    DebugPutString(tmpStr);
    int i;
    for (i = 0; i < can.pRxMsg->DLC; i++)
    {
        sprintf(tmpStr,"0x%02X  ", hcan.pRxMsg->Data[i]);
        DebugPutString(tmpStr);
    }
    DebugPutString("\r\n");
}

void printCANmsg1(CAN_HandleTypeDef can){
	  uint32_t msgID;
    if (can.pRx1Msg->IDE == CAN_ID_EXT) {
        DebugPutString("\r\nReceived CAN EXT, ");
        msgID = can.pRx1Msg->ExtId;
    }
    else {
        DebugPutString("\r\nReceived CAN STD, ");
        msgID = can.pRx1Msg->StdId;
    }

    sprintf(tmpStr, " StdMsgid: 0x%03X, extdMsgID = 0x%X, IDE = %d, data:\r\n ",
            can.pRx1Msg->StdId, can.pRx1Msg->ExtId, can.pRx1Msg->IDE);
    DebugPutString(tmpStr);
    int i;
    for (i = 0; i < can.pRx1Msg->DLC; i++)
    {
        sprintf(tmpStr,"0x%02X  ", hcan.pRx1Msg->Data[i]);
        DebugPutString(tmpStr);
    }
    DebugPutString("\r\n");
}

void printCANmsgTx(CAN_HandleTypeDef can)
{
    //DebugPutString("ReceiveCanMessage\r\n");
    uint32_t msgID;
    if (can.pTxMsg->IDE == CAN_ID_EXT) {
        DebugPutString("\r\nSent CAN EXT, ");
        msgID = can.pTxMsg->ExtId;
    }
    else {
        DebugPutString("\r\nSent CAN STD, ");
        msgID = can.pTxMsg->StdId;
    }

    sprintf(tmpStr, " StdMsgid: 0x%03X, extdMsgID = 0x%X, IDE = %d, data:\r\n ",
            can.pTxMsg->StdId, can.pTxMsg->ExtId, can.pTxMsg->IDE);
    DebugPutString(tmpStr);
    int i;
    for (i = 0; i < can.pTxMsg->DLC; i++)
    {
        sprintf(tmpStr,"0x%02X  ", hcan.pTxMsg->Data[i]);
        DebugPutString(tmpStr);
    }
    DebugPutString("\r\n");
}

/**
 * @brief MX_CAN_Transmit
 * @param stdID
 * @param u8Mode
 * @param u8PID
 * @param u32IDE 0 = stdid, 1 = extended id
 * @return typedef enum
 *{
 * HAL_OK       = 0x00,
 * HAL_ERROR    = 0x01,
 * HAL_BUSY     = 0x02,
 * HAL_TIMEOUT  = 0x03
 *} HAL_StatusTypeDef;
 */
int ODB2_CAN_Transmit(uint32_t msgID, uint8_t u8Mode, uint8_t u8PID, uint32_t u32IDE)
{
    /*##-3- Start the Transmission process #####################################*/
    //    u8SentMode = u8Mode;
    //    u8SentPID = u8PID;
    //    u8IsBLEWaitingCAN = 1;
    HAL_StatusTypeDef ret;

    hcan.pTxMsg->RTR = CAN_RTR_DATA;
    if (u32IDE == CAN_STANDARD)
    {
        hcan.pTxMsg->IDE = CAN_ID_STD;
        hcan.pTxMsg->StdId = msgID & 0x7FF;
        sprintf(tmpStr, "\r\n\r\nCAN Send Std msgid =0x%X", hcan.pTxMsg->StdId );
    }
    else
    {
        hcan.pTxMsg->IDE = CAN_ID_EXT;
        hcan.pTxMsg->ExtId = msgID & 0x1FFFFFFF;
        sprintf(tmpStr, "\r\n\r\nCAN Send ext msgid =0x%X", hcan.pTxMsg->ExtId );
    }
    DebugPutString(tmpStr);
    uint8_t u8Len = 8;
    if (u8Len > 8)
        u8Len = 8;
    hcan.pTxMsg->DLC = u8Len;
    int i;
    sprintf(tmpStr, ", mode = %d, pid = %d: ", u8Mode, u8PID);
    DebugPutString(tmpStr);
    if (u8Mode == 3 || u8Mode == 4 ) {
        hcan.pTxMsg->Data[0] = 1;
        hcan.pTxMsg->Data[1] = u8Mode;
        hcan.pTxMsg->Data[2] = 0;
    }
    else
    {
        hcan.pTxMsg->Data[0] = 2;
        hcan.pTxMsg->Data[1] = u8Mode;
        hcan.pTxMsg->Data[2] = u8PID;
    }
    for (i = 3; i < 8; i++)
        hcan.pTxMsg->Data[i] = 0x00;
    //HAL_TIM_Base_Start_IT(&htim2);
    printCANmsgTx(hcan);
    ret = HAL_CAN_Transmit(&hcan, 100);
    if(ret != HAL_OK)
    {
        /* Transmission Error */
        DebugPutString("\r\nCAN Transmit Error");
    }
    return ret;
}
void printConfigFilter(CAN_FilterConfTypeDef  sFilterConfig)
{
#if 1
    sprintf(tmpStr, "\r\nFilterIdHigh = 0x%X", sFilterConfig.FilterIdHigh);
    DebugPutString(tmpStr);
    sprintf(tmpStr, "\r\nFilterIdLow = 0x%X", sFilterConfig.FilterIdLow);
    DebugPutString(tmpStr);
    sprintf(tmpStr, "\r\nFilterMaskIdHigh = 0x%X", sFilterConfig.FilterMaskIdHigh);
    DebugPutString(tmpStr);
    sprintf(tmpStr, "\r\nFilterMaskIdLow = 0x%X", sFilterConfig.FilterMaskIdLow);
    DebugPutString(tmpStr);
#endif
}
/**
 * @brief setFilterEnable activate sFilterConfig
 * @param sFilterConfig
 * @param u8Enabled
 */
void setFilterEnableDisable(CAN_FilterConfTypeDef  sFilterConfig, uint8_t u8Enabled)
{
    if (u8Enabled)
        sFilterConfig.FilterActivation = ENABLE;
    else
        sFilterConfig.FilterActivation = DISABLE;
}

/**
 * @brief ODB2_CAN_InitFilterAny
 */
int ODB2_CAN_InitFilterAny(uint8_t u8Enabled, uint8_t FIFO_Number)
{
    CAN_FilterConfTypeDef  sFilterConfig;
    memcpy(&sFilterConfig, &sFilterConfigAny, sizeof(CAN_FilterConfTypeDef));
//    setFilterEnableDisable(sFilterConfig, u8Enabled);
		if (u8Enabled)
        sFilterConfig.FilterActivation = ENABLE;
    else
        sFilterConfig.FilterActivation = DISABLE;
		sFilterConfig.FilterFIFOAssignment = FIFO_Number;
		
    if(HAL_CAN_ConfigFilter(&hcan, &sFilterConfig) != HAL_OK)
    {
        /* Filter configuration Error */
        DebugPutString("\r\nCAN_ConfigFilter Error");
        return false;
    }
    else
        return true;
}

/**
 * @brief ODB2_CAN_InitFilter11bit
 * @param u8Enabled activate filter for ODB CAN 11 bit
 * @return true if init success
 */
int ODB2_CAN_InitFilter11bit(uint8_t u8Enabled, uint8_t FIFO_Number)
{
    CAN_FilterConfTypeDef  sFilterConfig;
    memcpy(&sFilterConfig, &sFilterConfig11Bit, sizeof(CAN_FilterConfTypeDef));
		if (u8Enabled)
        sFilterConfig.FilterActivation = ENABLE;
    else
        sFilterConfig.FilterActivation = DISABLE;
		sFilterConfig.FilterFIFOAssignment = FIFO_Number;
		
    if(HAL_CAN_ConfigFilter(&hcan, &sFilterConfig) != HAL_OK)
    {
        /* Filter configuration Error */
        DebugPutString("\r\nODB2_CAN_InitFilter11bit Error");
        return false;
    } else
        return true;
}

/**
 * @brief ODB2_CAN_InitFilter29bit
 * @param u8Enabled
 * @return 1 if init succesfully else return 0
 */
int ODB2_CAN_InitFilter29bit(uint8_t u8Enabled, uint8_t FIFO_Number)
{
    CAN_FilterConfTypeDef  sFilterConfig;
    memcpy(&sFilterConfig, &sFilterConfig29Bit, sizeof(CAN_FilterConfTypeDef));
    if (u8Enabled)
        sFilterConfig.FilterActivation = ENABLE;
    else
        sFilterConfig.FilterActivation = DISABLE;
		sFilterConfig.FilterFIFOAssignment = FIFO_Number;
    if(HAL_CAN_ConfigFilter(&hcan, &sFilterConfig) != HAL_OK)
    {
        /* Filter configuration Error */
        DebugPutString("\r\nODB2_CAN_InitFilter29bit Error");
        return false;
    }
    else
        return true;
}
/**
 * @brief CAN_Init_Speed
 * @param speed
 */
void ODB2_CAN_Setup_Speed(int speed, uint32_t u32Can_Mode)
{
    static CanTxMsgTypeDef        TxMessage;
    static CanRxMsgTypeDef        RxMessage;
		static CanRxMsgTypeDef				Rx1Message;
    
		//memset(TxMessage,0,sizeof(TxMessage));
		hcan.Instance = CAN;
	
    if (speed == CAN_HIGHSPEED)
        hcan.Init.Prescaler = CAN_500_PRESCALE;
    else
        hcan.Init.Prescaler = CAN_250_PRESCALE;
    hcan.pTxMsg = &TxMessage;
    hcan.pRxMsg = &RxMessage;
		hcan.pRx1Msg = &Rx1Message;
    hcan.Init.Mode = u32Can_Mode;//CAN_MODE_NORMAL;
    hcan.Init.SJW = CAN_SJW_3TQ;
    hcan.Init.BS1 = CAN_BS1_9TQ;
    hcan.Init.BS2 = CAN_BS2_6TQ;
    hcan.Init.TTCM = DISABLE;
    hcan.Init.ABOM = DISABLE;
    hcan.Init.AWUM = DISABLE;
    hcan.Init.NART = ENABLE;
    hcan.Init.RFLM = DISABLE;
    hcan.Init.TXFP = DISABLE;
    if (HAL_CAN_Init(&hcan) != HAL_OK)
    {
        DebugPutString("\r\nCan init ERROR");
    }
}
/**
 * @brief MX_CAN_DeInit
 */
void ODB2_CAN_DeInit()
{
	
//	__HAL_RCC_CAN1_FORCE_RESET();
//	__HAL_RCC_CAN1_RELEASE_RESET();
	
    if (HAL_CAN_DeInit(&hcan) != HAL_OK)
    {
        DebugPutString("\r\nCAN Deinit ERROR");
    }
}

void ODB2_CAN_InitFilters(uint8_t u8Enabled)
{
    FunctionalState canFilterEnabled;
    if (u8Enabled)
    {
        canFilterEnabled = ENABLE;
    } else
    {
        canFilterEnabled = DISABLE;
    }

    // ============Config Filter any
    sFilterConfigAny.FilterNumber = CAN_ANY_FILTER_NUMBER;
    sFilterConfigAny.FilterMode = CAN_FILTERMODE_IDMASK;
    sFilterConfigAny.FilterScale = CAN_ANY_FILTER_SCALE;
    sFilterConfigAny.FilterIdHigh = CAN_ANY_FILTER_ID_HIGH;
    sFilterConfigAny.FilterIdLow = CAN_ANY_FILTER_ID_LOW;
    sFilterConfigAny.FilterMaskIdHigh = CAN_ANY_FILTER_MASK_HIGH;
    sFilterConfigAny.FilterMaskIdLow = CAN_ANY_FILTER_MASK_LOW;
    sFilterConfigAny.FilterFIFOAssignment = CAN_FIFO0;
    sFilterConfigAny.FilterActivation = canFilterEnabled;
    sFilterConfigAny.BankNumber = CAN_FILTER_ANY_BANK_NUMBER;

    // ===============Config Filter 11 Bit for ODB2
    sFilterConfig11Bit.FilterNumber = CAN_11BIT_FILTER_NUMBER;
    sFilterConfig11Bit.FilterMode = CAN_FILTERMODE_IDMASK;
    sFilterConfig11Bit.FilterScale = CAN_11BIT_FILTER_SCALE;
    sFilterConfig11Bit.FilterIdHigh = CAN_11BIT_FILTER_ID_HIGH;
    sFilterConfig11Bit.FilterIdLow = CAN_11BIT_FILTER_ID_LOW;
    sFilterConfig11Bit.FilterMaskIdHigh = CAN_11BIT_FILTER_MASK_HIGH;
    sFilterConfig11Bit.FilterMaskIdLow = CAN_11BIT_FILTER_MASK_LOW;
    sFilterConfig11Bit.FilterFIFOAssignment = CAN_FIFO0;
    sFilterConfig11Bit.FilterActivation = canFilterEnabled;
    sFilterConfig11Bit.BankNumber = CAN_11BIT_FILTER_BANK_NUMBER;

    // ===============Config Filter 29 Bit for ODB2
    sFilterConfig29Bit.FilterNumber = CAN_29BIT_FILTER_NUMBER;
    sFilterConfig29Bit.FilterMode = CAN_FILTERMODE_IDMASK;
    sFilterConfig29Bit.FilterScale = CAN_29BIT_FILTER_SCALE;
    sFilterConfig29Bit.FilterIdHigh = CAN_29BIT_FILTER_ID_HIGH;
    sFilterConfig29Bit.FilterIdLow = CAN_29BIT_FILTER_ID_LOW;
    sFilterConfig29Bit.FilterMaskIdHigh = CAN_29BIT_FILTER_MASK_HIGH;
    sFilterConfig29Bit.FilterMaskIdLow = CAN_29BIT_FILTER_MASK_LOW;
    sFilterConfig29Bit.FilterFIFOAssignment = CAN_FIFO0;
    sFilterConfig29Bit.FilterActivation = canFilterEnabled;
    sFilterConfig29Bit.BankNumber = CAN_29BIT_FILTER_BANK_NUMBER;
}

/**
 * @brief ODB2_CAN_ProcessBleRequest
 *        - request ODB2 mode and pid in g_ODB2Msg
 *        - if received correct response
 *          + set correct response length data g_ODB2Msg.u8RespLen
 *          + copy response data to g_ODB2Msg.pu8Data
 *          + set g_ODB2Msg.u8Status b = ODB2_STATUS_PROCESS_END
 *
 * @return
 */
int ODB2_CAN_ProcessBleRequest()
{
    // TODO: process g_ODB2Msg
    uint32_t stdMsgID = 0x7DF;
    uint32_t extMsgID = 0x18DB33F1;// response = 0x18DB11F1;
    uint32_t extMsgID_response_mask = 0x18DA00F1;
    HAL_StatusTypeDef status;
    uint8_t u8Mode = g_ODB2Msg.u8Mode;
    // Now only handle one byte PID
    uint8_t u8PID = g_ODB2Msg.u8PID & 0xFF;
    uint8_t u8ISOTPLen = 0;  // first byte in ISO-TP CAN 1 https://en.wikipedia.org/wiki/ISO_15765-2
    int iReadingISOTP = 0;
    uint8_t u8ISOType = 0;
    uint8_t u8CopiedLen = 0;
    uint8_t u8ISReadBytes = 0;
    uint8_t u8SID;
    bool bRet = false;
    int iTries = 2;
    int iTriesRead;
    int i;

    CanRxMsgTypeDef CAN_RX_Buf[4];
    int rxCanCount = 0;
    if (g_ODB2Msg.u8Mode != 0x03) {
        while (iTries-- > 0) {
            if (g_ODB2Protocol == ODB2_PROTOCOL_CAN11_250 ||
                    g_ODB2Protocol == ODB2_PROTOCOL_CAN11_500) {
                status = ODB2_CAN_Transmit(stdMsgID, g_ODB2Msg.u8Mode, g_ODB2Msg.u8PID, CAN_STANDARD);
            } else {
                status = ODB2_CAN_Transmit(extMsgID, g_ODB2Msg.u8Mode, g_ODB2Msg.u8PID, CAN_EXTENDED);
            }
            g_ODB2Msg.u8RespLen = 0;
            if (status == HAL_OK) {
                g_ODB2Msg.u8RespLen = 0;
                while (status == HAL_OK)
                {
                    status = HAL_CAN_Receive(&hcan, CAN_FIFO0, 200);
                    if(status != HAL_OK){
                        //Reception Error
                        sprintf(tmpStr, "\r\n Can receive error 0x%02X ", (int)status);
                        DebugPutString(tmpStr);
                        //return false;
                    } else {
                        printCANmsg(hcan);
                        u8ISOTPLen = hcan.pRxMsg->Data[0];

                        if (u8ISOTPLen <=7) {
                            // Data: 06 41 00 BF BF A8 91 00
                            // g_ODB2Msg.pu8Data = [41 00 BF BF A8 91 00]
                            g_ODB2Msg.u8RespLen = u8ISOTPLen;
                            u8Mode = hcan.pRxMsg->Data[1];
                            u8PID = hcan.pRxMsg->Data[2];
                            memcpy(g_ODB2Msg.pu8Data, &(hcan.pRxMsg->Data[1]), u8ISOTPLen);
                            //if (u8PID == g_ODB2Msg.u8PID || u8Mode == 0x7F) { // comment if not check for PID
                            if (u8PID == g_ODB2Msg.u8PID) { // comment if not check for PID
                                DebugPutString("\r\nCAN recv correct CAN Message");
                                g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
                                break;
                            }// comment if not check for PID
                            else {
                                DebugPutString("\r\nCAN recv Wrong PID");
                                g_ODB2Msg.u8Status = ODB2_STATUS_PROCESSED_ERROR;

                            }
                        } else {
                            sprintf(tmpStr, "\r\nWrong u8DLC = %d", u8ISOTPLen);
                            DebugPutString(tmpStr);
                        }
                    }

                }// end of while (status == HAL_OK
            } // end of transmit status == OK
            else {
                DebugPutString("\r\nCAN Transmit Error");
                g_ODB2Msg.u8Status = ODB2_STATUS_PROCESSED_ERROR;
            }
            if (g_ODB2Msg.u8Status == ODB2_STATUS_PROCESS_END) {
                break; //while (iTries-- > 0)
            }
        }
    } else {  // for mode 0x03
        while (iTries-- > 0) {
            //taskENTER_CRITICAL();
            if (g_ODB2Protocol == ODB2_PROTOCOL_CAN11_250 ||
                    g_ODB2Protocol == ODB2_PROTOCOL_CAN11_500) {
                status = ODB2_CAN_Transmit(stdMsgID, g_ODB2Msg.u8Mode, g_ODB2Msg.u8PID, CAN_STANDARD);
            } else {
                status = ODB2_CAN_Transmit(extMsgID, g_ODB2Msg.u8Mode, g_ODB2Msg.u8PID, CAN_EXTENDED);
            }
            g_ODB2Msg.u8RespLen = 0;
            rxCanCount = 0;
            while (status == HAL_OK) {
                DebugPutChar('*');
                status = HAL_CAN_Receive(&hcan, CAN_FIFO0, 200);
                if (status == HAL_OK)
                {
                    memcpy(&CAN_RX_Buf[rxCanCount++], hcan.pRxMsg, sizeof(CanRxMsgTypeDef));
                    if (((hcan.pRxMsg->Data[0] & 0xF0)>>4) == 0x01) // check for First TP-ISO
                    {
                        if (g_ODB2Protocol == ODB2_PROTOCOL_CAN11_250 ||
                                g_ODB2Protocol == ODB2_PROTOCOL_CAN11_500)
                        {
                            ODB2_CAN_Send_TP_ISO_FlowControl(hcan.pRxMsg->StdId-8, CAN_STANDARD);
                        } else {
                            ODB2_CAN_Send_TP_ISO_FlowControl(0x18DA00F1 | ((hcan.pRxMsg->ExtId&0x00FF) << 8), CAN_EXTENDED);
                        }
                    } else {
                        DebugPutChar(':');
                    }
                }
                else {
                    sprintf(tmpStr, "\r\nCAN Status = %d", status);
                    DebugPutString(tmpStr);
                }
            }
            // Parsing 03 Mode read data
            iReadingISOTP = 0;
            u8ISReadBytes = 0;
            for (i = 0; i < rxCanCount; i++) {
                sprintf(tmpStr, "\r\nCanRx[%d], DLC = %d: ", i, CAN_RX_Buf[i].DLC);
                DebugPutString(tmpStr);
                dumpHex(CAN_RX_Buf[i].Data, CAN_RX_Buf[i].DLC);
                u8ISOType = (CAN_RX_Buf[i].Data[0] & 0xF0) >> 4;
                // ISO_TP First frame
                if (u8ISOType == ISO_TP_TYPE_FIRST) {
                    u8ISOTPLen = CAN_RX_Buf[i].Data[1];
                    u8ISReadBytes = 0;
                    u8CopiedLen = 0;
                    if (CAN_RX_Buf[i].Data[2] == 0x43) {
                        sprintf(tmpStr, "\r\n ISOTP First frame len = %d", u8ISOTPLen);
                        DebugPutString(tmpStr);
                        if (g_ODB2Msg.u8RespLen == 0) {
                            g_ODB2Msg.pu8Data[0] = 0x43;
                            g_ODB2Msg.u8RespLen = 1;
                            u8ISReadBytes = 1;
                        }
                        memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], &CAN_RX_Buf[i].Data[4], CAN_RX_Buf[i].DLC - 4);
                        g_ODB2Msg.u8RespLen += CAN_RX_Buf[i].DLC - 4;
                        u8ISReadBytes += CAN_RX_Buf[i].DLC - 3;
                        sprintf(tmpStr, "\r\n u8CopiedLen = %d, u8RespLen = %d, u8ISReadBytes = %d",
													u8CopiedLen, g_ODB2Msg.u8RespLen, u8ISReadBytes);
                        DebugPutString(tmpStr);

                    }
                    iReadingISOTP = 1;
                }
                if (u8ISOType == ISO_TP_TYPE_CONSEC) {
                    DebugPutString("\r\nISO_TP Consecutive frame");
                    if (iReadingISOTP) {
                        u8CopiedLen = (u8ISOTPLen - u8ISReadBytes) < 8 ? u8ISOTPLen - u8ISReadBytes : 7;

                        memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], &CAN_RX_Buf[i].Data[1],u8CopiedLen);
                        u8ISReadBytes += u8CopiedLen;
                        g_ODB2Msg.u8RespLen += u8CopiedLen;
                        sprintf(tmpStr, "\r\n u8CopiedLen = %d, u8RespLen = %d, u8ISReadBytes = %d",
													u8CopiedLen, g_ODB2Msg.u8RespLen, u8ISReadBytes);
                        DebugPutString(tmpStr);
                    }
                }
                if (u8ISOType == ISO_TP_TYPE_SINGLE) {
                    iReadingISOTP = 0;
                    if (CAN_RX_Buf[i].Data[1] == 0x43) {                        
                        u8CopiedLen = CAN_RX_Buf[i].Data[0];
                        if (g_ODB2Msg.u8RespLen > 0) { // truong hop nay da co ban tin isotp truoc khi co single
                            u8CopiedLen-=2;
														if (u8CopiedLen > 4)
															u8CopiedLen = 4;												
														sprintf(tmpStr, "\r\nISO_TP Single frame after ISO, u8CopiedLen = %d", u8CopiedLen);
														DebugPutString(tmpStr);
                            memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], &CAN_RX_Buf[i].Data[3],u8CopiedLen);

                        } else  {// Truong hop nay chi co mot ban tin single 
												
														g_ODB2Msg.pu8Data[0] = 0x43;
                            g_ODB2Msg.u8RespLen = 1;
														u8CopiedLen -= 2;
														sprintf(tmpStr, "\r\nISO_TP Single frame only, u8CopiedLen = %d", u8CopiedLen);
														DebugPutString(tmpStr);
                            memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], &CAN_RX_Buf[i].Data[3],u8CopiedLen);
                        }
                        g_ODB2Msg.u8RespLen += u8CopiedLen;
                    }
                }
            }
            if (g_ODB2Msg.u8RespLen > 0) {
                g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
                break;
            }
        } // while (iTries-- > 0)
    } // end of 03 Mode
    sprintf(tmpStr, "\r\nODB2_CAN_ProcessBleRequest Done: %d", g_ODB2Msg.u8RespLen );
    DebugPutString(tmpStr);
}


int ODB2_CAN_Send_TP_ISO_FlowControl(uint32_t msgID, uint32_t u32IDE)
{
    /*##-3- Start the Transmission process #####################################*/
    //    u8SentMode = u8Mode;
    //    u8SentPID = u8PID;
    //    u8IsBLEWaitingCAN = 1;
    HAL_StatusTypeDef ret;

    hcan.pTxMsg->RTR = CAN_RTR_DATA;
    if (u32IDE == CAN_STANDARD)
    {
        hcan.pTxMsg->IDE = CAN_ID_STD;
        hcan.pTxMsg->StdId = msgID & 0x7FF;
    }
    else
    {
        hcan.pTxMsg->IDE = CAN_ID_EXT;
        hcan.pTxMsg->ExtId = msgID & 0x1FFFFFFF;
    }
    hcan.pTxMsg->DLC = 8;
    hcan.pTxMsg->Data[0] = 0x30;
    hcan.pTxMsg->Data[1] = 0x00;
    hcan.pTxMsg->Data[2] = 0x00;
    int i;
    for (i = 3; i < 7; i++)
    {
        hcan.pTxMsg->Data[i] = 0x00;
    }
    //printCANmsgTx(hcan);
    ret = HAL_CAN_Transmit(&hcan, 100);
    if(ret != HAL_OK)
    {
        /* Transmission Error */
        DebugPutString("\r\nCAN Transmit Error");
    }
    return ret;
}

int ODB2_TOYOTA_CAN_Init(void)	{
	ODB2_CAN_Setup_Speed(CAN_HIGHSPEED, CAN_MODE_NORMAL);
	ODB2_CAN_InitFilter11bit(true,CAN_FIFO0);
	uint32_t stdMsgID = 0x7E0;
	HAL_StatusTypeDef status;
	uint8_t TxMessage[8] = {0};
	TxMessage[0] = 0x21;
	TxMessage[1] = 0x00;
//	ODB2_CAN_Transmit(stdMsgID, 0x21, 0x00, CAN_STANDARD);
	ODB2_CAN_TransmitMessage(0x7E0,CAN_STANDARD,TxMessage,2);
	status = HAL_CAN_Receive(&hcan, CAN_FIFO0, 1000);
	if(status != HAL_OK) {
		//Reception Error
		sprintf(tmpStr, "\r\nToyota Can tried error 0x%02X ", (int)status);
		DebugPutString(tmpStr);
	} else {
		DebugPutString("\r\nToyota can init response");
		printCANmsg(hcan);
		if(	hcan.pRxMsg->IDE==CAN_ID_STD && hcan.pRxMsg->Data[0]>=0x06 && hcan.pRxMsg->Data[1]==0x61 && hcan.pRxMsg->Data[2]==0x00) {
				if(	hcan.pRxMsg->Data[3]!=0x00 || hcan.pRxMsg->Data[4]!=0x00 || hcan.pRxMsg->Data[5]!=0x00 || hcan.pRxMsg->Data[6]!=0x00)
					return CAN_TOYOTA;
		} else if (	hcan.pRxMsg->IDE==CAN_ID_STD && 
								hcan.pRxMsg->Data[0]==0x07 &&	hcan.pRxMsg->Data[1]==0xFF && hcan.pRxMsg->Data[2]==0xFF &&	hcan.pRxMsg->Data[3]==0xFF && 
								hcan.pRxMsg->Data[4]==0xFF &&	hcan.pRxMsg->Data[5]==0xFF && hcan.pRxMsg->Data[6]==0x25 && hcan.pRxMsg->Data[7]==0x07){
			return CAN_REQUEST_BURNIN;
		}
	}
	ODB2_CAN_InitFilter11bit(false,CAN_FIFO0); // Disable Filter 11 Bit
	return 0;
}

int ODB2_CAN_TransmitMessage(uint32_t MessageId, uint32_t u32IDE, uint8_t* pu8Data, uint8_t u8Len){
	HAL_StatusTypeDef ret;
	hcan.pTxMsg->RTR = CAN_RTR_DATA;
	if (u32IDE == CAN_STANDARD) {
			hcan.pTxMsg->IDE = CAN_ID_STD;
			hcan.pTxMsg->StdId = MessageId & 0x7FF;
			sprintf(tmpStr, "\r\n\r\nCAN Send Std msgid =0x%X", hcan.pTxMsg->StdId );
	} else {
			hcan.pTxMsg->IDE = CAN_ID_EXT;
			hcan.pTxMsg->ExtId = MessageId & 0x1FFFFFFF;
			sprintf(tmpStr, "\r\n\r\nCAN Send ext msgid =0x%X", hcan.pTxMsg->ExtId );
	}
	DebugPutString(tmpStr);
	hcan.pTxMsg->DLC = 8;
	hcan.pTxMsg->Data[0] = 0;
	hcan.pTxMsg->Data[1] = 0;
	hcan.pTxMsg->Data[2] = 0;
	hcan.pTxMsg->Data[3] = 0;
	hcan.pTxMsg->Data[4] = 0;
	hcan.pTxMsg->Data[5] = 0;
	hcan.pTxMsg->Data[6] = 0;
	hcan.pTxMsg->Data[7] = 0;
	if(u8Len<=7) {
		hcan.pTxMsg->Data[0]  =  u8Len;
		for(int i=1; i<= u8Len; i++) {
			hcan.pTxMsg->Data[i] = pu8Data[i-1];
		}
	}
	ret = HAL_CAN_Transmit(&hcan, 100);
	if(ret != HAL_OK) {
			/* Transmission Error */
			DebugPutString("\r\nCAN Transmit Error");
	}
	return ret;
}

int ODB2_TOYOTA_CAN_ProcessBleRequest(void){
	uint32_t messageId = 0x7E0;
	HAL_StatusTypeDef status;
	uint8_t TxMessage[8] = {0};
	if(g_ODB2Msg.u8Mode == 0x01){
		TxMessage[0] = 0x21;
		TxMessage[1] = g_ODB2Msg.u8PID;
		status = ODB2_CAN_TransmitMessage(messageId,CAN_STANDARD, TxMessage, 2);
		if (status == HAL_OK) {
			g_ODB2Msg.u8RespLen = ODB2_TOYOTA_CAN_ISO_TP_Receive(messageId,CAN_STANDARD,0x61,g_ODB2Msg.u8PID,true, g_ODB2Msg.pu8Data);
			if(g_ODB2Msg.u8RespLen > 0) g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
			else g_ODB2Msg.u8Status = ODB2_STATUS_PROCESSED_ERROR;
		} else {
				DebugPutString("\r\nCAN Transmit Error");
				g_ODB2Msg.u8Status = ODB2_STATUS_PROCESSED_ERROR;
		}
	} else if (g_ODB2Msg.u8Mode == 0x03) {
		g_ODB2Msg.pu8Data[0] = 0x43;
//		g_ODB2Msg.pu8Data[1] = 0x00;
		g_ODB2Msg.u8RespLen = 1;
		/*Engine check*/
		TxMessage[0] = 0x13;
		TxMessage[1] = 0x80;
		status = ODB2_CAN_TransmitMessage(messageId,CAN_STANDARD, TxMessage, 2);
		if(status == HAL_OK){
			g_ODB2Msg.u8RespLen += ODB2_TOYOTA_CAN_ISO_TP_Mode03_Receive(messageId,CAN_STANDARD,0x53, &g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], true);
		} else {
			DebugPutString("\r\nCAN Transmit Error");
		}
		g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
		/**/
	}
	return 0;
}

int ODB2_TOYOTA_CAN_ISO_TP_TransmitFlowControlFrame(uint32_t MessageId, uint32_t u32IDE){
	HAL_StatusTypeDef ret;
	hcan.pTxMsg->RTR = CAN_RTR_DATA;
	if (u32IDE == CAN_STANDARD) {
			hcan.pTxMsg->IDE = CAN_ID_STD;
			hcan.pTxMsg->StdId = MessageId & 0x7FF;
			sprintf(tmpStr, "\r\n\r\nCAN Send Std msgid =0x%X", hcan.pTxMsg->StdId );
	} else {
			hcan.pTxMsg->IDE = CAN_ID_EXT;
			hcan.pTxMsg->ExtId = MessageId & 0x1FFFFFFF;
			sprintf(tmpStr, "\r\n\r\nCAN Send ext msgid =0x%X", hcan.pTxMsg->ExtId );
	}
	DebugPutString(tmpStr);
	hcan.pTxMsg->DLC = 8;
	hcan.pTxMsg->Data[0] = 30;
	hcan.pTxMsg->Data[1] = 0;
	hcan.pTxMsg->Data[2] = 0;
	hcan.pTxMsg->Data[3] = 0;
	hcan.pTxMsg->Data[4] = 0;
	hcan.pTxMsg->Data[5] = 0;
	hcan.pTxMsg->Data[6] = 0;
	hcan.pTxMsg->Data[7] = 0;
	ret = HAL_CAN_Transmit(&hcan, 100);
	if(ret != HAL_OK) {
		/* Transmission Error */
		DebugPutString("\r\nCAN Transmit Error");
	}
	return ret;
}

int ODB2_TOYOTA_CAN_ISO_TP_Receive(uint32_t MessageId, uint32_t u32IDE, uint8_t u8Mode, uint8_t u8PID, bool bPID, uint8_t* pRxData){
	HAL_StatusTypeDef status;
	int len = 0;
	status = HAL_CAN_Receive(&hcan, CAN_FIFO0, 200);
	if(status == HAL_OK){
		printCANmsg(hcan);
		if ( hcan.pRxMsg->Data[0]&0x10 && hcan.pRxMsg->Data[2] == u8Mode  && (!bPID || hcan.pRxMsg->Data[3] == u8PID) ) {
			int dlen = ((uint16_t)(hcan.pRxMsg->Data[0]&0x0f)<<8) + hcan.pRxMsg->Data[1];
			memcpy(pRxData, &(hcan.pRxMsg->Data[2]),6);
			len+=6;
			ODB2_TOYOTA_CAN_ISO_TP_TransmitFlowControlFrame(MessageId,CAN_STANDARD);
			uint8_t seqHeader = 0x21;
			while(status == HAL_OK && len<dlen){
				status = HAL_CAN_Receive(&hcan, CAN_FIFO0, 200);
				printCANmsg(hcan);
				if(hcan.pRxMsg->Data[0]==seqHeader) {
					memcpy(&pRxData[len], &(hcan.pRxMsg->Data[1]),7);
					len+=7;
					seqHeader++;
				} else break;
			}
			if(len>dlen) len = dlen;
		}	else if ( hcan.pRxMsg->Data[0]<=0x07 && hcan.pRxMsg->Data[1] == u8Mode && (!bPID || (hcan.pRxMsg->Data[2] == u8PID)) ) {
			len = hcan.pRxMsg->Data[0];
			memcpy(pRxData, &(hcan.pRxMsg->Data[1]),len);
		} else {
			DebugPutString("\r\nWrong thing happen");
			//printCANmsg(hcan);
			sprintf(tmpStr,"\r\n0x%02x , 0x%02x", hcan.pRxMsg->Data[1], hcan.pRxMsg->Data[2] );
			DebugPutString(tmpStr);
			sprintf(tmpStr,"\r\n0x%02x u8Mode, 0x%02x u8PID", u8Mode, u8PID);
			DebugPutString(tmpStr);
		}
	} else {
		DebugPutString("\r\nODB2_TOYOTA_CAN_ISO_TP_Receive error");
	}
	return len;
}

int ODB2_TOYOTA_CAN_ISO_TP_Mode03_Receive(uint32_t MessageId, uint32_t u32IDE, uint8_t u8Mode, uint8_t* pRxData, bool bErrCount){
	HAL_StatusTypeDef status;
	int len = 0;
	status = HAL_CAN_Receive(&hcan, CAN_FIFO0, 200);
	if(status == HAL_OK) {
		printCANmsg(hcan);
		if ( hcan.pRxMsg->Data[0]&0x10 && hcan.pRxMsg->Data[2] == u8Mode) {
			int dlen = ((uint16_t)(hcan.pRxMsg->Data[0]&0x0f)<<8) + hcan.pRxMsg->Data[1];
//			pRxData[0] = u8Mode;
			if(bErrCount)  {
				memcpy(pRxData,&(hcan.pRxMsg->Data[4]),4);
				len+=4;
			} else {
				memcpy(pRxData,&(hcan.pRxMsg->Data[3]),5);
				len+=5;
			}
			ODB2_TOYOTA_CAN_ISO_TP_TransmitFlowControlFrame(MessageId,CAN_STANDARD);
			uint8_t seqHeader = 0x21;
			while(status == HAL_OK && len<dlen){
				status = HAL_CAN_Receive(&hcan, CAN_FIFO0, 200);
				printCANmsg(hcan);
				if(hcan.pRxMsg->Data[0]==seqHeader) {
					memcpy(&pRxData[len], &(hcan.pRxMsg->Data[1]),7);
					len+=7;
					seqHeader++;
				} else break;
			}
			if(len>dlen) len = dlen;
		}	else if ( hcan.pRxMsg->Data[0]<=0x07 && hcan.pRxMsg->Data[1] == u8Mode ) {
			if(bErrCount){
				len = hcan.pRxMsg->Data[0] - 2;
				memcpy(pRxData, &(hcan.pRxMsg->Data[3]),len);
			} else {
				len = hcan.pRxMsg->Data[0] - 1;
				memcpy(pRxData, &(hcan.pRxMsg->Data[2]),len);
			}
		} else {
			DebugPutString("\r\nWrong thing happen");
		} 
	} else {
		DebugPutString("\r\nODB2_TOYOTA_CAN_ISO_TP_Receive error");
	}
	return len;
}

/**
 * @brief ODB2_CAN_Init_Safe
 * @param highspeed
 * @param extended
 * @return 0 if failed,  CAN_STANDARD = 1 for standard OK, CAN_EXTENDED = 2 for extended
 */
#define TRY_H_SPEED_STD 0
#define TRY_H_SPEED_EXT 1
// #define TRY_L_SPEED_STD 2
// #define TRY_L_SPEED_EXT 3
int ODB2_CAN_Init_Safe(int speed){
//	ODB2_CAN_InitFilters(true);
	uint32_t stdMsgID = 0x7DF;
	uint32_t extMsgID = 0x18DB33F1;// response = 0x18DB11F1;
	HAL_StatusTypeDef status;
	uint8_t FIFO_number = CAN_FIFO1;
	CanRxMsgTypeDef* pRxMessage;
	
    uint8_t tryCounter = TRY_H_SPEED_STD;
    while(1)
    {
        if(speed == CAN_HIGHSPEED){ 
            FIFO_number = CAN_FIFO0;
            pRxMessage = hcan.pRxMsg;
        } else {
            FIFO_number = CAN_FIFO1;
            pRxMessage = hcan.pRx1Msg;
        }
        
        // ODB2_CAN_Setup_Speed(speed, CAN_MODE_SILENT);
        
        // ODB2_CAN_InitFilterAny(true, FIFO_number);
        // status = HAL_CAN_Receive(&hcan, FIFO_number, 1000);
        // status = HAL_OK;
        // ODB2_CAN_InitFilterAny(false, FIFO_number);
        
        // ODB2_CAN_DeInit();
        ODB2_CAN_Setup_Speed(speed, CAN_MODE_NORMAL);
        
        if(tryCounter == TRY_H_SPEED_STD) {speed = CAN_HIGHSPEED; hcan.pRxMsg->IDE = CAN_ID_STD; DebugPutString("\r\nTry CAN 11 bit\r\n"); tryCounter = TRY_H_SPEED_EXT; }
        else if(tryCounter == TRY_H_SPEED_EXT) {speed = CAN_HIGHSPEED;  hcan.pRxMsg->IDE = CAN_ID_EXT; DebugPutString("\r\nTry CAN 29 bit\r\n"); tryCounter = TRY_H_SPEED_STD; }
        // else if(tryCounter == TRY_L_SPEED_STD) {speed = CAN_LOWSPEED; hcan.pRx1Msg->IDE = CAN_ID_STD; tryCounter = TRY_L_SPEED_EXT; }
        // else if(tryCounter == TRY_L_SPEED_EXT) {speed = CAN_HIGHSPEED; hcan.pRx1Msg->IDE = CAN_ID_EXT; tryCounter = TRY_H_SPEED_STD; }
        // if(status == HAL_OK){
            if(FIFO_number == CAN_FIFO0){
                printCANmsg(hcan);
            } else printCANmsg1(hcan);
            if(pRxMessage->IDE==CAN_ID_STD){
                ODB2_CAN_InitFilter11bit(true,FIFO_number);
                while(HAL_CAN_Receive(&hcan, FIFO_number,100)==HAL_OK);
                /* Try Standard CAN 11 bit */
                ODB2_CAN_Transmit(stdMsgID, 0x01, 0x00, CAN_STANDARD);
                status = HAL_CAN_Receive(&hcan, FIFO_number, 1000);
                if(status != HAL_OK){
                        //Reception Error
                        sprintf(tmpStr, "\r\n CanStd tried error 0x%02X ", (int)status);
                        DebugPutString(tmpStr);
                } else {
                    
                    if(FIFO_number == CAN_FIFO0){
                        printCANmsg(hcan);
                    } else {
                        printCANmsg1(hcan);
                    }
                    if(	pRxMessage->IDE==CAN_ID_STD && pRxMessage->Data[0]>=0x06 && pRxMessage->Data[1]==0x41 && pRxMessage->Data[2]==0x00) {
                                if(	pRxMessage->Data[3]!=0x00 || pRxMessage->Data[4]!=0x00 || pRxMessage->Data[5]!=0x00 || pRxMessage->Data[6]!=0x00)
                                    return CAN_STANDARD;
                    } else if (	pRxMessage->IDE==CAN_ID_STD && 
                                            pRxMessage->Data[0]==0x07 &&	pRxMessage->Data[1]==0xFF && pRxMessage->Data[2]==0xFF &&	pRxMessage->Data[3]==0xFF && 
                                            pRxMessage->Data[4]==0xFF &&	pRxMessage->Data[5]==0xFF && pRxMessage->Data[6]==0x25 && pRxMessage->Data[7]==0x07){
                        return CAN_REQUEST_BURNIN;
                    }
                }			
                osDelay(1000);
                /*Try Toyota CAN*/
                stdMsgID = 0x7E0;
                uint8_t TxMessage[8] = {0x21, 0x00};
                ODB2_CAN_TransmitMessage(0x7E0,CAN_STANDARD,TxMessage,2);
                status = HAL_CAN_Receive(&hcan, FIFO_number, 1000);
                if(status != HAL_OK) {
                    //Reception Error
                    sprintf(tmpStr, "\r\nToyota Can tried error 0x%02X ", (int)status);
                    DebugPutString(tmpStr);
                } else {
                    DebugPutString("\r\nToyota can init response");
                    CanRxMsgTypeDef* pRxMessage;
                    if(FIFO_number == CAN_FIFO0){
                        printCANmsg(hcan);
                    } else {
                        printCANmsg1(hcan);
                    }
                    
                    if(	pRxMessage->IDE==CAN_ID_STD && pRxMessage->Data[0]>=0x06 && pRxMessage->Data[1]==0x61 && pRxMessage->Data[2]==0x00) {
                                if(	pRxMessage->Data[3]!=0x00 || pRxMessage->Data[4]!=0x00 || pRxMessage->Data[5]!=0x00 || pRxMessage->Data[6]!=0x00)
                                    return CAN_TOYOTA;
                    } else if (	pRxMessage->IDE==CAN_ID_STD && 
                                            pRxMessage->Data[0]==0x07 &&	pRxMessage->Data[1]==0xFF && pRxMessage->Data[2]==0xFF &&	pRxMessage->Data[3]==0xFF && 
                                            pRxMessage->Data[4]==0xFF &&	pRxMessage->Data[5]==0xFF && pRxMessage->Data[6]==0x25 && pRxMessage->Data[7]==0x07){
                        return CAN_REQUEST_BURNIN;
                    }
                }
                ODB2_CAN_InitFilter11bit(false,FIFO_number);
            } else if (pRxMessage->IDE==CAN_ID_EXT) {
                ODB2_CAN_InitFilter29bit(true,FIFO_number);
                while(HAL_CAN_Receive(&hcan, FIFO_number,1)==HAL_OK);
                /* Try Extended CAN 29 bit */
                ODB2_CAN_Transmit(extMsgID, 0x01, 0x00, CAN_EXTENDED);
                status = HAL_CAN_Receive(&hcan, FIFO_number, 1000);
                if(status != HAL_OK){
                        //Reception Error
                        sprintf(tmpStr, "\r\n CanEx tried error 0x%02X ", (int)status);
                        DebugPutString(tmpStr);
                } else {
                    CanRxMsgTypeDef* pRxMessage;
                    if(FIFO_number == CAN_FIFO0){
                        printCANmsg(hcan);
                    } else {
                        printCANmsg1(hcan);
                    }
                    if(	pRxMessage->IDE==CAN_ID_EXT && pRxMessage->Data[0]>=0x06 && pRxMessage->Data[1]==0x41 && pRxMessage->Data[2]==0x00){
                                if(	pRxMessage->Data[3]!=0x00 || pRxMessage->Data[4]!=0x00 || pRxMessage->Data[5]!=0x00 || pRxMessage->Data[6]!=0x00)
                                    return CAN_EXTENDED;
                    }else if (	pRxMessage->IDE==CAN_ID_EXT && 
                                            pRxMessage->Data[0]==0x07 &&	pRxMessage->Data[1]==0xFF && pRxMessage->Data[2]==0xFF &&	pRxMessage->Data[3]==0xFF && 
                                            pRxMessage->Data[4]==0xFF &&	pRxMessage->Data[5]==0xFF && pRxMessage->Data[6]==0x25 && pRxMessage->Data[7]==0x07){
                        return CAN_REQUEST_BURNIN;
                    }
                }
                ODB2_CAN_InitFilter29bit(false,FIFO_number);
            } else {
                DebugPutString("\r\nERRROR Can Rx Message undefined ID type");
            }
        // } else {
        //     DebugPutString("\r\nNo Can messsage Readed");
        // }
    }
  return 0;
}


