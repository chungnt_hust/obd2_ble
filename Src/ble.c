/**
 * @file debug.c
 * @brief debug task via uart for stm32f1 (odbi adapter)
 * @date April 20, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */
#include "ble.h"
#include "debug.h"
#include "utils.h"
#include "protocol.h"
#include "can.h"
#include "trip_data.h"

extern osSemaphoreId g_ODB2_Semid;
extern osSemaphoreId g_BLESemid;
extern ODB2Message g_ODB2Msg;
extern ODB2_PROTOCOL g_ODB2Protocol;
extern QueueHandle_t xRxBleQueue;
extern bool g_ObdInitingCustom;
extern bool g_InfoAutoSend;
extern bool g_gyro;
extern uint32_t batt;
extern float g_battValue;
extern float g_battOffset;
extern float g_battA, g_battB;
extern float g_battSleepValue;
extern uint8_t isWaitingForWifiTurnedOf;

static char tmpStr[128];
uint16_t u16BleRxCount = 0;
uint8_t pu8BleBuf[BLE_RX_BUF_SIZE+2];
uint8_t pu8BleBufTrimed[BLE_RX_BUF_SIZE+2];
uint8_t extra_byte;
BLEMode g_BleMode = BLE_MODE_NORMAL;
osThreadId bleLoopTaskHandle;

uint8_t u8NumberPids = 0;
uint8_t pu8LoopPidList[16];

uint8_t HLK_MAC[6];
bool b_MAC = false;
uint8_t SIMCOM_TMP = 0xFF;

extern bool g_authen;
extern bool g_authenFistStep;

#define HEX_MSG_BOOTHOOK    ":00000012EE\n"
char g_hex_msg_boothook[] = HEX_MSG_BOOTHOOK;

float f_TripDistance = 0;	// in meters
float f_TripStopTime = 0; // in seconds
uint32_t u32TripDistance = 0; // in meters
uint32_t u32TripStopTime = 0; // in seconds
bool b_TripStart = false;
uint8_t u8Speed;
uint16_t u16LastSpeed;
uint32_t u32LastTickRecord;
float f_TripDistanceSended;
float f_TripStopTimeSended;
float f_TripDistanceSaved;

void BleLoopTask(void const* argument)
{
	static int i = 0;
	static uint8_t u8Mode = 0x01;
	static uint8_t u8Pid = 0x00;
	static uint32_t tick;
	static uint32_t u32_time;
	static float f_S;
	int iRet;
	while (true) {
		DebugPutChar('X');
		if(g_ODB2Protocol != ODB2_PROTOCOL_START){
		//if(false){
			g_ODB2Msg.u8Mode = 0x01;
			g_ODB2Msg.u8PID = 0x0D;
			g_ODB2Msg.u8PidBytes = 1;
			osSemaphoreRelease(g_ODB2_Semid);
			iRet = osSemaphoreWait(g_BLESemid, 2000);
			if (iRet != osOK) {
				u8Speed = 0;
				if(b_TripStart) {
					tick = HAL_GetTick();
					u32_time = tick - u32LastTickRecord;
					if(u32_time>10000) {
						DebugPutString("\r\nSomething wrong in tick");
						u32_time = 2000;
					}
					f_TripStopTime += (float)u32_time / 1000;
					u32LastTickRecord = tick;
				}	
				DebugPutString("\r\n Ble No response from ODB2 Thread");
			} else {
				if(g_ODB2Msg.u8RespLen==3 && g_ODB2Msg.pu8Data[1] == 0x0D) {
					tick = HAL_GetTick();
					u8Speed = g_ODB2Msg.pu8Data[2];
					if(b_TripStart) {
						u32_time = tick - u32LastTickRecord;
						if(u32_time>10000) {
							DebugPutString("\r\nSomething wrong in tick");
							u32_time = 100;
						}
						if(u8Speed > 0){
							f_S = (float)(u8Speed * u32_time) / 3600;
							f_TripDistance += f_S;
						} else {
							f_TripStopTime += (float)u32_time / 1000;
						}
						u32LastTickRecord = tick;
						
					} else osDelay(400);
				}
			}
		}
		osDelay(100);
	}
}

void Ble_FixPutString(char *str, uint16_t len)
{
	HAL_UART_Transmit(&UART_BLE, (uint8_t *)str, len, 0xFFFF);
}

void BlePutString(char *str)
{
#if 1
    while(*str !='\0')
    {
        HAL_UART_Transmit(&UART_BLE, (uint8_t *)str, 1, 0xFFFF);
        str++;
    }
#endif
}

void BlePutChar(char c)
{
#if 1
    HAL_UART_Transmit(&UART_BLE, (uint8_t*) &c, 1, 0xFFFF);
#endif
}

HAL_StatusTypeDef BleReadChar(char* c)
{
    return HAL_UART_Receive(&UART_BLE, (uint8_t*) c, 1, 0xFFFF);
}

void BleProcessReceiveByte(uint8_t c)
{
    pu8BleBuf[u16BleRxCount++] = c;
    int i = 0; //temp index
    int iRet = 0;// result;

    if (c == '\n' || u16BleRxCount >= BLE_RX_BUF_SIZE)
    {
        sprintf(tmpStr, "\r\nBle: Received Full (Length = %d): ", u16BleRxCount);
        DebugPutString(tmpStr);
        for ( i = 0; i < u16BleRxCount; i++)
        {
            DebugPutChar(pu8BleBuf[i]);
        }
				if(pu8BleBuf[0]==0xaa){
					if(!g_authen) {
						NotAuthenResponse();
						return;
					}
					switch(pu8BleBuf[1]){
						case 0x01:
							iRet=BleMessageHandleF4(pu8BleBuf, u16BleRxCount);
							if (iRet == BLE_MESSAGE_HANDLE_OK){
								DebugPutString("\r\nF4 message handle ok");
							} else {
								DebugPutString("\r\nF4 message handle nok");
							}
							break;
						case 0x02:
							BleMessageHandleReset();	break;
						case 0x22:
							BleResponseSpecialMessageAA(pu8BleBuf[1]);
							BleMessageHandleProtocol_CAN11_250();	break;
						case 0x23:
							BleResponseSpecialMessageAA(pu8BleBuf[1]);
							BleMessageHandleProtocol_CAN29_250();	break;
						case 0x24:
							BleResponseSpecialMessageAA(pu8BleBuf[1]);
							BleMessageHandleProtocol_CAN11_500(); break;
						case 0x25:
							BleResponseSpecialMessageAA(pu8BleBuf[1]);
							BleMessageHandleProtocol_CAN29_500(); break;
						case 0x26:
							BleResponseSpecialMessageAA(pu8BleBuf[1]);
							BleMessageHandleProtocol_KWP2000_FAST(); break;
						case 0x27:
							BleResponseSpecialMessageAA(pu8BleBuf[1]);
							BleMessageHandleProtocol_KWP2000_SLOW(); break;
						case 0x28:
							BleResponseSpecialMessageAA(pu8BleBuf[1]);
							BleMessageHandleProtocol_ISO9141();				break;
						case 0x29:
							BleResponseSpecialMessageAA(pu8BleBuf[1]);
							BleMessageHandleProtocol_VPW(); break;
						case 0x2A:
							BleResponseSpecialMessageAA(pu8BleBuf[1]);
							BleMessageHandleProtocol_PWM(); break;
						case 0xFF:
							BleResponseSpecialMessageAA(pu8BleBuf[1]);
							BleMessageHandleProtocol_START(); break;
						default: break;
					}
				}
				//else if (g_ODB2Protocol != ODB2_PROTOCOL_START) {
				else {
            iRet = BleMessageHandleFull(pu8BleBuf, u16BleRxCount);
            //iRet = messageHandle(pu8BleBuf, u16BleRxCount, MESSAGE_TYPE_BLE);
            if (iRet == BLE_MESSAGE_HANDLE_OK) {
								if (g_ObdInitingCustom ) {
									//just dont do any thing
								} else if (g_BleMode == BLE_MODE_NORMAL) {
                    // Release Semaphore for other Task
                    osSemaphoreRelease(g_ODB2_Semid);
                    // Wait for return osSemaphoreRelease
                    if (g_ODB2Msg.u8Mode != 0x03){
											iRet = osSemaphoreWait(g_BLESemid, 2000);
										} else {
											iRet = osSemaphoreWait(g_BLESemid, 4000);
										}
                    if (iRet != osOK) {
                        // maybe timeout --> reply to ble NODATA
                        DebugPutString("\r\n Ble No response from ODB2 Thread");
												if (g_ODB2Protocol != ODB2_PROTOCOL_START) {
													BleReplyNoData();
												} else {
													BlePutString("NO OBD\r\n");
													BlePutChar('\0');
												}
                    } 
										else {
                        // TODO: reply to ble odb2 message response
												DebugPutString("\r\nProcess byte");
                        BleReplySuccessRequest();
                        //DebugPutString("\r\n Ble received response from ODB2 Thread");
                    }
                }
            } else if ( iRet == BLE_MESSAGE_HANDLE_FAIL ) {
                if (g_ODB2Msg.u8Status == ODB2_STATUS_START) {
                    DebugPutString("\r\n Ble Message Handle Fail");
                    // reply to ble NODATA
                    //BleReplyNoData();
                }
            } else if (iRet == BLE_MESSAGE_HANDLE_CMD) {
                DebugPutString("\r\n Ble Message Handle CMD");
								g_ODB2Msg.u8ExtraBytes = 0;
                g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
                //BleReplySuccessRequest();
            }
        } 
        u16BleRxCount = 0; // reset count
        memset(pu8BleBuf, 0x00, sizeof(pu8BleBuf));
    }
		if (c == '\0' || u16BleRxCount >= 12) {
			if (memcmp(&pu8BleBuf[u16BleRxCount-11],g_hex_msg_boothook, 11) == 0) {
				u16BleRxCount = 0; // reset count
				DebugPutString("\r\nReset System\n");
					//JUMP_TO_APP(BOOT_START_ADDRESS);
				NVIC_SystemReset();
			}
		}
}

void BleProcessTask(void){
    uint8_t xC;
    osThreadDef(bleLoopTask, BleLoopTask, osPriorityNormal, 0, 512);
    bleLoopTaskHandle = osThreadCreate(osThread(bleLoopTask), NULL);
    while (1)
    {		
//				WRITE_REG(IWDG->KR,IWDG_KEY_RELOAD);
        if (xRxBleQueue != 0)
        {
            if( xQueueReceive( xRxBleQueue, &( xC ), ( TickType_t ) 10 ) )
            {
                // Receive a message from queue, process the byte
                BleProcessReceiveByte(xC);
            }
        }
    }
}

int BleMessageHandleFull(uint8_t* pu8Data, uint32_t u32Len)
{
    uint8_t data[BLE_MESSAGE_MAX_BYTES]; // buffer to store converted message (remove space, up->lowcase)
    uint16_t u16Count = 0; // count the number of characters after convert
    int i;
    char temp;
    int iRet;
    // Process DEBUG or BLE message

    // check for last 2 character is \r\n or not
    if (u32Len < 2)
    {
        return BLE_MESSAGE_HANDLE_FAIL;
    }
    if ((pu8Data[u32Len -2] != '\r') || (pu8Data[u32Len-1] != '\n'))
    {
			DebugPutString("\r\nError CR LF");
        return BLE_MESSAGE_HANDLE_FAIL;
    }

    // remove all space and convert to lowcase character
    for (i = 0; i < u32Len; i++)
    {
        temp = toLowerCase(pu8Data[i]);
        if (temp != ' ' && temp != '\t')
            data[u16Count++] = temp;
    }
    // check for AT command
    if (u32Len > 3)
    {
        if (data[0] == 'a' && data[1] == 't')
        {
						if(!g_authen) {
							NotAuthenResponse();
							return BLE_MESSAGE_HANDLE_FAIL;
						}
            iRet = BleMessageHandleAT(&data[2], u32Len -2);
            if (!iRet)
            {
                DebugPutString("messageHandleAT failed \r\n");
            }
            return iRet;
        }
        else
        {
            iRet = BleMessageHandlePID(data, u32Len);
            if (iRet == BLE_MESSAGE_HANDLE_FAIL)
            {
                DebugPutString("messageHandlePID failed \r\n");
            }
            return iRet;
        }
    }
    //return BLE_MESSAGE_HANDLE_OK;
    return BLE_MESSAGE_HANDLE_FAIL;
}

int BleMessageHandleAT(uint8_t* pu8Data, uint16_t u16Size)
{
    g_ODB2Msg.u8Status = ODB2_STATUS_START;
    if (u16Size >= 1) {
        if (pu8Data[0] == 'z')
        {
            BlePutString(ATZ_RETURN);
            BlePutString("   \r");
            DebugPutString(ATZ_RETURN);
            DebugPutString("   \r");
            g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
            return BLE_MESSAGE_HANDLE_OK;
        }
        if (pu8Data[0]=='b'&&pu8Data[1]=='d')
        {
            //atbd

            BlePutString(ATZ_RETURN);
            BlePutString("   \r");
            DebugPutString(ATZ_RETURN);
            DebugPutString("   \r");
            g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
            return BLE_MESSAGE_HANDLE_OK;
        }
        else return BLE_MESSAGE_HANDLE_FAIL;
    }
    return BLE_MESSAGE_HANDLE_FAIL;
}

int BleMessageHandlePID(uint8_t* pu8Data, uint16_t u16Size)
{
    g_ODB2Msg.u8Status = ODB2_STATUS_START;
    sprintf(tmpStr, "BleMessageHandlePID size: %d\r\n", u16Size);
    DebugPutString(tmpStr);
    int i;
    //    for (i = 0; i < u16Size; i++) {
    //        sprintf(tmpStr, "pu8Data[%d] = %c\r\n", i, (char)(pu8Data[i]));
    //        DebugPutString(tmpStr);
    //    }
    uint32_t u32ID = 0x7DF;
    uint8_t u8Mode = 0x01;
    uint8_t u8PID = 0x00;
    uint8_t u8NumberAdded = 0;
    if (u16Size <2)
        return BLE_MESSAGE_HANDLE_FAIL;
    //u8IsBLEWaitingCAN = true;
    if (pu8Data[0] == '0' && pu8Data[1] == '3')
    {
			if(!g_authen) {
					NotAuthenResponse();
					return BLE_MESSAGE_HANDLE_FAIL;
			}
			if (g_ODB2Protocol == ODB2_PROTOCOL_START) {
				return BLE_MESSAGE_HANDLE_FAIL;
			}
        ///check_response=1;
        DebugPutString("DTC request\r\n");
				// process other data
		
        //MX_CAN_Transmit(u32ID,03,00);
        //TODO: Update ODB2Msg
        g_ODB2Msg.u8Mode = 0x03;
        g_ODB2Msg.u8PidBytes = 0;
        return BLE_MESSAGE_HANDLE_OK;
    }
    // process command
		if(pu8Data[0] == '9' && pu8Data[1] == '9') {
			if (u16Size >=6) {
				uint8_t pid;
				if(!hextoByte(&pu8Data[2],&pid)) return BLE_MESSAGE_HANDLE_FAIL;
				switch(pid){
					case 0x00:
						//start trip
						BleHandleStartTrip(pu8Data, u16Size);
						return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
					case 0x01:
						//stop trip
						g_InfoAutoSend = false;
						BleHandleStopTrip(pu8Data, u16Size);
						return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
					case 0x02:
						//trip update
						BleHandleTripUpdate(pu8Data, u16Size);
						return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
					case 0x03:
						//set speed ratio
						BleHandleSetSpeedRatio(pu8Data, u16Size);
						return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
					case 0x04:
						// set Trip Index
						BleHandleTripIndex(pu8Data, u16Size);
					return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
					default:
						return BLE_MESSAGE_HANDLE_FAIL;
				}
			}
		}
    if (pu8Data[0] == '0' && pu8Data[1] == '0') {
        if (u16Size >=6) {
					
          g_ODB2Msg.u8Mode = 0x00;
          g_ODB2Msg.u8PidBytes = 0;
						
					uint8_t pid;
					if(!hextoByte(&pu8Data[2],&pid)) return BLE_MESSAGE_HANDLE_FAIL;
					
					if(pid == 0x10) {
						//first step of authen: receive Random String and send back with Special Key
						if(g_authen) {
							BlePutString("0010 OK\r\n");
							BlePutChar('\0');
							DebugPutString("\r\nMCU authen OK");
						} else {
							AuthenFirstStep(pu8Data, u16Size);
						}
						return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
					} else if (pid == 0x11) {
						//second step of authen: receive Random String and check reverse Special Key, return authen result
						AuthenSecondStep(pu8Data, u16Size);
						return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
					}
					if(!g_authen) {
						NotAuthenResponse();
						return BLE_MESSAGE_HANDLE_FAIL;
					}
					switch(pid) {
						case 0x01:{ //add Pid to bleloop task //
							BleHandleAddPidToAutoLoop(pu8Data,u16Size);
							break;
						}
						case 0x00:{	//stop bleloop task//
							BleHandleStopAutoLoop(pu8Data,u16Size);
							break;
						}
						case 0x02: { //start bleloop task//
							BleHandleStartAutoLoop(pu8Data,u16Size);
							DebugPutString("\r\n Start auto loop");
							break;
						}
						case 0x04: { //reset and stop autoloop//
							BleHandleStopAndResetAutoLoop(pu8Data,u16Size);
							break;
						} 
						case 0x03: { // Power save mode // 
							if(g_ODB2Protocol != ODB2_BURNIN_TEST)
									BleHandlePowerSave(pu8Data,u16Size);
							break;
						} 
						case 0x0a: { // Request OBD protocol // 
							DebugPutString("\r\nRequest OBD protocol");
							sprintf(tmpStr, "000A %02x\r\n",g_ODB2Protocol);
							BlePutString(tmpStr);
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x0d: { // Request Battery Voltage //
							DebugPutString("\r\nRequest battery voltage");
//							g_gyroRequest = true;
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x0e: { // Request Ping Pong//
							if(pu8Data[4]=='p' && pu8Data[5]=='i' && pu8Data[6]=='n' && pu8Data[7]=='g'){
								DebugPutString("\r\nRequest PING PONG");
								//get UID send back to pong message
								PingRequestHandle();
							} else if (pu8Data[4]=='p' && pu8Data[5]=='o' && pu8Data[6]=='n' && pu8Data[7]=='g') {
								DebugPutString("\r\nResponse PING PONG");
								PongResponseHandle(&pu8Data[8]);
							}
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x0f: { // Calib battery voltage //
//							strcpy(tmpStr,(char*)&pu8Data[4]);
//							float RxVoltage = atof(tmpStr)/100;
//							float voltage = (float)batt*DEFAULT_BATT_A_PARAM + DEFAULT_BATT_B_PARAM;
//							sprintf(tmpStr,"\r\n Calib battery request %f %f",RxVoltage,voltage);
//							DebugPutString(tmpStr);
//							if(abs(RxVoltage-voltage) > 0.5) {
//								sprintf(tmpStr,"NOK");
//								BleResponseSpecialMessageMode00(0x0f,(uint8_t*)tmpStr,strlen(tmpStr));
//							} else {
//								sprintf(tmpStr,"OK");
//								BleResponseSpecialMessageMode00(0x0f,(uint8_t*)tmpStr,strlen(tmpStr));
//								float a = (RxVoltage - DEFAULT_BATT_B_PARAM) / batt;
//								g_battOffset = a - DEFAULT_BATT_A_PARAM;
//								g_battA = DEFAULT_BATT_A_PARAM + g_battOffset;
//								UpdateFlash();
//							}
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x13: { // Permit auto send Gyro and Battery Voltage information//
							DebugPutString("\r\nPermit auto send Gyro and Battery Voltage information");
							BleResponseSpecialMessageMode00(0x13,NULL,0);
							g_InfoAutoSend = true;
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x14: { // Check Gyro status //
							DebugPutString("\r\nCheck Gyro status");
							if(g_gyro){
								sprintf(tmpStr,"OK");
								BleResponseSpecialMessageMode00(0x14,(uint8_t*)tmpStr,strlen(tmpStr));
							} else {
								sprintf(tmpStr,"NOK");
								BleResponseSpecialMessageMode00(0x14,(uint8_t*)tmpStr,strlen(tmpStr));
							}
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x19:{
							DebugPutString("\r\n control MCU_POWER_PIN");
							if(strstr((char*)&pu8Data[4],"ON") || strstr((char*)&pu8Data[4],"on")) {
								DebugPutString("\r\nMCU_POWER_PIN to HIGH");
								MCU_POWER_TURN_OFF();
							} else {
								DebugPutString("\r\nMCU_POWER_PIN to LOW");
								MCU_POWER_TURN_ON();
							}
							BleResponseSpecialMessageMode00(0x19,NULL,0);
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x1b: {
							DebugPutString("\r\nReceive SIMCOM temperature response");
							int simcom_tmp = atoi((char*)&pu8Data[4]);
							if(simcom_tmp<0) SIMCOM_TMP = 0xFF;
							else SIMCOM_TMP = (uint8_t)simcom_tmp;
							sprintf(tmpStr,"\r\nSIMCOM tmp = 0x%02x", SIMCOM_TMP);
							DebugPutString(tmpStr);
//							osSemaphoreRelease(g_MasterOfHlkSemid);
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x22:{
							BleResponseSpecialMessageMode00(0x22,NULL,0);
							BleMessageHandleProtocol_CAN11_250();
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x23:{
							BleResponseSpecialMessageMode00(0x23,NULL,0);
							BleMessageHandleProtocol_CAN29_250();
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x24: {
							BleResponseSpecialMessageMode00(0x24,NULL,0);
							BleMessageHandleProtocol_CAN11_500();
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x25:{
							BleResponseSpecialMessageMode00(0x25,NULL,0);
							BleMessageHandleProtocol_CAN29_500();
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x26:{
							BleResponseSpecialMessageMode00(0x26,NULL,0);
							BleMessageHandleProtocol_KWP2000_FAST();
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x27:{
							BleResponseSpecialMessageMode00(0x27,NULL,0);
							BleMessageHandleProtocol_KWP2000_SLOW();
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x28:{
							BleResponseSpecialMessageMode00(0x28,NULL,0);
							BleMessageHandleProtocol_ISO9141();
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x29:{
							BleResponseSpecialMessageMode00(0x29,NULL,0);
							BleMessageHandleProtocol_VPW();
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x2A:{
							BleResponseSpecialMessageMode00(0x2A,NULL,0);
							BleMessageHandleProtocol_PWM();
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						case 0x2F:{
							BleResponseSpecialMessageMode00(0x2F,NULL,0);
							BleMessageHandleProtocol_START();
							return BLE_MESSAGE_HANDLE_SPECIAL_CMD;
						}
						default:
							return BLE_MESSAGE_HANDLE_FAIL;
					}
						
        }
        return BLE_MESSAGE_HANDLE_FAIL;
    }
    
    if (u16Size >= 6 ) // get mode & pid
    {
				if(!g_authen) {
					NotAuthenResponse();
					return BLE_MESSAGE_HANDLE_FAIL;
				}
        // check for supported mode 01->0A
        if (pu8Data[4] == '1') extra_byte = '1';
        else extra_byte = 0;
        if (hextoByte(pu8Data, &u8Mode) && hextoByte(pu8Data+2, &u8PID))
        {
            //check_response=1;
            //MX_CAN_Transmit(u32ID,u8Mode,u8PID);
            // TODO: Update ODB2Msg
            g_ODB2Msg.u8Mode = u8Mode;
            g_ODB2Msg.u8PID = u8PID;
            g_ODB2Msg.u8PidBytes = 1;
						// process other data
						if (g_ODB2Protocol == ODB2_PROTOCOL_START || g_ODB2Protocol == ODB2_BURNIN_TEST || g_ODB2Protocol == ODB2_PROTOCOL_END) {
							return BLE_MESSAGE_HANDLE_FAIL;
						}
            return BLE_MESSAGE_HANDLE_OK;
        }
        // check extra byte example: 010D1\r\n --> extra byte = 1  ==> response should be
        // 010D1\r ...
    }
    return BLE_MESSAGE_HANDLE_FAIL;
}

void BleReplyNoData()
{
    sprintf(tmpStr, "%02X%02X\r\n", g_ODB2Msg.u8Mode,g_ODB2Msg.u8PID);
    BlePutString(tmpStr);
    DebugPutString(tmpStr);

    BlePutString("NO DATA\r\n\r>>\n");
    DebugPutString("NO DATA\r\n\r>>\n");

    BlePutChar(0x00);
    DebugPutChar(0x00);
}

/**
 * @brief BleReplySuccessRequest
 * example with six data bytes 06 41 00 BF 3F B8 13 in return (06 is number of bytes)
 * the send message will be devided into 5 message
 * first 	"01 00\r\n"
 * second 	"41 00 BF\n"
 * third		" 3F B8 1\n"
 * fourth	"3 \r\n"
 * fifth		"\r>\n"
 * 3 messages second, third and fourth are from "41 00 BF 3F B8 13"
 */
void BleReplySuccessRequest()
{
    static uint8_t pu8DataSend[64];
		static uint32_t tick;
		static uint32_t u32_time;
		static float f_S;
    // (can, sae, kwp) in g_ODB2Msg
    // example: g_ODB2Msg.pu8Data = [41 00 BF BF A8 91]
    // g_ODB2Msg.u8DLC = 6
    uint8_t u8ServiceID = g_ODB2Msg.pu8Data[0];
    uint8_t u8PID = g_ODB2Msg.pu8Data[1];
    uint8_t u8SentMode = g_ODB2Msg.u8Mode;
    uint8_t u8SentPID = g_ODB2Msg.u8PID & 0xFF;
    uint8_t u8DataLen = g_ODB2Msg.u8RespLen;
    sprintf(tmpStr, "\r\n BleReplySuccessRequest u8DataLen = %d, u8Status=%d\r\n", u8DataLen, g_ODB2Msg.u8Status);
		DebugPutString(tmpStr);
		int count, i;
    static int iSentBle = 0;
    if (g_ODB2Msg.u8Status == ODB2_STATUS_PROCESS_END)	{
        dumpHex(g_ODB2Msg.pu8Data, g_ODB2Msg.u8RespLen);
				/**/
				if(b_TripStart && g_ODB2Msg.u8RespLen==3 && g_ODB2Msg.pu8Data[1] == 0x0D) {
					tick = HAL_GetTick();
					u8Speed = g_ODB2Msg.pu8Data[2];
					u32_time = tick - u32LastTickRecord;
					if(u32_time>10000) {
						DebugPutString("\r\nSomething wrong in tick");
						u32_time = 100;
					}
					if(u8Speed > 0){
						f_S = (float)(u8Speed * u32_time) / 3600;
						f_TripDistance += f_S;
					} else {
						f_TripStopTime += (float)u32_time / 1000;
					}
					u32LastTickRecord = tick;
				}
        if (extra_byte == 0) {
            sprintf(tmpStr, "%02X%02X\r\n", u8SentMode,u8SentPID);//lack space between u8SentMode & u8SentPID
        }
        else {
            sprintf(tmpStr, "%02X%02X%c\r\n", u8SentMode,u8SentPID, extra_byte);//lack space between u8SentMode & u8SentPID
        }

        BlePutString(tmpStr);
        DebugPutString(tmpStr);
        //put data into string following the structure : 2 byte data + 1 space and \r in the end of string
        memset(pu8DataSend,NULL,64);//reset string pu8DataSend
        count=0;
					//for(i=0;i<u8DataLen;i++)
				for(i=0;i<u8DataLen;i++)	{
						hex2char(&pu8DataSend[count],g_ODB2Msg.pu8Data[i]);
						count+=3;
				}
        pu8DataSend[count]='\r';
        //count++;
        for(i=0;i<=count;i++)	{
            if ((i%8)==7)	{
                BlePutChar(pu8DataSend[i]);
                BleEndLine();
                DebugPutChar(pu8DataSend[i]);
                DebugEndLine();
            }	else	{
                BlePutChar(pu8DataSend[i]);
                DebugPutChar(pu8DataSend[i]);
            }
        }
        BleEndLine();
        BlePutChar(0x0D);
        BlePutChar('>');//xoa mot ky tu \r
        BleEndLine();// \n
#if USE_00_TERMINATE > 0
        BlePutChar(0x00);
#endif
        DebugEndLine();
        DebugPutChar(0x0D);
        DebugPutChar('>');
        DebugEndLine();
				
				
    } else {
        DebugPutString("\r\nFuck");
        BleReplyNoData();
    }
}

void BleEndLine(void)
{
    BlePutChar(0x0A);
}

int BleMessageHandleF4( uint8_t* pu8Data, uint16_t u16Size ){
	for ( int i=0; i<u16Size ;i++ ){
		BlePutChar(pu8Data[i]);
	}
	BlePutChar('\0');
	if ( u16Size!= 16){
		return BLE_MESSAGE_HANDLE_FAIL;
	}
	return BLE_MESSAGE_HANDLE_OK;
}

int BleMessageHandleReset(void){
	DebugPutString("\r\nSOFT RESET, MCU reset after");
	DebugPutString("\r\n5");
	DebugPutString("\r\n4");
	DebugPutString("\r\n3");
	DebugPutString("\r\n2");
	DebugPutString("\r\n1");
	NVIC_SystemReset();
	return BLE_MESSAGE_HANDLE_OK;
}
int BleMessageHandleProtocol_CAN11_500(void){
	g_ODB2Protocol = ODB2_PROTOCOL_CAN11_500;
	DebugPutString("\r\nBleMessageHandleProtocol_CAN11_500");
	g_ObdInitingCustom = true;
	osThreadState threadState;
	threadState = osThreadGetState(canTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(canTaskHandle);
	}
	threadState = osThreadGetState(kwpTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(kwpTaskHandle);
	}
	threadState = osThreadGetState(saeTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(saeTaskHandle);
	}
	osThreadDef(canTask, CanTask, osPriorityNormal, 0, 256);
  canTaskHandle = osThreadCreate(osThread(canTask), NULL);
	return BLE_MESSAGE_HANDLE_OK;
}
int BleMessageHandleProtocol_CAN11_250(void){
	g_ODB2Protocol = ODB2_PROTOCOL_CAN11_250;
	DebugPutString("\r\nBleMessageHandleProtocol_CAN11_250");
	g_ObdInitingCustom = true;
	osThreadState threadState;
	threadState = osThreadGetState(canTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(canTaskHandle);
	}
	threadState = osThreadGetState(kwpTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(kwpTaskHandle);
	}
	threadState = osThreadGetState(saeTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(saeTaskHandle);
	}
	osThreadDef(canTask, CanTask, osPriorityNormal, 0, 256);
  canTaskHandle = osThreadCreate(osThread(canTask), NULL);	
	return BLE_MESSAGE_HANDLE_OK;
}
int BleMessageHandleProtocol_CAN29_500(void){
	g_ODB2Protocol = ODB2_PROTOCOL_CAN29_500;
	DebugPutString("\r\nBleMessageHandleProtocol_CAN29_500");
	g_ObdInitingCustom = true;

	osThreadState threadState;
	threadState = osThreadGetState(canTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(canTaskHandle);
	}
	threadState = osThreadGetState(kwpTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(kwpTaskHandle);
	}
	threadState = osThreadGetState(saeTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(saeTaskHandle);
	}
	osThreadDef(canTask, CanTask, osPriorityNormal, 0, 256);
  canTaskHandle = osThreadCreate(osThread(canTask), NULL);
	return BLE_MESSAGE_HANDLE_OK;
}
int BleMessageHandleProtocol_CAN29_250(void){
	g_ODB2Protocol = ODB2_PROTOCOL_CAN29_250;
	DebugPutString("\r\nBleMessageHandleProtocol_CAN29_250");
	g_ObdInitingCustom = true;
	
	osThreadState threadState;
	threadState = osThreadGetState(canTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(canTaskHandle);
	}
	threadState = osThreadGetState(kwpTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(kwpTaskHandle);
	}
	threadState = osThreadGetState(saeTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(saeTaskHandle);
	}
	osThreadDef(canTask, CanTask, osPriorityNormal, 0, 256);
  canTaskHandle = osThreadCreate(osThread(canTask), NULL);
	return BLE_MESSAGE_HANDLE_OK;
}
int BleMessageHandleProtocol_ISO9141(void){
	g_ODB2Protocol = ODB2_PROTOCOL_ISO9141;
	DebugPutString("\r\nBleMessageHandleProtocol_ISO9141");
	g_ObdInitingCustom = true;
	osThreadState threadState;
	threadState = osThreadGetState(canTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(canTaskHandle);
	}
	threadState = osThreadGetState(kwpTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(kwpTaskHandle);
	}
	threadState = osThreadGetState(saeTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(saeTaskHandle);
	}
	osThreadDef(kwpTask, KwpTask, osPriorityNormal, 0, 256);
  kwpTaskHandle = osThreadCreate(osThread(kwpTask), NULL);
	return BLE_MESSAGE_HANDLE_OK;
}
int BleMessageHandleProtocol_KWP2000_FAST(void){
	g_ODB2Protocol = ODB2_PROTOCOL_KWP2000_FAST;
	DebugPutString("\r\nBleMessageHandleProtocol_KWP2000_FAST");
	g_ObdInitingCustom = true;
	osThreadState threadState;
	threadState = osThreadGetState(canTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(canTaskHandle);
	}
	threadState = osThreadGetState(kwpTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(kwpTaskHandle);
	}
	threadState = osThreadGetState(saeTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(saeTaskHandle);
	}
	osThreadDef(kwpTask, KwpTask, osPriorityNormal, 0, 256);
  kwpTaskHandle = osThreadCreate(osThread(kwpTask), NULL);
	return BLE_MESSAGE_HANDLE_OK;
}
int BleMessageHandleProtocol_KWP2000_SLOW(void){
	g_ODB2Protocol = ODB2_PROTOCOL_KWP2000_SLOW;
	DebugPutString("\r\nBleMessageHandleProtocol_KWP2000_SLOW");
	g_ObdInitingCustom = true;
	osThreadState threadState;
	threadState = osThreadGetState(canTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(canTaskHandle);
	}
	threadState = osThreadGetState(kwpTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(kwpTaskHandle);
	}
	threadState = osThreadGetState(saeTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(saeTaskHandle);
	}
	osThreadDef(kwpTask, KwpTask, osPriorityNormal, 0, 256);
  kwpTaskHandle = osThreadCreate(osThread(kwpTask), NULL);
	return BLE_MESSAGE_HANDLE_OK;
}
int BleMessageHandleProtocol_VPW(void){
	g_ODB2Protocol = ODB2_PROTOCOL_VPW;
	DebugPutString("\r\nBleMessageHandleProtocol_VPW");
	g_ObdInitingCustom = true;
	osThreadState threadState;
	threadState = osThreadGetState(canTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(canTaskHandle);
	}
	threadState = osThreadGetState(kwpTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(kwpTaskHandle);
	}
	threadState = osThreadGetState(saeTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(saeTaskHandle);
	}
	osThreadDef(saeTask, SaeTask, osPriorityNormal, 0, 512);
  saeTaskHandle = osThreadCreate(osThread(saeTask), NULL);
	return BLE_MESSAGE_HANDLE_OK;
}
int BleMessageHandleProtocol_PWM(void){
	g_ODB2Protocol = ODB2_PROTOCOL_PWM;
	DebugPutString("\r\nBleMessageHandleProtocol_PWM");
	g_ObdInitingCustom = true;
	osThreadState threadState;
	threadState = osThreadGetState(canTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(canTaskHandle);
	}
	threadState = osThreadGetState(kwpTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(kwpTaskHandle);
	}
	threadState = osThreadGetState(saeTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(saeTaskHandle);
	}
	osThreadDef(saeTask, SaeTask, osPriorityNormal, 0, 512);
  saeTaskHandle = osThreadCreate(osThread(saeTask), NULL);
	return BLE_MESSAGE_HANDLE_OK;
}
int BleMessageHandleProtocol_START(void){
	g_ODB2Protocol = ODB2_PROTOCOL_START;
	DebugPutString("\r\nBleMessageHandleProtocol_START");
	g_ObdInitingCustom = false;
	osThreadState threadState;
	threadState = osThreadGetState(canTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(canTaskHandle);
	}
	threadState = osThreadGetState(kwpTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(kwpTaskHandle);
	}
	threadState = osThreadGetState(saeTaskHandle);
	if(threadState != osThreadDeleted){
		osThreadTerminate(saeTaskHandle);
	}
		/* Define & create BleTask */
    osThreadDef(saeTask, SaeTask, osPriorityNormal, 0, 512);
    saeTaskHandle = osThreadCreate(osThread(saeTask), NULL);
    /* Define & create BleTask */
    osThreadDef(canTask, CanTask, osPriorityNormal, 0, 256);
    canTaskHandle = osThreadCreate(osThread(canTask), NULL);
    /* Define & create BleTask */
    osThreadDef(kwpTask, KwpTask, osPriorityNormal, 0, 256);
    kwpTaskHandle = osThreadCreate(osThread(kwpTask), NULL);
	
	return BLE_MESSAGE_HANDLE_OK;
}

void BleResponseSpecialMessageAA(uint8_t cmd){
	uint8_t TxBuff[5] = {0xaa,cmd,0x0d, 0x0a, 0x00};
	for(int i=0;i<5;i++){
		BlePutChar(TxBuff[i]);
	}
}


void hexToString(const uint8_t* hexInput,int dlen ,char* strOutput){
    int u8C;
    for(int i=0; i<dlen; i++){
        u8C = hexInput[i]>>4;
        if(u8C<0x0A){
            strOutput[i*2] = '0' + u8C;
        } else {
            strOutput[i*2] = 'A' + u8C - 0x0A;
        }
        u8C = hexInput[i] & 0x0F;
        if(u8C<0x0A){
            strOutput[i*2+1] = '0' + u8C;
        } else {
            strOutput[i*2+1] = 'A' + u8C - 0x0A;
        }
    }
}

int stringToHex(const char* strInput, uint8_t* hexOutput) {
    int u8C;
    char xC;
    int ret = strlen(strInput)/2;
    for(int i=0; i<ret; i++){
        xC = strInput[i*2];
        if('0'<= xC && xC<='9') {
            u8C = xC - '0';
        } else if ('a'<= xC && xC <= 'f') {
            u8C = xC - 'a' + 0x0A;
        } else if ('A'<= xC && xC <= 'F') {
            u8C = xC - 'A' + 0x0A;
        } else return -1;
        u8C <<= 4;
        xC = strInput[i*2+1];
        if('0'<= xC && xC<='9') {
            u8C += xC - '0';
        } else if ('a'<= xC && xC <= 'f') {
            u8C += xC - 'a' + 0x0A;
        } else if ('A'<= xC && xC <= 'F') {
            u8C += xC - 'A' + 0x0A;
        } else return -1;
        hexOutput[i] = u8C;
    }
    return ret;
}

void AuthenFirstStep(uint8_t* pu8Data, uint16_t u16Size){
/*	memset(strAuthenRandomString,0,50);
	memset(strAuthenBuffer,0,128);
	memset(pu8AuthenBuff,0,128);
	
	uint32_t idPart1 = ((uint32_t *)UID_BASE)[0];
	uint32_t idPart2 = ((uint32_t *)UID_BASE)[1];
	uint32_t idPart3 = ((uint32_t *)UID_BASE)[2];
	
	sprintf(strAuthenRandomString,"%08x:%08x",idPart1,idPart3);
	DebugPutString(strAuthenRandomString);
	int length = strlen(strAuthenRandomString);
	memcpy(pu8AuthenBuff,strAuthenRandomString,strlen(strAuthenRandomString));
	int i=0;
	while(i<length){
		if(length-i<16){
			memset(&pu8AuthenBuff[length],'F',16-(length-i));
		}
		Encrypt16(&pu8AuthenBuff[i]);
		i+=16;
	}
	hexToString(pu8AuthenBuff,i,strAuthenBuffer);
	sprintf(tmpStr,"0010 %s\r\n",strAuthenBuffer);
	BlePutString(tmpStr);
	BlePutChar('\0');
	g_authenFistStep = true;
	DebugPutString("\r\nAuthen first step response ");
	DebugPutString(tmpStr);
*/
}

void AuthenSecondStep(uint8_t* pu8Data, uint16_t u16Size){
/*
	memset(strAuthenBuffer,0,128);
	memset(pu8AuthenBuff,0,128);
	char strAuthenRandomString_buff[50];
	char strAuthenKey_buff[50];
	if( u16Size-6!=0 && (u16Size-6)%32!=0){
		memcpy(strAuthenBuffer,&pu8Data[4],(u16Size-6));
		int length = stringToHex(strAuthenBuffer,pu8AuthenBuff);
		if(length ==-1 || length<(AUTHEN_RANDOM_STRING_LENGTH + strlen(g_authen_key_2))){
			DebugPutString("\r\nAuthen second step FAILED due to input length or wrong format");
		} else {
			int i= 0;
			while(i<length){
				Decrypt16((unsigned char*)&pu8AuthenBuff[i]);
				i+=16;
			}
			sprintf(tmpStr,"Authen second step:%s",pu8AuthenBuff);
			DebugPutString(tmpStr);
			memcpy(strAuthenRandomString_buff,pu8AuthenBuff,strlen(strAuthenRandomString));
			memcpy(strAuthenKey_buff,&pu8AuthenBuff[AUTHEN_RANDOM_STRING_LENGTH],strlen(g_authen_key_1));
			if( strcmp(strAuthenRandomString_buff,strAuthenRandomString)==0 && strcmp(strAuthenKey_buff,g_authen_key_1)==0 && g_authenFistStep){
				g_authen = true;
				DebugPutString("\r\nAuthen success");
			} else {
				DebugPutString("\r\nAuthen failed");
			}
		}
	} else {
		DebugPutString("\r\nAuthen second step FAILED due to input length");
	}
	memset(pu8AuthenBuff,0,128);
	memcpy(pu8AuthenBuff,strAuthenRandomString_buff,AUTHEN_RANDOM_STRING_LENGTH);
	if(g_authen){
		sprintf(tmpStr,"0011 OK\r\n");
	} else {
		sprintf(tmpStr,"0011 NOK\r\n");
	}
	BlePutString(tmpStr);
	g_authenFistStep = false;
	DebugPutString("\r\nAuthen second step response");
	DebugPutString(tmpStr);
*/
}

void PingRequestHandle(void){
	uint32_t idPart1 = ((uint32_t *)UID_BASE)[0];
	uint32_t idPart2 = ((uint32_t *)UID_BASE)[1];
	uint32_t idPart3 = ((uint32_t *)UID_BASE)[2];
	sprintf(tmpStr,"000E PONG %08x:%08x:%08x\r\n",idPart1,idPart2,idPart3);
	BlePutString(tmpStr);
	BlePutChar('\0');
	DebugPutString(tmpStr);
}
void PongResponseHandle(uint8_t* pu8Data){
/*
	if(!hextoByte(&pu8Data[0],&HLK_MAC[0])) {DebugPutString("\r\nError get MAC"); return;}
	if(!hextoByte(&pu8Data[3],&HLK_MAC[1])) {DebugPutString("\r\nError get MAC"); return;}
	if(!hextoByte(&pu8Data[6],&HLK_MAC[2])) {DebugPutString("\r\nError get MAC"); return;}
	if(!hextoByte(&pu8Data[9],&HLK_MAC[3])) {DebugPutString("\r\nError get MAC"); return;}
	if(!hextoByte(&pu8Data[12],&HLK_MAC[4])) {DebugPutString("\r\nError get MAC"); return;}
	if(!hextoByte(&pu8Data[15],&HLK_MAC[5])) {DebugPutString("\r\nError get MAC"); return;}
	sprintf(tmpStr,"\r\nHLK MAC address %02x:%02x:%02x:%02x:%02x:%02x",
	HLK_MAC[0],HLK_MAC[1],HLK_MAC[2],HLK_MAC[3],HLK_MAC[4],HLK_MAC[5]);
	DebugPutString(tmpStr);
	b_MAC = true;
	osSemaphoreRelease(g_MasterOfHlkSemid);
*/
}

void NotAuthenResponse(void){
	sprintf(tmpStr,"0012\r\n");
	BlePutString(tmpStr);
}

void BleResponseSpecialMessageMode00(uint8_t cmd, uint8_t* buffer, uint8_t dlen){
	uint8_t u8C;
	BlePutChar('0');
	BlePutChar('0');
	u8C = cmd>>4;
	if(0<=u8C&&u8C<=9){
		BlePutChar(u8C+'0');
	} else if(0x0a<=u8C && u8C <= 0x0f){
		BlePutChar(u8C - 10 + 'A');
	}
	u8C = cmd&0x0f;
	if(0<=u8C&&u8C<=9){
		BlePutChar(u8C+'0');
	} else if(0x0a<=u8C && u8C <= 0x0f){
		BlePutChar(u8C - 10 + 'A');
	}
	BlePutChar(' ');
	if(buffer!=NULL)
		for(int i=0 ; i<dlen; i++){
			BlePutChar(buffer[i]);
		}
	BlePutChar('\r');
	BlePutChar('\n');
//	BlePutChar('>');
	BlePutChar('\0');
}

void BleHandleAddPidToAutoLoop(uint8_t* pu8Data, uint16_t u16Size){
	g_ODB2Msg.u8PID = 0x01;
	g_ODB2Msg.u8RespLen = 0;
	if (g_BleMode == BLE_MODE_NORMAL) {
		// start blelop task
		uint8_t u8NumberAdded = (u16Size - 6)/2;

		for (int i = 0; i < u8NumberAdded && u8NumberPids < 8; i++) {
			hextoByte(&pu8Data[4+i*2], &pu8LoopPidList[u8NumberPids++]);
			memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen++], &pu8LoopPidList[u8NumberPids-1], 1);
			//g_ODB2Msg.u8RespLen += 2;
			sprintf(tmpStr, "\r\npu8LoopPidList[%d] = %d", u8NumberPids-1, pu8LoopPidList[u8NumberPids-1]);
			DebugPutString(tmpStr);
		}
		sprintf(tmpStr, "\r\nu8NumberPids = %d, g_ODB2Msg.u8RespLen = %d", u8NumberPids, g_ODB2Msg.u8RespLen);
		DebugPutString(tmpStr);
		//g_BleMode = BLE_MODE_AUTO_LOOP;
	}
}

void BleHandleStopAutoLoop(uint8_t* pu8Data, uint16_t u16Size){
	g_ODB2Msg.u8PID = 0x00;
	g_ODB2Msg.u8RespLen = 0;
	// stop bleloop task//
	g_BleMode = BLE_MODE_NORMAL;
}

void BleHandleStartAutoLoop(uint8_t* pu8Data, uint16_t u16Size){
	g_ODB2Msg.u8PID = 0x02;
	g_ODB2Msg.u8RespLen = 0;
	if(b_TripStart){
		bool add = true;
		for(int i=0; i< u8NumberPids; i++){
			if (pu8LoopPidList[i] == 0x0D) {
				add = false;
				break;
			}
		}
		if(add) {
			pu8LoopPidList[u8NumberPids++] = 0x0D;
		}
	}	
	g_BleMode = BLE_MODE_AUTO_LOOP;
	DebugPutString("\r\n Start auto loop");
}

void BleHandleStopAndResetAutoLoop(uint8_t* pu8Data, uint16_t u16Size){
	g_ODB2Msg.u8PID = 0x04;
	g_ODB2Msg.u8RespLen = 0;
	g_BleMode = BLE_MODE_NORMAL;
	u8NumberPids = 0;
	DebugPutString("\r\n Reset auto loop and stop");
}

void BleHandlePowerSave(uint8_t* pu8Data, uint16_t u16Size){
/*
	g_ODB2Msg.u8PID = 0x03;
  g_ODB2Msg.u8RespLen = 0;
	
	g_battSleepValue = g_battValue;
	LteTurnOff();
	isWaitingForWifiTurnedOf = 1;
	osDelay(20000);	// wait 30s for wifi turned off
	g_sleep = true;
	isWaitingForWifiTurnedOf = 0;
	
	// ThinhNT add make a record of gyro + acc entering sleep mode.							
	BleDebugString("g_BleMode = BLE_MODE_POWER_OFF");
	gf_idle_Ax = gf_Ax; gf_idle_Ay = gf_Ay, gf_idle_Az = gf_Az;
	gf_idle_Gx = gf_Gx, gf_idle_Gy = gf_Gy, gf_idle_Gz = gf_Gz;
	accelTotal = sqrt(gf_Ax*gf_Ax + gf_Ay*gf_Ay + gf_Az*gf_Az);
	idle_accelTotal = accelTotal;
	sprintf(tmpStr,"recorded Gyros: [%.3f, %.3f, %.3f], %.3f", gf_idle_Ax, gf_idle_Ay, gf_idle_Az, idle_accelTotal);
	g_BleMode = BLE_MODE_POWER_OFF;
	BleDebugString(tmpStr);
	u8NumberPids = 0;
	DebugPutString("\r\n Reset auto loop and turn off LTE");
*/
}

void BleHandleStartTrip(uint8_t* pu8Data, uint16_t u16Size){
	DebugPutString("\r\nTrip start");
	/*add pid 0x0D to pid list*/
	bool add = true;
	for(int i=0; i< u8NumberPids; i++){
		if (pu8LoopPidList[i] == 0x0D) {
			add = false;
			break;
		}
	}
	if(add) {
		pu8LoopPidList[u8NumberPids++] = 0x0D;
	} 
	/**/
	f_TripDistanceSended = 0;
	f_TripStopTimeSended = 0;
	f_TripDistanceSaved = 0;
	f_TripDistance = 0;
	f_TripStopTime = 0;
	u16TripIndex++;
	u32LastTickRecord = HAL_GetTick();
	b_TripStart = true; u8TripActive = ACTIVE;
	
	TRIP_SaveData();
	
	BleHandleTripUpdate(pu8Data, u16Size);
	
	DebugPutString("\r\n9900\r>");
	BlePutString("9900\r>\r\n");
	BlePutChar('\0');
}
void BleHandleStopTrip(uint8_t* pu8Data, uint16_t u16Size){
	DebugPutString("\r\nTrip stop");
	b_TripStart = false;	u8TripActive = NOT_ACTIVE;
	TRIP_SaveData();
	
	BleHandleTripUpdate(pu8Data, u16Size);
	
	DebugPutString("\r\n9901\r>");
	BlePutString("9901\r>\r\n");
	BlePutChar('\0');
}
#define TEST_UPDATE_DATA
void BleHandleTripUpdate(uint8_t* pu8Data, uint16_t u16Size) {
	#ifdef TEST_UPDATE_DATA
	u32TripDistance++;
	#else
	u32TripDistance = (uint32_t)f_TripDistance;
	#endif
	u32TripStopTime = (uint32_t)f_TripStopTime;
	f_TripDistanceSended = f_TripDistance;
	f_TripStopTimeSended = f_TripStopTime;
	sprintf(tmpStr,"9902\r\n%u\r\n%u\r\n>\r\n",u32TripDistance,u32TripStopTime);
	DebugPutString("\r\nTrip update\r\n");
	DebugPutString(tmpStr);
	BlePutString(tmpStr);
	BlePutChar('\0');
	
	sprintf(tmpStr,"010D\r\n41 0D %02x\r\n>\r\n",u8Speed);
	DebugPutString(tmpStr);
	BlePutString(tmpStr);
	BlePutChar('\0');
	
	BleHandleTripIndex(pu8Data, u16Size);
}
void BleHandleSetSpeedRatio(uint8_t* pu8Data, uint16_t u16Size) {
	
}
void BleHandleTripIndex(uint8_t* pu8Data, uint16_t u16Size) {
	sprintf(tmpStr,"9904\r\n%u\r\n>\r\n",u16TripIndex);
	DebugPutString(tmpStr);
	BlePutString(tmpStr);
	BlePutChar('\0');
}

