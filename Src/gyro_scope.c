/*
 * hal_GYROSENSOR.c
 *
 *  Created on: Feb 19, 2016
 *      Author: Sina Darvishi
 */

/**
 * |----------------------------------------------------------------------
 * | Copyright (C) Sina Darvishi,2016
 * |
 * | This program is free software: you can redistribute it and/or modify
 * | it under the terms of the GNU General Public License as published by
 * | the Free Software Foundation, either version 3 of the License, or
 * | any later version.
 * |
 * | This program is distributed in the hope that it will be useful,
 * | but WITHOUT ANY WARRANTY; without even the implied warranty of
 * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * | GNU General Public License for more details.
 * |
 * | You should have received a copy of the GNU General Public License
 * | along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * |----------------------------------------------------------------------
 */

#include "gyro_scope.h"
#include "stm32f3xx_hal_i2c.h"
#include <stdio.h>
#include <math.h>
#include "cmsis_os.h"
#include <string.h>
#if (DB_EN==1)
#define DEBUG_SENSOR(str, ...) printf(str, ##__VA_ARGS__)
#else
#define DEBUG_SENSOR(str, ...)
#endif

/* Default I2C address */
#define GYROSENSOR_I2C_ADDR		0x68 // init address must be shift left 

/* Who I am register value */
#define GYROSENSOR_I_AM				0x68

/* GYROSENSOR registers */
#define GYROSENSOR_AUX_VDDIO					0x01

#define GYROSENSOR_XA_OFFS_USRH      0x06 //[15:0] XA_OFFS
#define GYROSENSOR_XA_OFFS_USRL      0x07
#define GYROSENSOR_YA_OFFS_USRH      0x08 //[15:0] YA_OFFS
#define GYROSENSOR_YA_OFFS_USRL      0x09
#define GYROSENSOR_ZA_OFFS_USRH      0x0A //[15:0] ZA_OFFS
#define GYROSENSOR_ZA_OFFS_USRL      0x0B

#define GYROSENSOR_XG_OFFS_USRH      0x13 //[15:0] XG_OFFS_USR
#define GYROSENSOR_XG_OFFS_USRL      0x14
#define GYROSENSOR_YG_OFFS_USRH      0x15 //[15:0] YG_OFFS_USR
#define GYROSENSOR_YG_OFFS_USRL      0x16
#define GYROSENSOR_ZG_OFFS_USRH      0x17 //[15:0] ZG_OFFS_USR
#define GYROSENSOR_ZG_OFFS_USRL      0x18

#define GYROSENSOR_SMPLRT_DIV				0x19
#define GYROSENSOR_CONFIG						0x1A
#define GYROSENSOR_GYRO_CONFIG				0x1B
#define GYROSENSOR_ACCEL_CONFIG			0x1C
#define GYROSENSOR_MOTION_THRESH			0x1F
#define GYROSENSOR_INT_PIN_CFG				0x37
#define GYROSENSOR_INT_ENABLE				0x38
#define GYROSENSOR_INT_STATUS				0x3A
#define GYROSENSOR_ACCEL_XOUT_H			0x3B
#define GYROSENSOR_ACCEL_XOUT_L			0x3C
#define GYROSENSOR_ACCEL_YOUT_H			0x3D
#define GYROSENSOR_ACCEL_YOUT_L			0x3E
#define GYROSENSOR_ACCEL_ZOUT_H			0x3F
#define GYROSENSOR_ACCEL_ZOUT_L			0x40
#define GYROSENSOR_TEMP_OUT_H				0x41
#define GYROSENSOR_TEMP_OUT_L				0x42
#define GYROSENSOR_GYRO_XOUT_H				0x43
#define GYROSENSOR_GYRO_XOUT_L				0x44
#define GYROSENSOR_GYRO_YOUT_H				0x45
#define GYROSENSOR_GYRO_YOUT_L				0x46
#define GYROSENSOR_GYRO_ZOUT_H				0x47
#define GYROSENSOR_GYRO_ZOUT_L			  0x48
#define GYROSENSOR_MOT_DETECT_STATUS	0x61
#define GYROSENSOR_SIGNAL_PATH_RESET	0x68
#define GYROSENSOR_MOT_DETECT_CTRL		0x69
#define GYROSENSOR_USER_CTRL			    0x6A
#define GYROSENSOR_PWR_MGMT_1			  0x6B
#define GYROSENSOR_PWR_MGMT_2			  0x6C
#define GYROSENSOR_FIFO_COUNTH			  0x72
#define GYROSENSOR_FIFO_COUNTL			  0x73
#define GYROSENSOR_FIFO_R_W			    0x74
#define GYROSENSOR_WHO_AM_I			    0x75

/* Gyro sensitivities in degrees/s */
#define GYROSENSOR_GYRO_SENS_250		((float) 131)
#define GYROSENSOR_GYRO_SENS_500		((float) 65.5)
#define GYROSENSOR_GYRO_SENS_1000		((float) 32.8)
#define GYROSENSOR_GYRO_SENS_2000		((float) 16.4)

/* Acce sensitivities in g/s */
#define GYROSENSOR_ACCE_SENS_2			((float) 16384)
#define GYROSENSOR_ACCE_SENS_4			((float) 8192)
#define GYROSENSOR_ACCE_SENS_8			((float) 4096)
#define GYROSENSOR_ACCE_SENS_16		((float) 2048)

/* Denta Error: accel changes suddenly */
#define DENTA_ACCEL_X ((float)0.2)
#define DENTA_ACCEL_Y ((float)0.2)
#define DENTA_ACCEL_Z ((float)0.2)
			
#define millis() HAL_GetTick()

/* define I2C handle */
I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef *Handle = &hi2c1;
/*********************************************************/
/* I2C1 init function */
void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Analogue filter 
    */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Digital filter 
    */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

void HAL_I2C_MspInit(I2C_HandleTypeDef* i2cHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(i2cHandle->Instance==I2C1)
  {
  /* USER CODE BEGIN I2C1_MspInit 0 */

  /* USER CODE END I2C1_MspInit 0 */
  
    /**I2C1 GPIO Configuration    
    PB6     ------> I2C1_SCL
    PB7     ------> I2C1_SDA 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* I2C1 clock enable */
    __HAL_RCC_I2C1_CLK_ENABLE();
  /* USER CODE BEGIN I2C1_MspInit 1 */

  /* USER CODE END I2C1_MspInit 1 */
  }
}

void HAL_I2C_MspDeInit(I2C_HandleTypeDef* i2cHandle)
{

  if(i2cHandle->Instance==I2C1)
  {
  /* USER CODE BEGIN I2C1_MspDeInit 0 */

  /* USER CODE END I2C1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_I2C1_CLK_DISABLE();
  
    /**I2C1 GPIO Configuration    
    PB6     ------> I2C1_SCL
    PB7     ------> I2C1_SDA 
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_6|GPIO_PIN_7);

  /* USER CODE BEGIN I2C1_MspDeInit 1 */

  /* USER CODE END I2C1_MspDeInit 1 */
  }
} 
/*********************************************************/
static unsigned long	last_time = 0;
static float last_accX = 0, last_accY = 0, last_accZ = 0;

GYROSENSOR_Result GYROSENSOR_Init(GYROSENSOR* DataStruct, GYROSENSOR_Device DeviceNumber, GYROSENSOR_Accelerometer AccelerometerSensitivity, GYROSENSOR_Gyroscope GyroscopeSensitivity)
{
	uint8_t WHO_AM_I = (uint8_t)GYROSENSOR_WHO_AM_I;
	uint8_t temp;
	uint8_t d[2];
	Debug("1\n");
	MX_I2C1_Init();
	// HAL_I2C_MspInit(Handle);
	/* Format I2C address */
	DataStruct->Address = (GYROSENSOR_I2C_ADDR | (uint8_t)DeviceNumber) << 1;
	uint8_t address = DataStruct->Address;
	Debug("2\n");
	/* Check if device is connected */
	if(HAL_I2C_IsDeviceReady(Handle, address, 2, 100)!=HAL_OK)
	{
				return GYROSENSOR_Result_Error;
	}
	// DEBUG_SENSOR("MPU: isDeviceReady\n");
	/* Check who am I */
	//------------------
	/* Send address */
	Debug("3\n");
	if(HAL_I2C_Master_Transmit(Handle, address, &WHO_AM_I, 1, 1000) != HAL_OK)
	{
		return GYROSENSOR_Result_Error;
	}
	Debug("4\n");
	/* Receive multiple byte */
	if(HAL_I2C_Master_Receive(Handle, address, &temp, 1, 1000) != HAL_OK)
	{
		return GYROSENSOR_Result_Error;
	}

	/* Checking */
	if(temp != GYROSENSOR_I_AM)
	{
			/* Return error */
			return GYROSENSOR_Result_DeviceInvalid;
	}
	// DEBUG_SENSOR("MPU: GYROSENSOR i am\n");
	//------------------

	/* Wakeup GYROSENSOR */
	//------------------
	/* Format array to send */
	d[0] = GYROSENSOR_PWR_MGMT_1;
	d[1] = 0x00;
	Debug("5\n");
	/* Try to transmit via I2C */
	if(HAL_I2C_Master_Transmit(Handle,(uint16_t)address , (uint8_t *)d, 2, 1000) != HAL_OK)
	{
				return GYROSENSOR_Result_Error;
	}
	//------------------
  //Init Default Params
	DataStruct->angX = 0;
	DataStruct->angY = 0;
	DataStruct->angZ = 0;
	DataStruct->gyrX = 0;
	DataStruct->gyrY = 0;
	DataStruct->gyrZ = 0;
	DataStruct->gyrXoffs = 0;
	DataStruct->gyrYoffs = 0;
	DataStruct->gyrZoffs = 0;
	/* Set sample rate to 1kHz */
	HAL_Delay(100);

	/* Config lowpassfilter */
	Debug("6\n");
	GYROSENSOR_configLPFilter(DataStruct);
	HAL_Delay(100);
	Debug("7\n");
	GYROSENSOR_SetDataRate(DataStruct, GYROSENSOR_DataRate_1KHz); // set sample rate is 1kHz
	HAL_Delay(100);
	Debug("8\n");
	/* Config accelerometer */
	GYROSENSOR_SetAccelerometer(DataStruct, AccelerometerSensitivity);
	HAL_Delay(100);
	Debug("9\n");
	/* Config Gyroscope */
	GYROSENSOR_SetGyroscope(DataStruct, GyroscopeSensitivity);
	HAL_Delay(100);
	Debug("10\n");
	/* Calib device */
	GYROSENSOR_DeviceCalibration(DataStruct);
	HAL_Delay(1000);
	Debug("11\n");
	last_time = millis();
	Debug("12\n");
	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_configLPFilter(GYROSENSOR* DataStruct)
{
	uint8_t d[2];
	uint8_t address = DataStruct->Address;
	/* Format array to send */
	d[0] = GYROSENSOR_CONFIG;
	d[1] = 0x01; // Gyroscope Output Rate = 1kHz

	/* Set data sample rate */
	while(HAL_I2C_Master_Transmit(Handle,(uint16_t)address,(uint8_t *)d,2,1000)!=HAL_OK);
	/*{
				return SD_GYROSENSOR_Result_Error;
	}*/

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_SetDataRate(GYROSENSOR* DataStruct, uint8_t rate)
{
	uint8_t d[2];
	uint8_t address = DataStruct->Address;
	/* Format array to send */
	d[0] = GYROSENSOR_SMPLRT_DIV;
	d[1] = rate;

	/* Set data sample rate */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, (uint8_t *)d, 2, 1000)!=HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_SetAccelerometer(GYROSENSOR* DataStruct, GYROSENSOR_Accelerometer AccelerometerSensitivity)
{
	uint8_t temp;
	uint8_t address = DataStruct->Address;
	uint8_t regAdd =(uint8_t )GYROSENSOR_ACCEL_CONFIG;

	/* Config accelerometer */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address,&regAdd, 1, 1000) != HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/
	while(HAL_I2C_Master_Receive(Handle, (uint16_t)address, &temp, 1, 1000) != HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/
	temp = (temp & 0xE7) | (uint8_t)AccelerometerSensitivity << 3;
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address,&temp, 1, 1000) != HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/

	/* Set sensitivities for multiplying gyro and accelerometer data */
	switch (AccelerometerSensitivity) {
		case GYROSENSOR_Accelerometer_2G:
			DataStruct->Acce_Mult = (float)1 / GYROSENSOR_ACCE_SENS_2;
			break;
		case GYROSENSOR_Accelerometer_4G:
			DataStruct->Acce_Mult = (float)1 / GYROSENSOR_ACCE_SENS_4;
			break;
		case GYROSENSOR_Accelerometer_8G:
			DataStruct->Acce_Mult = (float)1 / GYROSENSOR_ACCE_SENS_8;
			break;
		case GYROSENSOR_Accelerometer_16G:
			DataStruct->Acce_Mult = (float)1 / GYROSENSOR_ACCE_SENS_16;
			break;
		default:
			break;
		}

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_SetGyroscope(GYROSENSOR* DataStruct, GYROSENSOR_Gyroscope GyroscopeSensitivity)
{
	uint8_t temp;
	uint8_t address = DataStruct->Address;
	uint8_t regAdd =(uint8_t )GYROSENSOR_GYRO_CONFIG;

	/* Config gyroscope */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address,&regAdd, 1, 1000) != HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/
	while(HAL_I2C_Master_Receive(Handle, (uint16_t)address, &temp, 1, 1000) != HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/
	temp = (temp & 0xE7) | (uint8_t)GyroscopeSensitivity << 3;
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address,&temp, 1, 1000) != HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/

	switch (GyroscopeSensitivity) {
			case GYROSENSOR_Gyroscope_250s:
				DataStruct->Gyro_Mult = (float)1 / GYROSENSOR_GYRO_SENS_250;
				break;
			case GYROSENSOR_Gyroscope_500s:
				DataStruct->Gyro_Mult = (float)1 / GYROSENSOR_GYRO_SENS_500;
				break;
			case GYROSENSOR_Gyroscope_1000s:
				DataStruct->Gyro_Mult = (float)1 / GYROSENSOR_GYRO_SENS_1000;
				break;
			case GYROSENSOR_Gyroscope_2000s:
				DataStruct->Gyro_Mult = (float)1 / GYROSENSOR_GYRO_SENS_2000;
				break;
			default:
				break;
		}
	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_ReadAccelerometer(GYROSENSOR* DataStruct)
{
	uint8_t data[6];
	uint8_t reg = GYROSENSOR_ACCEL_XOUT_H;
	uint8_t address = DataStruct->Address;

	/* Read accelerometer data */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, &reg, 1, 1000) != HAL_OK);

	while(HAL_I2C_Master_Receive(Handle, (uint16_t)address, data, 6, 1000) != HAL_OK);

	/* Format */
	DataStruct->Accelerometer_X = (int16_t)(data[0] << 8 | data[1]);
	DataStruct->Accelerometer_Y = (int16_t)(data[2] << 8 | data[3]);
	DataStruct->Accelerometer_Z = (int16_t)(data[4] << 8 | data[5]);

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_ReadGyroscope(GYROSENSOR* DataStruct)
{
	uint8_t data[6];
	uint8_t reg = GYROSENSOR_GYRO_XOUT_H;
	uint8_t address = DataStruct->Address;

	/* Read gyroscope data */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, &reg, 1, 1000) != HAL_OK);

	while(HAL_I2C_Master_Receive(Handle, (uint16_t)address, data, 6, 1000) != HAL_OK);

	/* Format */
	DataStruct->Gyroscope_X = (int16_t)(data[0] << 8 | data[1]);
	DataStruct->Gyroscope_Y = (int16_t)(data[2] << 8 | data[3]);
	DataStruct->Gyroscope_Z = (int16_t)(data[4] << 8 | data[5]);

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_ReadTemperature(GYROSENSOR* DataStruct)
{
	uint8_t data[2];
	int16_t temp;
	uint8_t reg = GYROSENSOR_TEMP_OUT_H;
	uint8_t address = DataStruct->Address;

	/* Read temperature */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, &reg, 1, 1000) != HAL_OK);

	while(HAL_I2C_Master_Receive(Handle, (uint16_t)address, data, 2, 1000) != HAL_OK);

	/* Format temperature */
	temp = (data[0] << 8 | data[1]);
	DataStruct->Temperature = (float)((int16_t)temp / (float)340.0 + (float)36.53);

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_ReadAll(GYROSENSOR* DataStruct)
{
	uint8_t data[14];
	int16_t temp;
	uint8_t reg = GYROSENSOR_ACCEL_XOUT_H;
	uint8_t address = DataStruct->Address;
	HAL_StatusTypeDef result;
	/* Read full raw data, 14bytes */
	result = HAL_I2C_Master_Transmit(Handle, (uint16_t)address, &reg, 1, 1000);
	if(result == HAL_OK)
	{
		result = HAL_I2C_Master_Receive(Handle, (uint16_t)address, data, 14, 1000);
		if(result == HAL_OK)
		{
			/* Format accelerometer data */
			DataStruct->Accelerometer_X = (int16_t)(data[0] << 8 | data[1]);
			DataStruct->Accelerometer_Y = (int16_t)(data[2] << 8 | data[3]);
			DataStruct->Accelerometer_Z = (int16_t)(data[4] << 8 | data[5]);

			/* Format temperature */
			temp = (data[6] << 8 | data[7]);
			DataStruct->Temperature = (float)((float)((int16_t)temp) / (float)340.0 + (float)36.53);

			/* Format gyroscope data */
			DataStruct->Gyroscope_X = (int16_t)(data[8] << 8 | data[9]);
			DataStruct->Gyroscope_Y = (int16_t)(data[10] << 8 | data[11]);
			DataStruct->Gyroscope_Z = (int16_t)(data[12] << 8 | data[13]);

		//	DEBUG_SENSOR("MPU: aX: %d\t aY: %d\t aZ: %d\t gX: %d\t gY: %d\t gZ: %d\n",
		//		DataStruct->Accelerometer_X, DataStruct->Accelerometer_Y, DataStruct->Accelerometer_Z,
		//		DataStruct->Gyroscope_X, DataStruct->Gyroscope_Y, DataStruct->Gyroscope_Z);
		}
		// else {DEBUG_SENSOR("Get data Err %d\n", result); return GYROSENSOR_Result_Error;}
	}
	// else {DEBUG_SENSOR("Access reg Err %d\n", result); return GYROSENSOR_Result_Error;}
	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_EnableInterrupts(GYROSENSOR* DataStruct)
{
	uint8_t temp;
	uint8_t reg[2] = {GYROSENSOR_INT_ENABLE,0x21};
	uint8_t address = DataStruct->Address;

	/* Enable interrupts for data ready and motion detect */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, reg, 2, 1000) != HAL_OK);

	uint8_t mpu_reg= GYROSENSOR_INT_PIN_CFG;
	/* Clear IRQ flag on any read operation */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, &mpu_reg, 1, 1000) != HAL_OK);

	while(HAL_I2C_Master_Receive(Handle, (uint16_t)address, &temp, 14, 1000) != HAL_OK);
	temp |= 0x10;
	reg[0] = GYROSENSOR_INT_PIN_CFG;
	reg[1] = temp;
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, reg, 2, 1000) != HAL_OK);

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_DisableInterrupts(GYROSENSOR* DataStruct)
{
	uint8_t reg[2] = {GYROSENSOR_INT_ENABLE,0x00};
	uint8_t address = DataStruct->Address;

	/* Disable interrupts */
	while(HAL_I2C_Master_Transmit(Handle,(uint16_t)address,reg,2,1000)!=HAL_OK);
	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_ReadInterrupts(GYROSENSOR* DataStruct, GYROSENSOR_Interrupt* InterruptsStruct)
{
	uint8_t read;

	/* Reset structure */
	InterruptsStruct->Status = 0;
	uint8_t reg = GYROSENSOR_INT_STATUS;
	uint8_t address = DataStruct->Address;

	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, &reg, 1, 1000) != HAL_OK);

	while(HAL_I2C_Master_Receive(Handle, (uint16_t)address, &read, 14, 1000) != HAL_OK);

	/* Fill value */
	InterruptsStruct->Status = read;
	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

/* filter*/
#define NUM_OF_FILTER 30
int32_t sum(int16_t *arr, uint8_t len)
{
	uint8_t i;
	int32_t s = 0;
	for(i = 0; i < len; i++) s += arr[i];
	return s;
}

int16_t filterX(int16_t inputX)
{
	static int16_t tempX[30] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	static int16_t lastResultX;
	int16_t result;
	uint8_t index;
	if((inputX > 0 && lastResultX < 0) || (inputX < 0 && lastResultX > 0)) memset(tempX, 0, NUM_OF_FILTER); 
	tempX[NUM_OF_FILTER-1] = inputX;
	lastResultX = result = (int16_t)(sum(tempX, NUM_OF_FILTER)/NUM_OF_FILTER);
	for(index = 0; index < NUM_OF_FILTER-1; index++) tempX[index] = tempX[index+1];
	tempX[NUM_OF_FILTER-1] = inputX;
	return result;
}

int16_t filterY(int16_t inputY)
{
	static int16_t tempY[30] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	int16_t result;
	static int16_t lastResultY;
	uint8_t index;
	if((inputY > 0 && lastResultY < 0) || (inputY < 0 && lastResultY > 0)) memset(tempY, 0, NUM_OF_FILTER);
	tempY[NUM_OF_FILTER-1] = inputY;
	result = (int16_t)(sum(tempY, NUM_OF_FILTER)/NUM_OF_FILTER);
	for(index = 0; index < NUM_OF_FILTER-1; index++) tempY[index] = tempY[index+1];
	tempY[NUM_OF_FILTER-1] = inputY;
	return result;
}

int16_t filterZ(int16_t inputZ)
{
	static int16_t tempZ[30] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	int16_t result;
	static int16_t lastResultZ;
	uint8_t index;
	if((inputZ > 0 && lastResultZ < 0) || (inputZ < 0 && lastResultZ > 0)) memset(tempZ, 0, NUM_OF_FILTER);
	tempZ[NUM_OF_FILTER-1] = inputZ;
	result = (int16_t)(sum(tempZ, NUM_OF_FILTER)/NUM_OF_FILTER);
	for(index = 0; index < NUM_OF_FILTER-1; index++) tempZ[index] = tempZ[index+1];
	tempZ[NUM_OF_FILTER-1] = inputZ;
	return result;
}
/**/
GYROSENSOR_Result GYROSENSOR_ProcessAccelerometer(GYROSENSOR* DataStruct)
{
	GYROSENSOR_Result result;
	result = GYROSENSOR_ReadAccelerometer(DataStruct);
	int16_t x, y , z;
	if(result == GYROSENSOR_Result_Ok)
	{
		x = filterX(DataStruct->Accelerometer_X);
		y = filterY(DataStruct->Accelerometer_Y);
		z = filterZ(DataStruct->Accelerometer_Z);
		
		DataStruct->accX = x * DataStruct->Acce_Mult;
		DataStruct->accY = y * DataStruct->Acce_Mult;
		DataStruct->accZ = z * DataStruct->Acce_Mult;
//		DEBUG_SENSOR("MPU: aX: %.2f\t aY: %.2f\t aZ: %.2f\n", ax, ay, az);
		// DEBUG_SENSOR("%.2f,%.2f,%.2f\n", DataStruct->accX, DataStruct->accY, DataStruct->accZ);
	}
	return result;
}

GYROSENSOR_Result GYROSENSOR_ProcessGyroscope(GYROSENSOR* DataStruct)
{
	GYROSENSOR_Result result;
	result = GYROSENSOR_ReadGyroscope(DataStruct);
	if(result == GYROSENSOR_Result_Ok)
	{
		DataStruct->gyrX = ( ( DataStruct->Gyroscope_X ) - DataStruct->gyrXoffs) * DataStruct->Gyro_Mult;
		DataStruct->gyrY = ( ( DataStruct->Gyroscope_Y ) - DataStruct->gyrYoffs) * DataStruct->Gyro_Mult;
		DataStruct->gyrZ = ( ( DataStruct->Gyroscope_Z ) - DataStruct->gyrZoffs) * DataStruct->Gyro_Mult;
		// DEBUG_SENSOR("MPU: gX: %.2f\t gY: %.2f\t gZ: %.2f\n", DataStruct->gyrX, DataStruct->gyrY, DataStruct->gyrZ);
	}
	return result;
}

GYROSENSOR_Result GYROSENSOR_ProcessAll(GYROSENSOR* DataStruct)
{
	GYROSENSOR_Result result;
	result = GYROSENSOR_ReadAll(DataStruct);
	if(result == GYROSENSOR_Result_Ok)
	{
		DataStruct->accX = DataStruct->Accelerometer_X * DataStruct->Acce_Mult;
		DataStruct->accY = DataStruct->Accelerometer_Y * DataStruct->Acce_Mult;
		DataStruct->accZ = DataStruct->Accelerometer_Z * DataStruct->Acce_Mult;
		DataStruct->gyrX = ( ( DataStruct->Gyroscope_X ) - DataStruct->gyrXoffs) * DataStruct->Gyro_Mult;
		DataStruct->gyrY = ( ( DataStruct->Gyroscope_Y ) - DataStruct->gyrYoffs) * DataStruct->Gyro_Mult;
		DataStruct->gyrZ = ( ( DataStruct->Gyroscope_Z ) - DataStruct->gyrZoffs) * DataStruct->Gyro_Mult;
//		DEBUG_SENSOR("MPU: aX: %.2f\t aY: %.2f\t aZ: %.2f\t gX: %.2f\t gY: %.2f\t gZ: %.2f\n", DataStruct->accX, DataStruct->accY, DataStruct->accZ, DataStruct->gyrX, DataStruct->gyrY, DataStruct->gyrZ);
		// DEBUG_SENSOR("%.2f,%.2f,%.2f\n", DataStruct->accX, DataStruct->accY, DataStruct->accZ);
	}
	return result;
}

GYROSENSOR_Result GYROSENSOR_SetOffsAccelerometer_X(GYROSENSOR* DataStruct, int16_t offset)
{
	uint8_t d[3];
	uint8_t address = DataStruct->Address;
	/* Format array to send */
	d[0] = GYROSENSOR_XA_OFFS_USRH;
	d[1] = (offset & 0xFF00)>>8;
	d[2] =  offset & 0x00FF;
	DataStruct->accelXoffs = offset;
	
	/* Set data offset */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, (uint8_t *)d, 3, 1000)!=HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_SetOffsAccelerometer_Y(GYROSENSOR* DataStruct, int16_t offset)
{
	uint8_t d[3];
	uint8_t address = DataStruct->Address;
	/* Format array to send */
	d[0] = GYROSENSOR_YA_OFFS_USRH;
	d[1] = (offset & 0xFF00)>>8;
	d[2] =  offset & 0x00FF;
  DataStruct->accelYoffs = offset;
	
	/* Set data offset */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, (uint8_t *)d, 3, 1000)!=HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_SetOffsAccelerometer_Z(GYROSENSOR* DataStruct, int16_t offset)
{
	uint8_t d[3];
	uint8_t address = DataStruct->Address;
	/* Format array to send */
	d[0] = GYROSENSOR_ZA_OFFS_USRH;
	d[1] = (offset & 0xFF00)>>8;
	d[2] =  offset & 0x00FF;
	DataStruct->accelZoffs = offset;
	
	/* Set data offset */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, (uint8_t *)d, 3, 1000)!=HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_SetOffsAccelerometer(GYROSENSOR* DataStruct, int16_t offsetX, int16_t offsetY, int16_t offsetZ)
{
	uint8_t d[7];
	uint8_t address = DataStruct->Address;
	/* Format array to send */
	d[0] = GYROSENSOR_XA_OFFS_USRH;
	d[1] = (offsetX & 0xFF00)>>8;
	d[2] =  offsetX & 0x00FF;
	d[3] = (offsetY & 0xFF00)>>8;
	d[4] =  offsetY & 0x00FF;
	d[5] = (offsetZ & 0xFF00)>>8;
	d[6] =  offsetZ & 0x00FF;
	DataStruct->accelXoffs = offsetX;
	DataStruct->accelYoffs = offsetY;
	DataStruct->accelZoffs = offsetZ;
	
	/* Set data offset */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, (uint8_t *)d, 7, 1000)!=HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_SetOffsGyroscope_X(GYROSENSOR* DataStruct, int16_t offset)
{
	uint8_t d[3];
	uint8_t address = DataStruct->Address;
	/* Format array to send */
	d[0] = GYROSENSOR_XG_OFFS_USRH;
	d[1] = (offset & 0xFF00)>>8;
	d[2] =  offset & 0x00FF;
	DataStruct->gyrXoffs = offset;
	
	/* Set data offset */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, (uint8_t *)d, 3, 1000)!=HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_SetOffsGyroscope_Y(GYROSENSOR* DataStruct, int16_t offset)
{
	uint8_t d[3];
	uint8_t address = DataStruct->Address;
	/* Format array to send */
	d[0] = GYROSENSOR_YG_OFFS_USRH;
	d[1] = (offset & 0xFF00)>>8;
	d[2] =  offset & 0x00FF;
	DataStruct->gyrYoffs = offset;
	
	/* Set data offset */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, (uint8_t *)d, 3, 1000)!=HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_SetOffsGyroscope_Z(GYROSENSOR* DataStruct, int16_t offset)
{
	uint8_t d[3];
	uint8_t address = DataStruct->Address;
	/* Format array to send */
	d[0] = GYROSENSOR_ZG_OFFS_USRH;
	d[1] = (offset & 0xFF00)>>8;
	d[2] =  offset & 0x00FF;
	DataStruct->gyrZoffs = offset;
	
	/* Set data offset */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, (uint8_t *)d, 3, 1000)!=HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_SetOffsGyroscope(GYROSENSOR* DataStruct, int16_t offsetX, int16_t offsetY, int16_t offsetZ)
{
	uint8_t d[7];
	uint8_t address = DataStruct->Address;
	/* Format array to send */
	d[0] = GYROSENSOR_XG_OFFS_USRH;
	d[1] = (offsetX & 0xFF00)>>8;
	d[2] =  offsetX & 0x00FF;
	d[3] = (offsetY & 0xFF00)>>8;
	d[4] =  offsetY & 0x00FF;
	d[5] = (offsetZ & 0xFF00)>>8;
	d[6] =  offsetZ & 0x00FF;
	DataStruct->gyrXoffs = offsetX;
	DataStruct->gyrYoffs = offsetY;
	DataStruct->gyrZoffs = offsetZ;
	
	/* Set data offset */
	while(HAL_I2C_Master_Transmit(Handle, (uint16_t)address, (uint8_t *)d, 7, 1000)!=HAL_OK);
	/*{
				return GYROSENSOR_Result_Error;
	}*/

	/* Return OK */
	return GYROSENSOR_Result_Ok;
}
#include <stdlib.h>
GYROSENSOR_Result GYROSENSOR_Update(GYROSENSOR* DataStruct)
{
	double dt, filter_coefficient;
	//read IMU data
	if(GYROSENSOR_ReadAll(DataStruct) == GYROSENSOR_Result_Ok)
	{
		double roll, pitch;
		/* Fillter: https://docs.google.com/viewer?a=v&pid=sites&srcid=ZGVmYXVsdGRvbWFpbnxteWltdWVzdGltYXRpb25leHBlcmllbmNlfGd4OjY1Yzk3YzhiZmE1N2M4Y2U */
		DataStruct->accX = DataStruct->Accelerometer_X * DataStruct->Acce_Mult;
		DataStruct->accY = DataStruct->Accelerometer_Y * DataStruct->Acce_Mult;
		DataStruct->accZ = DataStruct->Accelerometer_Z * DataStruct->Acce_Mult;
		DataStruct->gyrX = ( ( DataStruct->Gyroscope_X ) - DataStruct->gyrXoffs) * DataStruct->Gyro_Mult;
		DataStruct->gyrY = ( ( DataStruct->Gyroscope_Y ) - DataStruct->gyrYoffs) * DataStruct->Gyro_Mult;
		DataStruct->gyrZ = ( ( DataStruct->Gyroscope_Z ) - DataStruct->gyrZoffs) * DataStruct->Gyro_Mult;
	//	roll = atan2(DataStruct->Accelerometer_X, sqrt( pow(DataStruct->Accelerometer_Y, 2) + pow(DataStruct->Accelerometer_Z, 2) ) ) * 180 / 3.1415926;
	//	pitch = atan2(DataStruct->Accelerometer_Y, sqrt( pow(DataStruct->Accelerometer_X, 2) + pow(DataStruct->Accelerometer_Z, 2) ) ) * 180 / 3.1415926; 
		
		/* 
		* http://students.iitk.ac.in/roboclub/2017/12/21/Beginners-Guide-to-IMU.html 
		* https://engineering.stackexchange.com/questions/3348/calculating-pitch-yaw-and-roll-from-mag-acc-and-gyro-data
		*/
		roll = atan2(DataStruct->Accelerometer_Y, DataStruct->Accelerometer_Z)*180/3.14; // FORMULA FOUND ON INTERNET
	  pitch = atan2((-1)*DataStruct->Accelerometer_X, sqrt(DataStruct->Accelerometer_Y*DataStruct->Accelerometer_Y + DataStruct->Accelerometer_Z*DataStruct->Accelerometer_Z))*180/3.14; //FORMULA FOUND ON INTERNET
	//	
		dt = (double)(millis() - last_time) / 1000;
		last_time = millis();
	//	if(DataStruct->Accelerometer_Z > 0)
	//  {
	//    DataStruct->angX = DataStruct->angX - DataStruct->gyrY * dt;
	//    DataStruct->angY = DataStruct->angY + DataStruct->gyrX * dt;
	//  }
	//  else
	//  {
	//    DataStruct->angX = DataStruct->angX + DataStruct->gyrY * dt;
	//    DataStruct->angY = DataStruct->angY - DataStruct->gyrX * dt;
	//  }
		DataStruct->angX += DataStruct->gyrX * dt;
		DataStruct->angY += DataStruct->gyrY * dt;
		if(abs(DataStruct->gyrZoffs - DataStruct->Gyroscope_Z) > 50)
		{
			DataStruct->angZ += DataStruct->gyrZ * dt;
			DataStruct->angZ = DataStruct->angZ - 360 * floor(DataStruct->angZ / 360);
			if(DataStruct->angZ > 180)
			{
				DataStruct->angZ = DataStruct->angZ - 360;
			}
		}

		/*
			complementary filter
			set 0.5sec = tau = dt * A / (1 - A)   time constant greater than timescale of typical accelerometer noise
				** Refer to: http://www.geekmomprojects.com/gyroscopes-and-accelerometers-on-a-chip/
			so A = tau / (tau + dt)
				** Refer to: https://sites.google.com/site/myimuestimationexperience/filters/complementary-filter
		*/
		filter_coefficient = 0.5 / (0.5 + dt);
		DataStruct->angX = DataStruct->angX * filter_coefficient + roll * (1 - filter_coefficient);
		DataStruct->angY = DataStruct->angY * filter_coefficient + pitch * (1 - filter_coefficient);
		// DataStruct->angX -= DataStruct->offsAngX; 
		// DataStruct->angY -= DataStruct->offsAngY; 
		// DataStruct->angZ -= DataStruct->offsAngZ; 
		// DEBUG_SENSOR("MPU: angX: %.2f\t angY: %.2f\t angZ: %.2f\n", DataStruct->angX-DataStruct->offsAngX, DataStruct->angY-DataStruct->offsAngY, DataStruct->angZ-DataStruct->offsAngZ);
		// DEBUG_SENSOR("%.2f,%.2f,%.2f\r\n", roll, DataStruct->gyrX* dt, DataStruct->angX);
		return GYROSENSOR_Result_Ok;
	}
	return GYROSENSOR_Result_Error;
}

GYROSENSOR_Result GYROSENSOR_Mini_Update(GYROSENSOR* DataStruct)
{
	double dt;
	//read IMU data
	GYROSENSOR_ReadGyroscope(DataStruct);
	DataStruct->gyrX = ( ( DataStruct->Gyroscope_X ) - DataStruct->gyrXoffs) * DataStruct->Gyro_Mult;
	DataStruct->gyrY = ( ( DataStruct->Gyroscope_Y ) - DataStruct->gyrYoffs) * DataStruct->Gyro_Mult;
	DataStruct->gyrZ = ( ( DataStruct->Gyroscope_Z ) - DataStruct->gyrZoffs) * DataStruct->Gyro_Mult;
	
	dt = (double)(millis() - last_time) / 1000;
	last_time = millis();
  DataStruct->angZ += DataStruct->gyrZ * dt;
  DataStruct->angZ = DataStruct->angZ - 360 * floor(DataStruct->angZ / 360);
  if(DataStruct->angZ > 180)
  {
    DataStruct->angZ = DataStruct->angZ - 360;
  }
  /*
     complementary filter
     set 0.5sec = tau = dt * A / (1 - A)
     so A = tau / (tau + dt)
  */
	return GYROSENSOR_Result_Ok;
}

static struct matrixElement_t
{
	double a11;
	double a12;
	double a13;
	double a21;
	double a22;
	double a23;
	double a31;
	double a32;
	double a33;
} matrixElement;

#define PI 3.14159265359
float nearest(float val)
{
	return (roundf(val * 100) / 100);
}

GYROSENSOR_Result GYROSENSOR_DeviceCalibration(GYROSENSOR* DataStruct)
{
	uint16_t x = 0;
  uint16_t num = 50;
  long xAccelSum = 0, yAccelSum = 0, zAccelSum = 0, xGyroSum	= 0, yGyroSum = 0, zGyroSum = 0;
	uint8_t data[14];
	uint8_t reg = GYROSENSOR_ACCEL_XOUT_H;
	uint8_t address = DataStruct->Address;
	float z;
	char tem[100];
	
	DataStruct->offsAngX = DataStruct->offsAngY = DataStruct->offsAngZ = 0;
  for(x = 0; x < num; x++)
  {
		while(HAL_I2C_Master_Transmit(Handle, address, &reg, 1, 10000) != HAL_OK);	
		while(HAL_I2C_Master_Receive(Handle, address, data, 14, 10000) != HAL_OK);

	/* Format */
		xAccelSum += (int16_t)(data[0] << 8 | data[1]);
		yAccelSum += (int16_t)(data[2] << 8 | data[3]);
		z = (int16_t)(data[4] << 8 | data[5]);
//		if(z > 0) z -= (int16_t)(0.2/DataStruct->Acce_Mult);
//		else z += (int16_t)(0.2/DataStruct->Acce_Mult);
		zAccelSum += z;
		
		xGyroSum += (int16_t)(data[8] << 8 | data[9]);
		yGyroSum += (int16_t)(data[10] << 8 | data[11]);
		zGyroSum += (int16_t)(data[12] << 8 | data[13]);
		HAL_Delay(100);
  }
	DataStruct->accelXoffs = (xAccelSum / num);
  DataStruct->accelYoffs = (yAccelSum / num);
	DataStruct->accelZoffs = (zAccelSum / num);
	float t1, t2, t3;
	t1 = nearest(1.0*DataStruct->accelXoffs*DataStruct->Acce_Mult);
	t2 = nearest(1.0*DataStruct->accelYoffs*DataStruct->Acce_Mult);
	t3 = nearest(1.0*DataStruct->accelZoffs*DataStruct->Acce_Mult) - (float)0.2;
	
	sprintf(tem, "accX: %d, accY: %d, accZ: %d\r\n", (int)DataStruct->accelXoffs, (int)DataStruct->accelYoffs, (int)DataStruct->accelZoffs);
	Debug(tem);
//	DataStruct->rollCalib = atan2(DataStruct->accelYoffs, DataStruct->accelZoffs); // FORMULA FOUND ON INTERNET
//	DataStruct->pitchCalib = atan2((-1)*DataStruct->accelXoffs, sqrt(DataStruct->accelYoffs*DataStruct->accelYoffs + DataStruct->accelZoffs*DataStruct->accelZoffs)); //FORMULA FOUND ON INTERNET
	DataStruct->rollCalib = atan2(t2, t3); // FORMULA FOUND ON INTERNET
	DataStruct->pitchCalib = atan2((-1)*t1, sqrt(t2*t2 + t3*t3)); //FORMULA FOUND ON INTERNET
	
	// if(DataStruct->rollCalib < 0) DataStruct->rollCalib += 5;
	// else if(DataStruct->rollCalib > 0) DataStruct->rollCalib -= 5;
	// if(DataStruct->pitchCalib < 0) DataStruct->pitchCalib -= 10;
	// else if(DataStruct->pitchCalib > 0) DataStruct->pitchCalib += 10;
	// DEBUG_SENSOR("MPU: rollCalib: %.2f\t pitchCalib: %.2f\n", DataStruct->rollCalib, DataStruct->pitchCalib);
//	Debug("x\n");
	double sinp, cosp, sinr, cosr;
	sinr = sin(DataStruct->rollCalib);
	cosr = cos(DataStruct->rollCalib);
//	cosr = sqrt(1-sinr*sinr); 
	sinp = sin(DataStruct->pitchCalib);
	cosp = cos(DataStruct->pitchCalib);
//  cosp = sqrt(1-sinp*sinp);	
	Debug("x\n");
	matrixElement.a11 = nearest(cosp);
	matrixElement.a12 = nearest(sinp*sinr);
	matrixElement.a13 = nearest(sinp*cosr);
	matrixElement.a21 = 0;
	matrixElement.a22 = nearest(cosr);
	matrixElement.a23 = -1*nearest(sinr);
	matrixElement.a31 = -1*nearest(sinp);
	matrixElement.a32 = nearest(cosp*sinr);
	matrixElement.a33 = nearest(cosp*cosr);

//	matrixElement.a11 = nearest(cosp);
//	matrixElement.a21 = nearest(sinp*sinr);
//	matrixElement.a31 = nearest(sinp*cosr);
//	matrixElement.a12 = 0;
//	matrixElement.a22 = nearest(cosr);
//	matrixElement.a32 = -1*nearest(sinr);
//	matrixElement.a13 = -1*nearest(sinp);
//	matrixElement.a23 = nearest(cosp*sinr);
//	matrixElement.a33 = nearest(cosp*cosr);
	
//	matrixElement.a11 = 1;
//	matrixElement.a21 = 0;
//	matrixElement.a31 = 0;
//	matrixElement.a12 = 0;
//	matrixElement.a22 = 1;
//	matrixElement.a32 = 0;
//	matrixElement.a13 = 0;
//	matrixElement.a23 = 0;
//	matrixElement.a33 = 1;
	
	/*
	sprintf(tem, "a11: %d, a12: %d, a13: %d\r\na21: %d, a22: %d, a23: %d\r\na31: %d, a32: %d, a33: %d\r\n", 
			(int)(matrixElement.a11*1000),(int)(matrixElement.a12*1000),(int)(matrixElement.a13*1000),
				(int)(matrixElement.a21*1000),(int)(matrixElement.a22*1000),(int)(matrixElement.a23*1000),
					(int)(matrixElement.a31*1000),(int)(matrixElement.a32*1000),(int)(matrixElement.a33*1000));
*/
sprintf(tem, "a11: %.2f, a12: %.2f, a13: %.2f\r\na21: %.2f, a22: %.2f, a23: %.2f\r\na31: %.2f, a32: %.2f, a33: %.2f\r\n", 
			matrixElement.a11,matrixElement.a12,matrixElement.a13,
				matrixElement.a21,matrixElement.a22,matrixElement.a23,
					matrixElement.a31,matrixElement.a32,matrixElement.a33);
	Debug(tem);
  DataStruct->gyrXoffs = xGyroSum / num;
  DataStruct->gyrYoffs = yGyroSum / num;
	DataStruct->gyrZoffs = zGyroSum / num;
	
	last_accX = DataStruct->accelXoffs * DataStruct->Acce_Mult;
	last_accY = DataStruct->accelYoffs * DataStruct->Acce_Mult;
	last_accZ = DataStruct->accelZoffs * DataStruct->Acce_Mult;
	// DEBUG_SENSOR("MPU: gyrXoffs: %d\t gyrYoffs: %d\t gyrZoffs: %d\n", DataStruct->gyrXoffs, DataStruct->gyrYoffs, DataStruct->gyrZoffs);
	Debug("x\n");
	GYROSENSOR_Update(DataStruct);
	DataStruct->offsAngX = DataStruct->angX;
	DataStruct->offsAngY = DataStruct->angY;
	DataStruct->offsAngZ = DataStruct->angZ;

//	for(uint8_t i = 0; i < 2*NUM_OF_FILTER; i++){
//		GYROSENSOR_UpdateRotation(DataStruct);
//		HAL_Delay(100);
//	}
	GYROSENSOR_UpdateRotation(DataStruct);
	DataStruct->firstAccX = DataStruct->newAccX;
	DataStruct->firstAccY = DataStruct->newAccY;
	DataStruct->firstAccZ = DataStruct->newAccZ;
	
	return GYROSENSOR_Result_Ok;
}

//#define NUM_OF_FILTER 20
//float sum(float *arr, uint8_t len)
//{
//	uint8_t i;
//	float s = 0;
//	for(i = 0; i < len; i++) s += arr[i];
//	return s;
//}

//float filterX(float inputX)
//{
//	static float tempX[NUM_OF_FILTER] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//	float result;
//	uint8_t index;
//	tempX[NUM_OF_FILTER-1] = inputX;
//	result = sum(tempX, NUM_OF_FILTER)/(float)NUM_OF_FILTER;
//	for(index = 0; index < NUM_OF_FILTER-1; index++) tempX[index] = tempX[index+1];
//	tempX[NUM_OF_FILTER-1] = inputX;
//	return result;
//}

//float filterY(float inputY)
//{
//	static float tempY[NUM_OF_FILTER] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//	float result;
//	uint8_t index;
//	tempY[NUM_OF_FILTER-1] = inputY;
//	result = sum(tempY, NUM_OF_FILTER)/(float)NUM_OF_FILTER;
//	for(index = 0; index < NUM_OF_FILTER-1; index++) tempY[index] = tempY[index+1];
//	tempY[NUM_OF_FILTER-1] = inputY;
//	return result;
//}

//float filterZ(float inputZ)
//{
//	static float tempZ[NUM_OF_FILTER] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//	float result;
//	uint8_t index;
//	tempZ[NUM_OF_FILTER-1] = inputZ;
//	result = sum(tempZ, NUM_OF_FILTER)/(float)NUM_OF_FILTER;
//	for(index = 0; index < NUM_OF_FILTER-1; index++) tempZ[index] = tempZ[index+1];
//	tempZ[NUM_OF_FILTER-1] = inputZ;
//	return result;
//}

GYROSENSOR_Result GYROSENSOR_UpdateRotation(GYROSENSOR* DataStruct)
{
	if(GYROSENSOR_ProcessAccelerometer(DataStruct) != GYROSENSOR_Result_Ok) return GYROSENSOR_Result_Error;
	float xx, yy, zz;
	xx = (DataStruct->accX);
	yy = (DataStruct->accY);
	zz = (DataStruct->accZ) - 0.2;
//	xx = filterX(xx);
//	yy = filterY(yy);
//	zz = filterZ(zz);
	
	DataStruct->newAccX = nearest((matrixElement.a11*xx + matrixElement.a12*yy + matrixElement.a13*zz))*100;
	DataStruct->newAccY = nearest((matrixElement.a21*xx + matrixElement.a22*yy + matrixElement.a23*zz))*100;
	DataStruct->newAccZ = nearest((matrixElement.a31*xx + matrixElement.a32*yy + matrixElement.a33*zz))*100;
	
	// DEBUG_SENSOR("%d,%d,%d\n", DataStruct->newAccX, DataStruct->newAccY, DataStruct->newAccZ);
	
	return GYROSENSOR_Result_Ok;
}

GYROSENSOR_Result GYROSENSOR_DetectDanger(GYROSENSOR* DataStruct)
{	
	float dentaX, dentaY, dentaZ;
	dentaX = DataStruct->accX - last_accX;
	dentaY = DataStruct->accY - last_accY;
	dentaZ = DataStruct->accZ - last_accZ;
	
	if(dentaX < -DENTA_ACCEL_X || dentaX > DENTA_ACCEL_X) {/*DEBUG_SENSOR("MPU:----------------------------> X danger\n");*/}
	if(dentaY < -DENTA_ACCEL_Y || dentaY > DENTA_ACCEL_Y) {/*DEBUG_SENSOR("MPU:----------------------------> Y danger\n");*/}
	if(dentaZ < -DENTA_ACCEL_Z || dentaZ > DENTA_ACCEL_Z) {/*DEBUG_SENSOR("MPU:----------------------------> Z danger\n");*/}
	
	last_accX = DataStruct->accX;
	last_accY = DataStruct->accY;
	last_accZ = DataStruct->accZ;
	return GYROSENSOR_Result_Ok;
}
