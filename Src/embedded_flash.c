/* Includes ------------------------------------------------------------------*/
#include "embedded_flash.h"
#include "stm32f3xx_hal_flash.h"
#include "stm32f3xx_hal_flash_ex.h"
#include "debug.h"

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define EFLASH_BUFFER_SIZE			32

#define EFPAGE_SIZE  (uint16_t)0x800

#define EFLASH_START_PAGE			40	//26
#define EFLASH_END_PAGE				60	//31
#define EFLASH_NO_PAGE				(EFLASH_END_PAGE - EFLASH_START_PAGE + 1)

//#define EFLASH_PAGE_START_ADDRESS( _PAGE_ )		((uint32_t)0x08000000 + (uint32_t)_PAGE_ * EFPAGE_SIZE)
//#define EFLASH_PAGE_END_ADDRESS(_PAGE_)				((uint32_t)0x08000000 + (uint32_t)(_PAGE_+1) * EFPAGE_SIZE - 1)

static uint32_t EFLASH_PAGE_START_ADDRESS(int page){
	return ( 0x08000000 + (uint32_t)page*EFPAGE_SIZE );
}

static uint32_t EFLASH_PAGE_END_ADDRESS(int page){
	return( 0x08000000 + (uint32_t)(page+1) * EFPAGE_SIZE - 1 );
}

#define EFLASH_START_ADDRESS	EFLASH_PAGE_START_ADDRESS(EFLASH_START_PAGE)
#define EFLASH_END_ADDRESS		EFLASH_PAGE_END_ADDRESS(EFLASH_END_PAGE)

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static uint16_t 	pu16EFlashBuffer[EFLASH_BUFFER_SIZE];
static uint32_t 	EF_data_address = 0x00000000;
static char tmpStr[256];
static bool flashEmpty = false;
/* Global variable used to store variable value in read sequence */
/* Virtual address defined by the user: 0xFFFF value is prohibited */
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
void EF_Init() {
	/*find current data page*/
	int page;
	for (page = EFLASH_END_PAGE; page>EFLASH_START_PAGE; page--) {
		if(*(__IO uint16_t*)EFLASH_PAGE_START_ADDRESS(page) != 0xFFFF) break;
	}
//	DebugPutString("current page ")
	DEBUG(tmpStr,"Current page %d\r\n",page);
	/*find EF_data_address*/
	uint32_t address = EFLASH_PAGE_END_ADDRESS(page) - EFLASH_BUFFER_SIZE*2 + 1;
	while (address>EFLASH_PAGE_START_ADDRESS(page)) {
		if(*(__IO uint16_t*)address != 0xFFFF) break;
		else address -= 2*EFLASH_BUFFER_SIZE;
	}
	EF_data_address = address;
	DEBUG(tmpStr,"EF_data_address 0x%08x\r\n", EF_data_address);
	if(*(__IO uint16_t*)EF_data_address == 0xFFFF) { /*all EFLASH is empty*/
		DEBUG(tmpStr,"Flash empty\r\n");
		flashEmpty = true;
	} else {
		for(int i= 0; i< EFLASH_BUFFER_SIZE; i++){
			DEBUG(tmpStr,"0x%04x 0x%08x", *(__IO uint16_t*)(EF_data_address + i*2), EF_data_address + i*2);
			DebugPutString("\r\n");
		}
	}
}
uint8_t EF_WriteVariable(const uint16_t* Data, uint8_t Len){
	if(EF_data_address == 0x00000000) EF_Init();
	if(Len > EFLASH_BUFFER_SIZE - 2) return EFLASH_LEN_ERROR;
	/*move data into pu8EFlashBuffer, first and last byte used for */
	memset(pu16EFlashBuffer,0,sizeof(pu16EFlashBuffer));
	for (int i=0; i<Len; i++){
		pu16EFlashBuffer[i+1] = Data[i];
	}
	
//	for(int i=0; i<EFLASH_BUFFER_SIZE; i++){
//		DEBUG(tmpStr,"0x%04x\r\n", pu16EFlashBuffer[i]);
//	}
	/**/
	if(!flashEmpty)
		EF_data_address += EFLASH_BUFFER_SIZE*2;
	
	if(EF_data_address > EFLASH_END_ADDRESS){	/*Flash full*/
		DebugPutString("Flash full\r\n");
		taskENTER_CRITICAL();
		uint32_t SectorError=0;
		FLASH_EraseInitTypeDef EraseInitStruct;
		HAL_FLASH_Unlock();
		/*Erase first page*/
		EraseInitStruct.NbPages = 1;
		EraseInitStruct.PageAddress = EFLASH_START_ADDRESS;
		EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
		if (HAL_FLASHEx_Erase(&EraseInitStruct,&SectorError)!=HAL_OK){
			HAL_FLASH_Lock(); taskEXIT_CRITICAL();
			return EFLASH_ERASE_ERROR;
		}
//		HAL_FLASH_Lock();
		/*EF_data_address to begin of EFLASH*/
		EF_data_address = EFLASH_START_ADDRESS;
		/*Write data to first page*/
//		HAL_FLASH_Unlock();
		for(int i=0; i<EFLASH_BUFFER_SIZE; i++){
			if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, EF_data_address+i*2, pu16EFlashBuffer[i])!=HAL_OK){
				HAL_FLASH_Lock(); taskEXIT_CRITICAL();
				return EFLASH_WRITE_ERROR;
			}	
			DEBUG(tmpStr,"Write 0x%04x 0x%08x\r\n", pu16EFlashBuffer[i], EF_data_address+i*2);
		}
//		HAL_FLASH_Lock();
		/*Erase other page*/
//		HAL_FLASH_Unlock();
//		SectorError = 0;
		uint32_t page = EFLASH_START_PAGE+1;
		EraseInitStruct.NbPages = EFLASH_NO_PAGE - 1;
		EraseInitStruct.PageAddress = EFLASH_PAGE_START_ADDRESS(page);
		EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
		DEBUG(tmpStr,"No pages %d, page address 0x%08x, page %d",EraseInitStruct.NbPages, EFLASH_PAGE_START_ADDRESS(EFLASH_START_PAGE+1), EFLASH_START_PAGE+1);
		if(HAL_FLASHEx_Erase(&EraseInitStruct,&SectorError)!=HAL_OK){
			HAL_FLASH_Lock(); taskEXIT_CRITICAL();
			return EFLASH_ERASE_ERROR;
		}
		taskEXIT_CRITICAL();
		HAL_FLASH_Lock();
	} else { /*Normal write*/
		HAL_FLASH_Unlock();
		for(int i=0; i<EFLASH_BUFFER_SIZE; i++){
			if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, EF_data_address+i*2, pu16EFlashBuffer[i])!=HAL_OK){
				HAL_FLASH_Lock(); return EFLASH_WRITE_ERROR;
			}
//			DEBUG(tmpStr,"Write 0x%04x 0x%08x\r\n", pu16EFlashBuffer[i], EF_data_address+i*2);
		}
		HAL_FLASH_Lock();
	}
	flashEmpty = false;
	DEBUG(tmpStr,"EF_data_address 0x%08x\r\n", EF_data_address);
	return EFLASH_OK;
}
uint8_t EF_ReadVariable(uint16_t* Data, uint8_t Len){
	if(flashEmpty) return EFLASH_EMPTY;
	if(EF_data_address == 0x00000000) EF_Init();
	if(Len > EFLASH_BUFFER_SIZE - 2) return EFLASH_LEN_ERROR;
	for(int i=0; i<Len; i++){
		Data[i] = *(__IO uint16_t*)(EF_data_address + i*2 + 2);
//		DEBUG(tmpStr,"Read 0x%04x 0x%08x\r\n",Data[i], (EF_data_address + i*2 + 2));
	}
	return EFLASH_OK;
}

