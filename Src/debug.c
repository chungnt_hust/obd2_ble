/**
 * @file debug.c
 * @brief debug task via uart for stm32f1 (odbi adapter)
 * @date April 20, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */
#include "debug.h"
#include "ble.h"
#include "utils.h"
#include "protocol.h"
#include <stdlib.h>

extern QueueHandle_t xRxDebugQueue;
extern osSemaphoreId g_ODB2_Semid;
extern osSemaphoreId g_BLESemid;
extern ODB2Message g_ODB2Msg;
extern ODB2_PROTOCOL g_ODB2Protocol;
uint16_t u16DebugRxCount = 0;
uint8_t pu8DebugBuf[DEBUG_RX_BUF_SIZE+2];
uint8_t pu8DebugBufTrimed[DEBUG_RX_BUF_SIZE+2];
char pu8TempStr[128];

void DebugPutString(char *str)
{
#if 1
    while(*str !='\0')
    {
        HAL_UART_Transmit(&UART_DEBUG, (uint8_t *)str, 1, 0xFFFF);
        str++;
    }
#endif
}

void DebugPutChar(char c)
{
#if 1
    HAL_UART_Transmit(&UART_DEBUG, (uint8_t*) &c, 1, 0xFFFF);
#endif
}
void DebugEndLine()
{
    DebugPutChar(0x0A);
}

HAL_StatusTypeDef DebugReadChar(char* c)
{
    return HAL_UART_Receive(&UART_DEBUG, (uint8_t*) c, 1, 0xFFFF);
}

void DebugProcessReceiveByte(uint8_t c)
{
    //DebugPutChar('.');
    pu8DebugBuf[u16DebugRxCount++] = c;
    int i = 0; //temp index
    int iRet = 0;// result;
    (void)iRet;
    if ((c == 0x0A) ||( u16DebugRxCount >= DEBUG_RX_BUF_SIZE))
    {
        DebugPutString("\r\nDebug: Received Full Message: ");
        for ( i = 0; i < u16DebugRxCount; i++)
        {
            DebugPutChar(pu8DebugBuf[i]);
        }
        // TODO process received message
        //iRet = messageHandle(pu8DebugBuf, u16DebugRxCount, MESSAGE_TYPE_DEBUG);
				if (g_ODB2Protocol != ODB2_PROTOCOL_START) {
            iRet = BleMessageHandleFull(pu8DebugBuf, u16DebugRxCount);
            //iRet = messageHandle(pu8BleBuf, u16BleRxCount, MESSAGE_TYPE_BLE);
            if (iRet == BLE_MESSAGE_HANDLE_OK) {
                // Release Semaphore for other Task
                osSemaphoreRelease(g_ODB2_Semid);
                // Wait for return osSemaphoreRelease
                iRet = osSemaphoreWait(g_BLESemid, 2000);
                if (iRet != osOK) {
                    // maybe timeout --> reply to ble NODATA
                    DebugPutString("\r\n Ble No response from ODB2 Thread");
                    BleReplyNoData();
                } 
								else {
                    // TODO: reply to ble odb2 message response
                    BleReplySuccessRequest();
                    //DebugPutString("\r\n Ble received response from ODB2 Thread");
                }
            } else { //iRet == BLE_MESSAGE_HANDLE_FAIL
                if (g_ODB2Msg.u8Status == ODB2_STATUS_START) {
                    DebugPutString("\r\n Ble Message Handle Fail");
                    // reply to ble NODATA
                    //BleReplyNoData();
                }
            }
        }
        u16DebugRxCount = 0; // reset count
        memset(pu8DebugBuf, 0x00, sizeof(pu8DebugBuf));
        //u16DebugRxCount = 0; // reset count
    }
}

void DebugProcessTask(void){
    uint8_t xC;
    while (1)
    {
        if (xRxDebugQueue != 0)
        {
            if( xQueueReceive( xRxDebugQueue, &( xC ), ( TickType_t ) 10 ) )
            {
                // Receive a message from queue, process the byte
                //DebugPutChar('*');
                DebugProcessReceiveByte(xC);
            }
        }
    }
}

