#include "j1850_pwm_brutal.h"
#include "debug.h"
uint8_t  pwm_u8TxByteBuf[12]={0x61,0x6A,0xF1,0x01,0x00};
uint8_t pwm_pu8TxPulseBuf[256];
uint16_t pwm_u16PulsleCount;
uint8_t ackTime;
uint8_t ackPulse[8];
uint8_t receivePulse[2000];
uint16_t receivePulseCount;
uint8_t pwm_pu8ReceiveAckPulse[16] = {8,16,8,16,8,16,8,16,16,8,16,8,16,8,8,70};
uint8_t pwm_pu8ReceiveByte[56];
uint16_t pwm_u16ReceiveByteCount;
char temp[256];
void brutalPwmInit(void){
	GPIO_InitTypeDef GPIO_InitStruct;
	
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12|GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin : PA4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 PB1 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
 
  /*Configure GPIO pins : PB12 PB7 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

uint16_t pwm_calculate_pulse_with_sof(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf) { //do dai toi da cua ban tin la 12 bytes
    pu8PulseBuf[0] = TP7_TX_NOM;
    pu8PulseBuf[1]= TP4_TX_NOM - TP7_TX_NOM;
    uint16_t u16Ret;
    u16Ret = pwm_calculate_pulse(pu8Data, u16Leng, &pu8PulseBuf[2]);
    return u16Ret + 2;
}

uint16_t pwm_calculate_pulse(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf) { //do dai toi da cua ban tin la 12 bytes
    if (u16Leng>0&&u16Leng<=12) {
        for (int i=0;i<u16Leng;i++)  {
            pwm_calculate_pulse_byte(pu8Data[i], &pu8PulseBuf[i*16]);
        }
        return u16Leng*16;
    }
}

void pwm_calculate_pulse_byte(uint8_t u8Data, uint8_t *pu8Buf) {
    int a;uint8_t BitCheck;
    for (a=0;a<8;a++) {
        BitCheck=u8Data&(0x80>>a);
        pwm_bit_to_pulse(BitCheck, &pu8Buf[a*2]);
    }
}

void pwm_bit_to_pulse(uint8_t BitValue, uint8_t* pu8Buf) {
    if (BitValue==0) {
        pu8Buf[0]= TP2_TX_NOM;
        pu8Buf[1]= TP3_TX_NOM - pu8Buf[0];
    }
    else {
        pu8Buf[0]=TP1_TX_NOM;
        pu8Buf[1]=TP3_TX_NOM - pu8Buf[0];
    }
}

PWM_SEND pwm_send_cmd(uint8_t u8mode, uint8_t u8pid){
	pwm_u8TxByteBuf[3] = u8mode;
	pwm_u8TxByteBuf[4] = u8pid;
	pwm_u8TxByteBuf[5] =  j1850_crc(pwm_u8TxByteBuf,5);
	pwm_u16PulsleCount = pwm_calculate_pulse_with_sof(pwm_u8TxByteBuf, 6, pwm_pu8TxPulseBuf);
	return pwm_send(pwm_pu8TxPulseBuf,pwm_u16PulsleCount);
}

uint8_t j1850_crc(uint8_t *msg_buf, int8_t nbytes) {
    uint8_t crc_reg=0xff,poly,byte_count,bit_count;
    uint8_t *byte_point;
    uint8_t bit_point;
    for (byte_count=0, byte_point=msg_buf; byte_count<nbytes; ++byte_count, ++byte_point) {
        for (bit_count=0, bit_point=0x80 ; bit_count<8; ++bit_count, bit_point>>=1) {
            if (bit_point & *byte_point) {	// case for new bit = 1
                if (crc_reg & 0x80)
                    poly=1;	// define the polynomial
                else
                    poly=0x1c;
                crc_reg= ( (crc_reg << 1) | 1) ^ poly;
            }
            else	{	// case for new bit = 0
                poly=0;
                if (crc_reg & 0x80)
                    poly=0x1d;
                crc_reg= (crc_reg << 1) ^ poly;
            }
        }
    }
    return ~crc_reg;	// Return CRC
}

PWM_SEND pwm_send(uint8_t* pu8PulseBuff, uint16_t u16PulseCount){
	int i=0;
	int count;
	while (i<u16PulseCount){
		PWM_OUT_HIGH();
		__delay_us(pu8PulseBuff[i++]);
		if (HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
			return PWM_SEND_NOK;
		}
		PWM_OUT_LOW();
		__delay_us(pu8PulseBuff[i++]);
		if (HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_SET){
			return PWM_SEND_OVER_WRITE;
		}
	}
	count=0;
	while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
		__delay_us(1);
		if(count++>TP4_TX_NOM) {
			return PWM_SEND_BUT_NO_ACK;
		}
	}
	ackTime = count;
	for(i=0;i<8;i++){
		count=0;
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
			__delay_us(1);
			if(count++>10){
				return PWM_SEND_BUT_ACK_WRONG;
			}
		}
		count=0;
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_SET){
			__delay_us(1);
			if(count++>10){
				return PWM_SEND_BUT_ACK_WRONG;
			}
		}
		ackPulse[i] = count;
	}
	return PWM_SEND_OK;
}

void pwm_communication(uint8_t u8mode, uint8_t u8pid){
	int i;
	pwm_send_cmd(u8mode,u8pid);
	pwm_read();
	sprintf (temp,"\r\nget ack after %u us", ackTime);
	DebugPutString(temp);
	for(i=0;i<8;i++){
		sprintf(temp,"\r\nACK pulse number %u = %u us",i,ackPulse[i]);
		DebugPutString(temp);
	}
	sprintf(temp,"\r\nNumber of received bits is %u",receivePulseCount);
	DebugPutString(temp);
//	for(i=0;i<receivePulseCount;i++){
//		sprintf(temp,"\r\nRECEIVE pulse number %u = %u us",i,receivePulse[i]);
//		DebugPutString(temp);
//	}
	if (receivePulse[0]<15) {
		DebugPutString("\r\nMessage received had wrong SOF");
		return;
	}
	pwm_u16ReceiveByteCount = receivePulseCount/8;
	for (i=0;i<pwm_u16ReceiveByteCount;i++){
		pwm_pu8ReceiveByte[i] = pwm_pulse_to_byte(&receivePulse[1+i*8]);
		sprintf(temp,"\r\nRECEIVED byte number %u = 0x%02x",i,pwm_pu8ReceiveByte[i]);
		DebugPutString(temp);
	}
}

PWM_RECEIVE pwm_read(void){
	int count;
	count=0;
	__delay_us(20);
	while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
		__delay_us(1);
		if(count++>1000){
			return PWM_RECEIVE_NOTHING;
		}
	}
	receivePulseCount = 0;
	while(1){
		count=0;
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_SET){
			__delay_us(1);
			if(count++>TP7_TX_NOM){
//				DebugPutString("\r\nPWM receive wrong");
				return PWM_RECEIVE_WRONG;
			}
		}
		receivePulse[receivePulseCount++]=count;
		if (count>12){
			while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
			__delay_us(1);
			if(count++>30){
//					DebugPutString("\r\nPWM receive wrong");
					return PWM_RECEIVE_WRONG;
				}
			}
		} else {
			while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
			__delay_us(1);
			if(count++>20){
					pwm_send_ack();
//					DebugPutString("\r\nPWM receive ok");
					return PWM_RECEIVE_OK;
				}
			}
		}	
	}
}

void pwm_send_ack(void){
	int i=0;
	while(i<16){
		PWM_OUT_HIGH();
		__delay_us(pwm_pu8ReceiveAckPulse[i++]);
		PWM_OUT_LOW();
		__delay_us(pwm_pu8ReceiveAckPulse[i++]);
	}
	return;
}

uint8_t pwm_pulse_to_byte(uint8_t* pu8PusleBuf){
	uint8_t ret =0;
	for(int i=0; i<8; i++){
		ret <<= 1;
		if (pu8PusleBuf[i]<6){
		
			ret |= 0x01;
		}else {
			//	ret &= 0xfe;
		}
	}
	return ret;
}