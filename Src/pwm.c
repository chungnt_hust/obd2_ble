#include "pwm.h"
#include "string.h"
static char tmpStr[256];
//cac ham truyen 
extern int u32CountBit,u32dem;
static uint8_t PWM_u8DataSend[12]={0x61,0x6A,0xF1,0x01,0x00};
//static uint8_t PWM_u8DataSend[12]={0x68,0x6A,0xF1,0x01,0x00};
extern uint16_t u16PulseToSAE[256];
extern uint16_t u16compare;
extern int C3,C4;
void SAE_PWM_TxProcessTask(void)
{
    //DebugPutString(temp);
    PWM_u8DataSend[5]=j1850_crc(PWM_u8DataSend,5);
    PWM_CalPulseToSAEBus(PWM_u8DataSend,6);
    u32dem=0;
    __HAL_TIM_SET_COMPARE(&TxTIMER,TIM_CHANNEL_2,1000); // gia tri set_compare khong duoc de nho(gan 0)
    HAL_TIM_OC_Start_IT(&TxTIMER, TIM_CHANNEL_2);
}
void PWM_CalPulseToSAEBus(uint8_t* pu8Data,uint16_t u16Leng)//do dai toi da cua ban tin la 12 bytes
{
    if (u16Leng>0&&u16Leng<=12)
    {
        //u16PulseToSAE[0]=100;
        u16PulseToSAE[0]=Tp7;
        u16PulseToSAE[1]=Tp10-Tp7;
        u32CountBit=2;
        for (int i=0;i<u16Leng;i++)
        {
            PWM_CalPulseByte(pu8Data[i]);
        }
        //u16PulseToSAE[u32CountBit++]=Tp5-Tp1;
    }
}
void PWM_CalPulseByte(uint8_t u8Data)
{
    int a;uint8_t BitCheck;
    for (a=0;a<8;a++)
    {
        BitCheck=u8Data&(0x80>>a);//(0x80>>a);
        PWM_BitToPulse(BitCheck);
    }
}

void SAE_PWM_SetPIDMode(uint8_t mode, uint8_t pid)
{
    PWM_u8DataSend[SAE_PWM_MODE_IDX] = mode;
    PWM_u8DataSend[SAE_PWM_PID_IDX] = pid;
}

void PWM_BitToPulse(uint8_t BitValue)
{
    if (BitValue==0)
    {
        u16PulseToSAE[u32CountBit++]=Tp2-3;
        u16PulseToSAE[u32CountBit++]=Tp3-Tp2;
    }
    else
    {
        u16PulseToSAE[u32CountBit++]=Tp1-3;
        u16PulseToSAE[u32CountBit++]=Tp3-Tp1;
    }
}
//cac ham nhan
volatile uint16_t u16PWMRxPulseCount;
uint16_t* pu16PWMRxBitPulse;
uint16_t u16PWMRxBitCount;
uint8_t* pu8PWMRxBitValue;
uint16_t u16PWMNumberOfRxBytes;
uint8_t* pu8PWMRxByteValue;
extern uint16_t u8j1850_Rx_Frame_Count;
extern uint16_t u8j1850_Rx_Start_Byte[J1850_MAX_RX_FRAME_COUNT + 1];
extern uint16_t u8j1850_Rx_Stop_Byte[J1850_MAX_RX_FRAME_COUNT + 1];
void SAE_PWM_StopReceive(void)
{
    //u16PWMRxBitCount = 0;
    HAL_TIM_IC_Stop_IT(&RxTIMER,TIM_CHANNEL_3);
    HAL_TIM_IC_Stop_IT(&RxTIMER,TIM_CHANNEL_4);
}
void SAE_PWM_StartReceive(void)
{
    //u16PWMRxBitCount = 0;
    HAL_TIM_IC_Start_IT(&RxTIMER,TIM_CHANNEL_3);
    HAL_TIM_IC_Start_IT(&RxTIMER,TIM_CHANNEL_4);
}
uint16_t SAE_PWM_RxProcessTask(void)
{
    int i;
    sprintf(tmpStr,"\r\nPWM bit count = %u\r\n",u16PWMRxPulseCount);
    DebugPutString(tmpStr);
    uint8_t u8Bit;
    u16PWMRxBitCount=0;
    bool isSOF = false;
    u8j1850_Rx_Frame_Count = 0;
    for (i=0; i < u16PWMRxPulseCount; i++)
    {
        // Binh process code
        //        if ((i%2)==0 && i>0)
        //        {
        //            pu8PWMRxBitValue[u16PWMRxBitCount++]=PWM_CalBitValue(pu16PWMRxBitPulse[i]);
        //            sprintf(tmpStr, "\r\npulse[%d]: %d, bit = %d", i, pu16PWMRxBitPulse[i], pu8PWMRxBitValue[u16PWMRxBitCount-1]);
        //            DebugPutString(tmpStr);
        //        } else {
        ////            if (pu16PWMRxBitPulse[i] > TP5_TX_MIN && (i > 2)) {
        //                sprintf(tmpStr, "\r\npulse[%d]: %d", i, pu16PWMRxBitPulse[i]);
        //                DebugPutString(tmpStr);
        ////                break;
        ////            }
        //        }

        if (i & 0x01) {
            u8Bit = PWM_CalBitValue2(pu16PWMRxBitPulse[i-1], pu16PWMRxBitPulse[i]);
            
            if (u8Bit == PWM_SOF_BIT_VAL) {
                //DebugPutString("\r\n ============== SOF ============== \r\n");
                u8j1850_Rx_Start_Byte[u8j1850_Rx_Frame_Count] = u16PWMRxBitCount/8;
                isSOF = true;
            }
            else if (u8Bit == PWM_EOD_BIT_VAL || u8Bit == PWM_EOF_BIT_VAL)
            {
                if (isSOF) {
                    if (pu16PWMRxBitPulse[i-1] > MAX_TP1)
                        pu8PWMRxBitValue[u16PWMRxBitCount++] = 0;
                    else
                        pu8PWMRxBitValue[u16PWMRxBitCount++] = 1;
                    if (u8j1850_Rx_Frame_Count < J1850_MAX_RX_FRAME_COUNT) {
                        u8j1850_Rx_Stop_Byte[u8j1850_Rx_Frame_Count] = u16PWMRxBitCount/8;
                        //                        sprintf(tmpStr, "\r\n [%d] start: %d stop: %d",u8j1850_Rx_Frame_Count, u8j1850_Rx_Start_Byte[u8j1850_Rx_Frame_Count],
                        //                                u8j1850_Rx_Stop_Byte[u8j1850_Rx_Frame_Count]);
                        //                        DebugPutString(tmpStr);
                        u8j1850_Rx_Frame_Count++;
                    }
                }
                isSOF = false;
                //                if (u8Bit == PWM_EOD_BIT_VAL)
                //                    DebugPutString("\r\n ============== EOD ============== \r\n");
                //                else
                //                    DebugPutString("\r\n ============== EOF ============== \r\n");
            }
            // only receive frame not ifr
            else if (isSOF) {
                if (u8Bit == 1 || u8Bit == 0) {
                    pu8PWMRxBitValue[u16PWMRxBitCount++] = u8Bit;
                }
            }
            //            sprintf(tmpStr, "\r\npu16PWMRxBitPulse[%d], bit count = [%d] = [%d, %d], u8Bit = 0x%02X ", i, u16PWMRxBitCount, pu16PWMRxBitPulse[i-1], pu16PWMRxBitPulse[i], u8Bit);
            //            DebugPutString(tmpStr);
        }
    }
    u16PWMNumberOfRxBytes=(u16PWMRxBitCount+1)/8;
    //memset (pu8PWMRxByteValue,0,12);
    sprintf(tmpStr, "\r\nPWM Received %d Bytes: \r\n", u16PWMNumberOfRxBytes);
    DebugPutString(tmpStr);
    for (i = 0; i < u8j1850_Rx_Frame_Count; i++)
    {
        sprintf(tmpStr, "pwm recv [%d] start %d, stop %d", i, u8j1850_Rx_Start_Byte[i], u8j1850_Rx_Stop_Byte[i]);
        DebugPutString(tmpStr);
    }
    for (i=0;i<u16PWMNumberOfRxBytes;i++)
    {
        pu8PWMRxByteValue[i] =	PWM_BitsToByte(&pu8PWMRxBitValue[i*8]);
        sprintf(tmpStr," 0x%02x ",pu8PWMRxByteValue[i]);
        DebugPutString(tmpStr);
    }
    return u16PWMNumberOfRxBytes;
}

static inline uint8_t PWM_CalBitValue2(uint16_t u16Pulse1, uint16_t u16Pulse2)
{
    //DebugPutChar('$');
    if (MIN_TP1<=u16Pulse1&&u16Pulse1<=MAX_TP1 && MIN_TP3<=u16Pulse2&&u16Pulse2<=MAX_TP3) return 0x01;
    if (MIN_TP2<=u16Pulse1&&u16Pulse1<=MAX_TP2 && MIN_TP3<=u16Pulse2&&u16Pulse2<=MAX_TP3) return 0x00;
    // EOF
    if (MIN_TP5<= u16Pulse2 && u16Pulse2 > MAX_TP4)
        return PWM_EOF_BIT_VAL;
    if (u16Pulse2 >= MIN_TP4 && u16Pulse2 <= MAX_TP4)
    {
        if (u16Pulse1 >= MIN_TP7 && u16Pulse1 <= MAX_TP7) {
            return PWM_SOF_BIT_VAL;
        }
        else {
            return PWM_EOD_BIT_VAL;
        }
    }
    return PWM_ERR_BIT_VAL;
}

static uint8_t PWM_CalBitValue(uint16_t u16Pulse)
{
    if (MIN_TP1<=u16Pulse&&u16Pulse<=MAX_TP1) return 0x01;
    if (MIN_TP2<=u16Pulse&&u16Pulse<=MAX_TP2) return 0x00;
    return 0x00;
}
static uint8_t PWM_BitsToByte(uint8_t* pu8Value)
{
    uint8_t FinalValue=0;
    for (int i=0;i<8;i++)
    {
        FinalValue+=(pu8Value[i]<<(7-i));
    }
    return FinalValue;
}


void SAE_PWM_Transmit(uint8_t mode, uint8_t pid, uint8_t u8PidBytes)
{
    SAE_PWM_SetPIDMode(mode, pid);
    if (u8PidBytes > 1)
    {
        sprintf(tmpStr,"\r\n VMP unsupported number of bytes %d", u8PidBytes );
        return;
    }
    uint8_t u8Len = 4 + u8PidBytes;
    PWM_u8DataSend[u8Len]=j1850_crc(PWM_u8DataSend,u8Len);
    PWM_CalPulseToSAEBus(PWM_u8DataSend,u8Len+1);
    u32dem=0;
    __HAL_TIM_SET_COMPARE(&TxTIMER,TIM_CHANNEL_2,1000); // gia tri set_compare khong duoc de nho(gan 0)
    HAL_TIM_OC_Start_IT(&TxTIMER, TIM_CHANNEL_2);

}
