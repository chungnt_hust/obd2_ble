/**
 * @file utils.c
 * @brief useful functions for stm32f1 (odbi adapter)
 * @date May 04, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */

#include "utils.h"
#include "debug.h"
#include "j1850.h"

uint8_t u8OS_Started = 0;
static bool timeOut = true;
static char tmpStr[128];
void DelayMs(uint32_t milisec)
{
    if (u8OS_Started)
    {
        osDelay(milisec);
    } else {
        HAL_Delay(milisec);
    }
}


void Utils_Timer1_Callback  (void const *arg)
{
    j1850_check_recv_status();
    timeOut = true;
}


void timerOnceStart(uint32_t timeout)
{
    timeOut = false;
    osStatus status = osTimerStart (Timer1Handle, timeout);
    if (status != osOK) {
          // Timer could not be started
        DebugPutString("\r\nCould not start Timer");
    }
}

bool isTimeOut()
{
    return timeOut;
}

void stopTimerOnce()
{
    timeOut = false;
    osStatus status = osTimerStop (Timer1Handle);
    if (status != osOK) {
          // Timer could not be started
        DebugPutString("\r\nCould not stop Timer");
    }
}

uint8_t toLowerCase(uint8_t x)
{
    uint8_t ret = x;
    if (x >= 'A' && x <= 'Z')
    {
        ret = x|0x60;
    }
    return ret;
}

uint8_t isValidHex(uint8_t x)
{
    if (( '0' <= x && x <= '9') || ('A' <= x && x <= 'F') || ('a' <= x && x <= 'f'))
        return 1;
    else return 0;
}
uint8_t validHex2Value(uint8_t x)
{
    if ( '0' <= x && x <= '9')
    {
        return (x -'0');
    }
    else {
        return (toLowerCase(x) - 'a' + 10);
    }
}

uint8_t hextoByte(uint8_t* hex, uint8_t* byte)
{
    uint8_t high,low;
    if (isValidHex(hex[0]) && isValidHex(hex[1]))
    {
        high = validHex2Value(hex[0]);
        low = validHex2Value(hex[1]);
        *byte = ((high<<4) | (0x0F & low));
        return 1;
    }
    else
    {
        return 0;
    }
}


void hex2char(uint8_t* pu8Data, uint8_t data)
{
    pu8Data[0]=byte2char((data&0xF0)>>4);
    pu8Data[1]=byte2char(data&0x0F);
    pu8Data[2]=' ';
}
uint8_t byte2char(uint8_t byte)
{
    if (byte>=0&&byte<=9) return (byte+'0');
    else if (byte>=10&&byte<=15) return (byte-10+'A');
    else return NULL;
}

void dumpHex(uint8_t *puData, uint32_t u32Len)
{
    uint32_t i;
		DebugPutString("\r\n");
    for (i = 0; i < u32Len; i++)
    {
				if (i% 8 == 0) 
					DebugPutString("\r\n");
        sprintf(tmpStr, " 0x%02X", puData[i]);
        DebugPutString(tmpStr);
			  
    }
		DebugPutString("\r\n");
}
