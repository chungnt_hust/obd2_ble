/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */
#include "ble.h"
#include "debug.h"
#include "can.h"
#include "kwp.h"
#include "j1850.h"
#include "j1850pwm.h"
#include "protocol.h"
#include "utils.h"
#include "vpw.h"
#include "eeprom.h"
#include "embedded_flash.h"
#include "trip_data.h"
#include "gyro_scope.h"
#define IGNORE_SEA_TASK 1
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan;

TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;

osThreadId defaultTaskHandle;
osTimerId Timer1Handle;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
osThreadId debugTaskHandle;
osThreadId bleTaskHandle;
osThreadId canTaskHandle;
osThreadId saeTaskHandle;
osThreadId kwpTaskHandle;
osThreadId gyroSensorTaskHandle;

QueueHandle_t xRxDebugQueue;
QueueHandle_t xRxBleQueue;
static char tmpStr[256];
char bleRx[BLE_RX_BUF_SIZE];
uint8_t count_bleRx=0;
char debugRx[DEBUG_RX_BUF_SIZE];
uint8_t count_debugRx=0;
char my_date[] = "\r\nDate = " __DATE__; 
char my_time[] = "\r\nTime = " __TIME__; 
ODB2_PROTOCOL g_ODB2Protocol = ODB2_PROTOCOL_START;
// variables for processing odb2 package
ODB2Message g_ODB2Msg;
osSemaphoreId g_ODB2_Semid;
osSemaphoreId g_ODB2_SAE_Semid;
osSemaphoreId g_ODB2_CAN_Semid;
osSemaphoreId g_ODB2_KWP_Semid;
osSemaphoreId g_BLESemid;

bool g_sleep = false;
bool g_gyro = false;

uint32_t batt;
bool g_ToyotaMode01Increase = false;
bool g_gyroRequest = false;
bool g_authen = true;
bool g_authenFistStep = false;
bool g_obdInit = true;
int obdCount = 0;
bool g_ObdInitingCustom = false;
bool g_InfoAutoSend = false;
float g_battValue;
float g_battA, g_battB;
float g_battOffset;
float g_battSleepValue;
bool 	g_wakeUp = false;

uint16_t VirtAddVarTab[NB_OF_VAR] = {0x5555, 0x6666, 0x7777};

char ahg_system_string[] =
        "\r\n================================================================================================\r\n\r\n"
        "   _|_|    _|    _|    _|_|_|        _|_|_|                        _|                              \r\n"
        " _|    _|  _|    _|  _|            _|        _|    _|    _|_|_|  _|_|_|_|    _|_|    _|_|_|  _|_|   \r\n"
        " _|_|_|_|  _|_|_|_|  _|  _|_|        _|_|    _|    _|  _|_|        _|      _|_|_|_|  _|    _|    _| \r\n"
        " _|    _|  _|    _|  _|    _|            _|  _|    _|      _|_|    _|      _|        _|    _|    _|  \r\n"
        " _|    _|  _|    _|    _|_|_|      _|_|_|      _|_|_|  _|_|_|        _|_|    _|_|_|  _|    _|    _|  \r\n"
        "                                                   _|                                                \r\n"
        "                                               _|_|                                                  \r\n"
        "\r\n===============================================================================================\r\n";
#define POWER_SAVE_PIN		GPIO_PIN_12
#define POWER_SAVE_PORT		GPIOB

#define PERIPHERAL_POWER_OFF()		HAL_GPIO_WritePin(POWER_SAVE_PORT,POWER_SAVE_PIN, GPIO_PIN_SET)
#define PERIPHERAL_POWER_ON()			HAL_GPIO_WritePin(POWER_SAVE_PORT,POWER_SAVE_PIN, GPIO_PIN_RESET)

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_CAN_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART3_UART_Init(void);
void StartDefaultTask(void const * argument);

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
                                

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
// Private Tasks
void DebugTask(void const * argument);
void BleTask(void const* argument);
void CanTask(void const * argument);
void SaeTask(void const* argument);
void KwpTask(void const* argument);
void GyroSensorTask(void const* argument);
// Private function
int CAN_Init(void);
int SAE_Init(void);
int KWP_Init(void);
void UART_RxAgain(UART_HandleTypeDef *huart);
void EnterSleepMode(void);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
GYROSENSOR DataStruct;
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();
	OB->RDP |= OB_RDP_LEVEL_1;
  /* Configure the system clock */
  SystemClock_Config();

	
    /* Initialize all configured peripherals */
  MX_GPIO_Init();
//  MX_CAN_Init();
  //MX_TIM3_Init();
  //MX_TIM4_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();

  /* USER CODE BEGIN 2 */
  #if(IGNORE_SEA_TASK == 0)
	j1850_brutal_init();
  #endif
	DebugPutString(ahg_system_string);
	DebugPutString(my_date);
	DebugPutString("\r\n");
	DebugPutString(my_time);
	DebugPutString("\r\n");
	//DebugPutString("ENABLE_LTE_PWR\r\n");
	//DebugPutString("Hello D \r\n");
	BlePutString("{Hello BLE }\r\n");
	HAL_GPIO_WritePin(GPIOA,GPIO_PIN_5,GPIO_PIN_SET);
	// Init global ODB2Message
	g_ODB2Msg.u8Status = ODB2_STATUS_START;
	uint32_t tmp_reg = READ_BIT(FLASH->OBR, FLASH_OBR_RDPRT);
	if(tmp_reg==FLASH_OBR_RDPRT_1){
		DebugPutString("Read Protection level 1 enable\r\n");
	} else if(tmp_reg==FLASH_OBR_RDPRT_2){
		DebugPutString("Read Protection level 2 enable\r\n");
	} else {
		DebugPutString("Read protection level 0\r\n");
	}
	
	TRIP_Init();
	  
//	DebugPutString("Init EFLASH\r\n");
//	EF_Init();
	
//	while(1)
//	{
//		TRIP_saveData();
//		TRIP_readData();
//		HAL_Delay(100);
//	}
	
		//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
  /* USER CODE END 2 */
		
#if 1

  /* USER CODE BEGIN RTOS_SEMAPHORES */
    /* add semaphores, ... */
    osSemaphoreDef(odb2SemSAE);
    g_ODB2_SAE_Semid = osSemaphoreCreate(osSemaphore(odb2SemSAE), 1);
    osSemaphoreDef(odb2SemKWP);
    g_ODB2_KWP_Semid = osSemaphoreCreate(osSemaphore(odb2SemKWP), 1);
    osSemaphoreDef(odb2SemCAN);
    g_ODB2_CAN_Semid = osSemaphoreCreate(osSemaphore(odb2SemCAN), 1);
    osSemaphoreDef(bleSem);
    g_BLESemid = osSemaphoreCreate(osSemaphore(bleSem), 1);
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 256);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
    /* add threads, ... */
    /* Define & create BleTask */
    osThreadDef(bleTask, BleTask, osPriorityNormal, 0, 512);
    bleTaskHandle = osThreadCreate(osThread(bleTask), NULL);
		/* Define & create BleTask */
    // osThreadDef(saeTask, SaeTask, osPriorityNormal, 0, 512);
    // saeTaskHandle = osThreadCreate(osThread(saeTask), NULL);
    // /* Define & create BleTask */
    // osThreadDef(canTask, CanTask, osPriorityNormal, 0, 512);
    // canTaskHandle = osThreadCreate(osThread(canTask), NULL);
    // /* Define & create BleTask */
    // osThreadDef(kwpTask, KwpTask, osPriorityNormal, 0, 256);
    // kwpTaskHandle = osThreadCreate(osThread(kwpTask), NULL);
		
    osThreadDef(HdlGyroSensor, GyroSensorTask, osPriorityNormal, 0, 256);
    gyroSensorTaskHandle = osThreadCreate(osThread(HdlGyroSensor), NULL);
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
    /* add queues, ... */
    xRxDebugQueue = xQueueCreate( 32, sizeof( char ) );
    if( xRxDebugQueue == 0 )
    {
        /* Failed to create the queue. */
        DebugPutString("Init xRxDebugQueue Failed\r\n");
    }
    else
    {
        DebugPutString("Init xRxDebugQueue OK\r\n");
    }
    /* CREATE SOME QUEUE for BLE UART */
    xRxBleQueue = xQueueCreate( 128, sizeof( char ) );
    if( xRxBleQueue == 0 )
    {
        /* Failed to create the queue. */
        DebugPutString("Init xRxBleQueue Failed\r\n");
    }
    else
    {
        DebugPutString("Init xRxBleQueue OK\r\n");
    }

    osSemaphoreWait(g_ODB2_CAN_Semid, osWaitForever);
    osSemaphoreWait(g_ODB2_SAE_Semid, osWaitForever);
    osSemaphoreWait(g_ODB2_KWP_Semid, osWaitForever);
    osSemaphoreWait(g_BLESemid, osWaitForever);

  /* USER CODE END RTOS_QUEUES */
 
	//EnterSleepMode();
  /* Start scheduler */
  osKernelStart();
#endif
		
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
    while (1)
    {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

    }
  /* USER CODE END 3 */

}


void ODB2_After_Init(int iProtocol)
{
	
}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_USART3;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* CAN init function */
static void MX_CAN_Init(void)
{

  hcan.Instance = CAN;
  hcan.Init.Prescaler = CAN_500_PRESCALE;
  hcan.Init.Mode = CAN_MODE_NORMAL;
  hcan.Init.SJW = CAN_SJW_3TQ;
  hcan.Init.BS1 = CAN_BS1_9TQ;
  hcan.Init.BS2 = CAN_BS2_6TQ;
  hcan.Init.TTCM = DISABLE;
  hcan.Init.ABOM = DISABLE;
  hcan.Init.AWUM = DISABLE;
  hcan.Init.NART = ENABLE;
  hcan.Init.RFLM = DISABLE;
  hcan.Init.TXFP = DISABLE;
  if (HAL_CAN_Init(&hcan) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM3 init function */
static void MX_TIM3_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_IC_InitTypeDef sConfigIC;

  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 63;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 65535;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_IC_Init(&htim3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
  if (HAL_TIM_IC_ConfigChannel(&htim3, &sConfigIC, TIM_CHANNEL_4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM4 init function */
static void MX_TIM4_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 63;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 65535;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_OC_Init(&htim4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim4);

}
/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = UART_BLE_BAUDRATE;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = UART_DEBUG_BAUDRATE;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART3 init function */
static void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  huart3.Init.BaudRate = UART_ISO9141_BAUDRATE;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_RXOVERRUNDISABLE_INIT|UART_ADVFEATURE_DMADISABLEONERROR_INIT;
  huart3.AdvancedInit.OverrunDisable = UART_ADVFEATURE_OVERRUN_DISABLE;
  huart3.AdvancedInit.DMADisableonRxError = UART_ADVFEATURE_DMA_DISABLEONRXERROR;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOF_CLK_ENABLE();
	
  /*Configure GPIO pin Output Level */
//  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
	
	PERIPHERAL_POWER_ON();
	
  /*Configure GPIO pin : PB12 */
  GPIO_InitStruct.Pin = POWER_SAVE_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(POWER_SAVE_PORT, &GPIO_InitStruct);
	
	// Init GPIO for enable disable VPW, PWM
  /*Configure GPIO pin Output Level */
//  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);

//	/*Configure GPIO pin : PB12 */
//  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5;
//  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
//  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
}

/* USER CODE BEGIN 4 */
/**
 * @brief HAL_CAN_RxCpltCallback
 * @param CanHandle
 */
void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef* CanHandle)
{
    //check_response=0;
    HAL_StatusTypeDef status;
    //osSemaphoreRelease(canSemid);
    status = HAL_CAN_Receive_IT(CanHandle, CAN_FIFO0);
    if(status != HAL_OK){
        //Reception Error
        sprintf(tmpStr, "\r\n HAL_CAN_Receive_IT error 0x%02X ", (int)status);
        DebugPutString(tmpStr);
        //else BlePutString(pu8Temp);
    }
}

/**
 * @brief HAL_UART_RxCpltCallback UART Callback which is called at the
 *        end of UART IRQ Handler
 * @param huart uart instance
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    // ble interrupt
    BaseType_t xDebugHigherPriorityTaskWoken;
    BaseType_t xBleHigherPriorityTaskWoken;
    // Debug Interrupt using
    if (huart->Instance == DEBUG_UART_INSTANCE)
    {
        if (xRxDebugQueue != 0)
        {
            xDebugHigherPriorityTaskWoken = pdFALSE;
            /* Post the byte. */
            if (xQueueSendFromISR( xRxDebugQueue, debugRx, &xDebugHigherPriorityTaskWoken ))
            {
                //DebugPutChar('+'); // for debug ^^
            }
        }
        HAL_UART_Receive_IT(&UART_DEBUG, (uint8_t*)&debugRx[0], 1);
    }
    // BLE Interrupt using
    if (huart->Instance == BLE_UART_INSTANCE)
    {
        if (xRxBleQueue != 0)
        {
            xBleHigherPriorityTaskWoken = pdFALSE;
            /* Post the byte. */
            if (xQueueSendFromISR( xRxBleQueue, bleRx, &xBleHigherPriorityTaskWoken ))
            {
                //DebugPutChar('+'); // for debug ^^
                //DebugPutChar(bleRx[0]);
            }
        }
        //HAL_UART_Receive_IT(&UART_BLE, (uint8_t*)&bleRx[0], 1);
				UART_RxAgain(&UART_BLE);
				/* USER CODE BEGIN USART2_IRQn 1 */
  /* USER CODE END USART2_IRQn 1 */
    }

}
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart){
	if(huart->Instance == BLE_UART_INSTANCE){
		//HAL_UART_Receive_IT(&UART_BLE, (uint8_t*)&bleRx[0], 1);
		UART_RxAgain(&UART_BLE);
	}
}
/**
 * @brief DebugTask
 * @param argument
 */
void DebugTask(void const * argument)
{
    DebugPutString("\r\nDebugTask Start");
    HAL_UART_Receive_IT(&UART_DEBUG, (uint8_t*)&debugRx[0], 1);
    for (;;)
    {
        DebugProcessTask();
    }
}
/**
 * @brief BleTask
 * @param argument
 */
void BleTask(void const* argument)
{
    HAL_UART_Receive_IT(&UART_BLE, (uint8_t*)&bleRx[0], 1);
	if(g_ODB2Protocol == ODB2_PROTOCOL_START)
    for (;;)
    {
        //        osDelay(100);
        //        DebugPutChar('-');
        BleProcessTask();
    }
}

/**
 * @brief CanTask
 * @param argument
 */
void CanTask(void const* argument)
{
    //HAL_UART_Receive_IT(&UART_BLE, (uint8_t*)&bleRx[0], 1);
    DebugPutString("\r\nCanTask Start");
    int ret;
	if(g_ODB2Protocol == ODB2_PROTOCOL_START){
		while (1) {
//			WRITE_REG(IWDG->KR,IWDG_KEY_RELOAD);
        ret = CAN_Init();
        if (ret == ODB2_PROTOCOL_START)	{
            DebugPutString("\r\nCanTask scan failed, rescan ...");
            //osThreadTerminate(NULL);
        } else if ( ret== ODB2_BURNIN_TEST){
						DebugPutString("\r\nInto Burnintest mode");
						g_ODB2Protocol = ret;
						osThreadSuspend(saeTaskHandle);
						osThreadSuspend(kwpTaskHandle);
						break;
				}else {
            g_ODB2Protocol = ret;
            g_ODB2_Semid = g_ODB2_CAN_Semid;
						osThreadSuspend(saeTaskHandle);
						osThreadSuspend(kwpTaskHandle);
            break;
        }
    }
	} else if(g_ODB2Protocol == ODB2_PROTOCOL_CAN11_250 || 
						g_ODB2Protocol == ODB2_PROTOCOL_CAN11_500 ||
						g_ODB2Protocol == ODB2_PROTOCOL_CAN29_250 ||
						g_ODB2Protocol == ODB2_PROTOCOL_CAN29_500 ||
						g_ODB2Protocol == ODB2_PROTOCOL_TOYOTA_CAN){
		while(1){
			ret = CAN_Init();
			if(ret==g_ODB2Protocol){
				g_ODB2_Semid = g_ODB2_CAN_Semid;
				break;
			}
		}
	}
	g_ObdInitingCustom = false;
	
//	ODB2_CAN_Transmit(0x7DF,0x03,0x00,CAN_STANDARD);
//	osDelay(5000);
//	
//	ODB2_CAN_Transmit(0x7DF,0x04,0x00,CAN_STANDARD);
//		osDelay(5000);
	
	for (;;) {
			osSemaphoreWait(g_ODB2_CAN_Semid, osWaitForever);
			DebugPutChar('@');
			if(g_ODB2Protocol == ODB2_PROTOCOL_TOYOTA_CAN) 
				ODB2_TOYOTA_CAN_ProcessBleRequest();
			else 
				ODB2_CAN_ProcessBleRequest();
			osSemaphoreRelease(g_BLESemid);
//			BleReplySuccessRequest();
	}
}

/**
 * @brief SaeTask
 * @param argument
 */
void SaeTask(void const* argument)
{
#if(IGNORE_SEA_TASK == 0)
    DebugPutString("\r\nSaeTask Start");
#if 1
    int ret;
    while (1) {
        ret = SAE_Init();
        if (ret == ODB2_PROTOCOL_START)
        {
            DebugPutString("\r\nSaeTask Scanned Failed, rescan ...");
            //osThreadTerminate(NULL);
        } else {
            g_ODB2Protocol = ret;
            g_ODB2_Semid = g_ODB2_SAE_Semid;
            osThreadTerminate(canTaskHandle);
            osThreadTerminate(kwpTaskHandle);
//						ENABLE_VPW();
//						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET);
						ISO_UART_DISABLE();
						HAL_GPIO_DeInit(ISO_UART_GPIO, ISO_UART_PIN_RX);
						HAL_GPIO_DeInit(ISO_UART_GPIO, ISO_UART_PIN_TX);
            break;
        }
    }
#endif
//		osThreadDef(bleTask, BleTask, osPriorityNormal, 0, 256);
//    bleTaskHandle = osThreadCreate(osThread(bleTask), NULL);
#endif
    for (;;)
    {
        osSemaphoreWait(g_ODB2_SAE_Semid, osWaitForever);
        DebugPutChar('@');
        #if(IGNORE_SEA_TASK == 0)
        ODB2_j1850_ProcessBleRequest();
        #endif
        osSemaphoreRelease(g_BLESemid);
//				BleReplySuccessRequest();
    }

}

/**
 * @brief KwpTask
 * @param argument
 */
void KwpTask(void const* argument)
{
//	while(1) osDelay(1000);	
	DebugPutString("\r\nKwpTask Start");
	int ret;
	if(g_ODB2Protocol == ODB2_PROTOCOL_START){
		while (1) {
        ret = KWP_Init();
        if (ret == ODB2_PROTOCOL_START)
        {
            DebugPutString("\r\nKwpTask scan failed, rescan ...");
          } else {
            g_ODB2Protocol = ret;
            g_ODB2_Semid = g_ODB2_KWP_Semid;
						osThreadSuspend(saeTaskHandle);
						osThreadSuspend(canTaskHandle);
            break;
        }
    }
	} else {
		while (1){
			ret = KWP_Init();
			if(ret==g_ODB2Protocol){
				g_ODB2_Semid = g_ODB2_KWP_Semid;
				break;
			}
		}
	}
	g_ObdInitingCustom = false;
	
	if(g_ODB2Protocol == ODB2_PROTOCOL_TOYOTA_KLINE){
		if(!checkToyotaMode01()){
			DebugPutString("\r\nODB2_PROTOCOL_TOYOTA_KLINE something wrong in checkToyotaMode01");
		}
	}
	
	for (;;) {
			 if (osSemaphoreWait(g_ODB2_KWP_Semid, 2000) != osOK) {
				ODB2_KWP_KeepAlive();
				if(!bKWP_Alive){
					KWP_Reinit();
				}
			} else {
				if(!bKWP_Alive){
					KWP_Reinit();
				}
				if(bKWP_Alive){
					if(g_ODB2Protocol != ODB2_PROTOCOL_TOYOTA_KLINE){
					ODB2_KWP_ProcessBleRequest();
					} else {
						ODB2_Toyota_ProcessBleRequest();
					}
				}          
				osSemaphoreRelease(g_BLESemid);
//				BleReplySuccessRequest();
			}
			DebugPutChar('+');
	}
} 
int CAN_Init(void)
{
		ODB2_CAN_InitFilters(true);
    int ret = ODB2_PROTOCOL_START;
		if(g_ODB2Protocol == ODB2_PROTOCOL_START) {
			while(1) {
				osDelay(1000);
				DebugPutString("\r\nCAN_HIGHSPEED init");
				ret = ODB2_CAN_Init_Safe(CAN_HIGHSPEED);
				if(ret == CAN_STANDARD){
					DebugPutString("\r\nInit CAN Safe 11-500 OK");
					return ODB2_PROTOCOL_CAN11_500;
				} else if (ret == CAN_EXTENDED) {
					DebugPutString("\r\nInit CAN Safe 29-500 OK");
					return ODB2_PROTOCOL_CAN29_500;
				}	else if (ret == CAN_REQUEST_BURNIN) {
					return ODB2_BURNIN_TEST;
				} else if (ret == CAN_TOYOTA){
					DebugPutString("\r\nInit CAN Toyota OK");
					return ODB2_PROTOCOL_TOYOTA_CAN;
				}
				ODB2_CAN_DeInit();
				#if 0
				osDelay(1000);
				DebugPutString("\r\nCAN_LOWSPEED init");
				ret = ODB2_CAN_Init_Safe(CAN_LOWSPEED);
				if(ret == CAN_STANDARD){
					DebugPutString("\r\nInit CAN Safe 11-250 OK");
					ODB2_CAN_InitFilter11bit(true,CAN_FIFO0);
					return ODB2_PROTOCOL_CAN11_250;
				} else if (ret == CAN_EXTENDED) {
					DebugPutString("\r\nInit CAN Safe 29-250 OK");
					ODB2_CAN_InitFilter29bit(true,CAN_FIFO0);
					return ODB2_PROTOCOL_CAN29_250;
				}	else if (ret == CAN_REQUEST_BURNIN) {
					return ODB2_BURNIN_TEST;
				}
				ODB2_CAN_DeInit();
				#endif
			}
		}
		// Test CAN Highspeed 500kbaud
		else if( g_ODB2Protocol == ODB2_PROTOCOL_CAN11_500 || g_ODB2Protocol == ODB2_PROTOCOL_CAN29_500) {
			osDelay(500);
			ret = ODB2_CAN_Init(CAN_HIGHSPEED);
			if (ret == CAN_STANDARD) {
					DebugPutString("\r\nCAN11_500 inited OK");
					return ODB2_PROTOCOL_CAN11_500;
			} else if (ret == CAN_EXTENDED) {
					DebugPutString("\r\nCAN29_500 inited OK");
					return ODB2_PROTOCOL_CAN29_500;
			} else if (ret == CAN_REQUEST_BURNIN) {
					return ODB2_BURNIN_TEST;
			}
			ODB2_CAN_DeInit();
		}
    // Test CAN lowspeed 250kbaud
		else if( g_ODB2Protocol == ODB2_PROTOCOL_CAN11_250 || g_ODB2Protocol == ODB2_PROTOCOL_CAN29_250) {
			osDelay(500);
			ret = ODB2_CAN_Init(CAN_LOWSPEED);
			if (ret == CAN_STANDARD) {
					DebugPutString("\r\nCAN11_250 inited OK");
					return ODB2_PROTOCOL_CAN11_250;
			} else if (ret == CAN_EXTENDED) {
					DebugPutString("\r\nCAN29_250 inited OK");
					return ODB2_PROTOCOL_CAN29_250;
			} else if (ret == CAN_REQUEST_BURNIN) {
					return ODB2_BURNIN_TEST;
			}
			ODB2_CAN_DeInit();
		}
		// Test CAN Toyota
		else if(g_ODB2Protocol == ODB2_PROTOCOL_TOYOTA_CAN) {
			osDelay(500);
			ret = ODB2_TOYOTA_CAN_Init();
			if(ret==CAN_TOYOTA) {
				DebugPutString("\r\nCAN Toyota inited OK");
				return ODB2_PROTOCOL_TOYOTA_CAN;
			} else if (ret==CAN_REQUEST_BURNIN){
				return ODB2_BURNIN_TEST;
			}
			ODB2_CAN_DeInit();
		}
    return ODB2_PROTOCOL_START;
}
int SAE_Init(void)
{
    int ret;
    ret = j1850_init();
    if (ret == J1850_PROTOCOL_VPW) {
        DebugPutString("\r\nODB2_PROTOCOL_VPW inited OK");
        return ODB2_PROTOCOL_VPW;
    }
    else if (ret == J1850_PROTOCOL_PWM) {
        DebugPutString("\r\nODB2_PROTOCOL_PWM inited OK");
        return ODB2_PROTOCOL_PWM;
    } else {
        DebugPutString("\r\nJ1850 Inited Failed\r\n");
        return ODB2_PROTOCOL_START;
    }
}
int KWP_Init(void)
{
    int ret;
    ret = ODB2_ISO_KWP_Init();
    if (ret == KWP_2000_FAST) {
        DebugPutString("\r\nODB2_KWP2000_FAST inited OK!");
        return ODB2_PROTOCOL_KWP2000_FAST;
    } else if (ret == KWP_2000_SLOW) {
        DebugPutString("\r\nODB2_KWP2000_SLOW inited OK!");
        return ODB2_PROTOCOL_KWP2000_SLOW;
    } else if (ret == ISO9141) {
        DebugPutString("\r\nODB2_ISO9141 inited OK!");
        return ODB2_PROTOCOL_ISO9141;
    } else  if (ret == KWP_FAST_TOYOTA) {
				DebugPutString("\r\nODB2_PROTOCOL_TOYOTA_KLINE inited OK!");
				return ODB2_PROTOCOL_TOYOTA_KLINE;
		}  else {
        return ODB2_PROTOCOL_START;
    }
}
void UART_RxAgain(UART_HandleTypeDef *huart) {
	/* Check that a Rx process is not already ongoing */
  if(huart->RxState == HAL_UART_STATE_READY)
  {

		huart->pRxBuffPtr = (uint8_t*)&bleRx[0];
		huart->RxXferSize = 1;
		huart->RxXferCount = 1;

    /* Computation of UART mask to apply to RDR register */
    UART_MASK_COMPUTATION(huart);

    huart->ErrorCode = HAL_UART_ERROR_NONE;
    huart->RxState = HAL_UART_STATE_BUSY_RX;

    /* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
    SET_BIT(huart->Instance->CR3, USART_CR3_EIE);

    /* Enable the UART Parity Error and Data Register not empty Interrupts */
    SET_BIT(huart->Instance->CR1, USART_CR1_PEIE | USART_CR1_RXNEIE);
    
  }
	
}
void EnterSleepMode(void){
//	g_sleep = true;
	PERIPHERAL_POWER_OFF();
	
	taskENTER_CRITICAL();
	HAL_SuspendTick();
	/* Enable Power Control clock */
	__HAL_RCC_PWR_CLK_ENABLE();

	/* Enter Sleep Mode , wake up is done once User push-button is pressed */
	HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);

	/* Resume Tick interrupt if disabled prior to sleep mode entry*/
	
	HAL_ResumeTick();
	taskEXIT_CRITICAL();
}
/* USER CODE END 4 */

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{
	
     /* USER CODE BEGIN 5 */
    //osTimerStart(Timer1Handle, 1000);
    //HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET);
    uint16_t u16Count = 0;
    //    ODB2_PROTOCOL ret = ODB2_Init();
    //    sprintf(tmpStr, "\r\n\r\n=========ODB2_Init ret = %d===========", ret);
    DebugPutString(tmpStr);
    /* Infinite loop */
    for(;;)
    {
			osDelay(1000);      
			if(b_TripStart){
//					if((f_TripDistance - f_TripDistanceSended) >= 50 || (f_TripStopTime - f_TripStopTimeSended) >= 30)
					BleHandleTripUpdate(NULL,0);
				if(g_ODB2Protocol != ODB2_PROTOCOL_START) {
					u16Count++;
					if( u16Count >= 30 || (f_TripDistance - f_TripDistanceSaved) >= 1000 ) {
						
						TRIP_SaveData();
						f_TripDistanceSaved = f_TripDistance;
						u16Count = 0;
					}
				}
			} else {
				if(g_ODB2Protocol != ODB2_PROTOCOL_START){
					sprintf(tmpStr,"010D\r\n41 0D %02x\r\n>\r\n",u8Speed);
					DebugPutString(tmpStr);
					BlePutString(tmpStr);
					BlePutChar('\0');
				}
			}
    }
    /* USER CODE END 5 */
}

/* Timer1_Callback function */
void Timer1_Callback(void const * argument)
{
    /* USER CODE BEGIN Timer1_Callback */
    DebugPutString("\r\n Timer1 Done");
    Utils_Timer1_Callback(argument);
    /* USER CODE END Timer1_Callback */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM2 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    /* USER CODE BEGIN Callback 0 */

    /* USER CODE END Callback 0 */
    if (htim->Instance == TIM2) {
        HAL_IncTick();
    }
    /* USER CODE BEGIN Callback 1 */
    if (htim->Instance == TIM4) {
        j1850_pwm_err_timer_handler();
    }
    /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif
void Debug(char *str)
{
#if 1
    while(*str !='\0')
    {
        HAL_UART_Transmit(&huart2, (uint8_t *)str, 1, 0xFFFF);
        str++;
    }
#endif
}

typedef struct{
	int32_t x;
	int32_t y;
	int32_t z;
} dataMax_t;
static dataMax_t tempMax, tempMin, Min, Max, Balance;
void findMinMax(uint8_t *count, dataMax_t dataIn)
{
	uint16_t len;
	if(tempMax.x < dataIn.x) {Max.x = dataIn.x - Balance.x; tempMax.x = dataIn.x; }
	if(tempMax.y < dataIn.y) {Max.y = dataIn.y - Balance.y; tempMax.y = dataIn.y; }
	if(tempMax.z < dataIn.z) {Max.z = dataIn.z - Balance.z; tempMax.z = dataIn.z; }
	
	if(tempMin.x > dataIn.x) {Min.x = dataIn.x - Balance.x; tempMin.x = dataIn.x; }
	if(tempMin.y > dataIn.y) {Min.y = dataIn.y - Balance.y; tempMin.y = dataIn.y; }
	if(tempMin.z > dataIn.z) {Min.z = dataIn.z - Balance.z; tempMin.z = dataIn.z; }
	
	if(*count == 49)
	{
		*count = 0;
		len = sprintf(tmpStr,"%d,%d,%d,%d,%d,%d\r\n", Min.x, Max.x, Min.y, Max.y, Min.z, Max.z);
		Min.x = Min.y = Min.z = Max.x = Max.y = Max.z = 0;
		Balance.x = tempMax.x = tempMin.x = dataIn.x;
		Balance.y = tempMax.y = tempMin.y = dataIn.y;
		Balance.z = tempMax.z = tempMin.z = dataIn.z;

		//      Debug(tmpStr);
		Ble_FixPutString(tmpStr, len);
	}
}

void GyroSensorTask(void const* argument)
{
	dataMax_t tempDataGyroMax;
	uint8_t count = 0;
  Debug("gyroTask start\n");
  GYROSENSOR_Result result;
  while(1)
  {
    Debug("Init sensor\n");
    osDelay(1000);
    result = GYROSENSOR_Init(&DataStruct, GYROSENSOR_Device_0, GYROSENSOR_Accelerometer_2G, GYROSENSOR_Gyroscope_250s);
    if(result == GYROSENSOR_Result_Ok) break;
  }

  Debug("Init sensor OK\n");
  g_InfoAutoSend = true;
	
	tempMax.x = DataStruct.firstAccX;
	tempMax.y = DataStruct.firstAccY;
	tempMax.z = DataStruct.firstAccZ;

	tempMin.x = DataStruct.firstAccX;
	tempMin.y = DataStruct.firstAccY;
	tempMin.z = DataStruct.firstAccZ;

  for(;;)
  {
    osDelay(10);
    if(g_InfoAutoSend || b_TripStart)
    {
      GYROSENSOR_UpdateRotation(&DataStruct);
//      sprintf(tmpStr,"010E\r\n%d\r\n%d\r\n%d\r\n>\r\n", DataStruct.newAccX, DataStruct.newAccY, DataStruct.newAccZ);
//			sprintf(tmpStr,"%d,%d,%d\r\n", DataStruct.newAccX, DataStruct.newAccY, DataStruct.newAccZ);
//			tempDataGyroMax.x = DataStruct.newAccX;
//			tempDataGyroMax.y = DataStruct.newAccY;
//			tempDataGyroMax.z = DataStruct.newAccZ;
//			findMinMax(&count, tempDataGyroMax);
//			count++;
			sprintf(tmpStr,"%d,%d,%d\r\n", DataStruct.newAccX, DataStruct.newAccY, DataStruct.newAccZ);
//      Debug(tmpStr);
      BlePutString(tmpStr);
//      BlePutChar('\0');
    }
  }
}

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
