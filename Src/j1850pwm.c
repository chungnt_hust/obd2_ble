#include "j1850pwm.h"
#include "j1850.h"
#include "debug.h"
#include "brutal_timing.h"

PWM_MODE pwm_mode;


int pwm_bus_status = PWM_BUS_PASSIVE;
// Tx variables
uint16_t pwm_u16TxPulseCount;
uint16_t pwm_u16TxPulseSent;
// Tx pulse buffer
uint8_t  pwm_u8TxByteBuf[12]={0x61,0x6A,0xF1,0x01,0x00};
uint8_t *pwm_pu8TxPulseBuf;
// Rx buffer
uint16_t pwm_u16RxPulseCount;
uint16_t pwm_u16RxByteCount;
uint16_t pwm_u16RxIFRPulseCount;
static uint8_t* pwm_pu8RxPulseBuf;
static uint8_t* pwm_pu8RxBitBuf;
static uint8_t* pwm_pu8RxByteBuf;
static uint8_t pwm_ifr_buf[32];
static uint8_t pwm_ifr_buf_read[32];
// error timer check
static bool pwm_err_timer_on = false;
// tmp str
static char tmpStr[128];
uint16_t u16Temp;

//uint8_t  pwm_u8TxByteBuf[12]={0x61,0x6A,0xF1,0x01,0x00};
//uint8_t pwm_pu8TxPulse__Buf[50];
//uint16_t pwm_u16PulsleCount;
uint8_t ackTime;
uint8_t ackPulse[8];
//uint8_t receivePulse[2000];
//uint16_t receivePulseCount;
uint8_t pwm_pu8ReceiveAckPulse[16] = {8,16,8,16,8,16,8,16,16,8,16,8,16,8,8,16};
//uint8_t pwm_pu8ReceiveByte[56];
//uint16_t pwm_u16ReceiveByteCount;


void inline pwm_set_output_compare_pulse(uint16_t u16Pulse) {

    //		sprintf(tmpStr, "\r\nu16Pulse = %d", u16Pulse);
    //		DebugPutString(tmpStr);
    __HAL_TIM_SET_COUNTER(&PWM_Tx_TIMER,0); // Cái này fix lỗi truyền một lúc thì sai
    __HAL_TIM_SET_COMPARE(&PWM_Tx_TIMER,PWM_TX_TIM_OC_CHANNEL,u16Pulse);
    // u16Temp = __HAL_TIM_GET_COMPARE(&PWM_Tx_TIMER,PWM_TX_TIM_OC_CHANNEL);
    //		sprintf(tmpStr, "\r\nu16Temp = %d", u16Temp);
    //		DebugPutString(tmpStr);
    //__HAL_TIM_SET_COUNTER(&PWM_Tx_TIMER,0);
}
void inline pwm_init_output_compare() {
    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;
    TIM_OC_InitTypeDef sConfigOC;

    PWM_Tx_TIMER.Instance = TIM4;
    PWM_Tx_TIMER.Init.Prescaler = 63;
    PWM_Tx_TIMER.Init.CounterMode = TIM_COUNTERMODE_UP;
    PWM_Tx_TIMER.Init.Period = 65535;
    PWM_Tx_TIMER.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    if (HAL_TIM_Base_Init(&PWM_Tx_TIMER) != HAL_OK)
    {
        Error_Handler();
    }

    //    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    //    if (HAL_TIM_ConfigClockSource(&PWM_Tx_TIMER, &sClockSourceConfig) != HAL_OK)
    //    {
    //        Error_Handler();
    //    }

    //    if (HAL_TIM_OC_Init(&PWM_Tx_TIMER) != HAL_OK)
    //    {
    //        Error_Handler();
    //    }

    //    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    //    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    //    if (HAL_TIMEx_MasterConfigSynchronization(&PWM_Tx_TIMER, &sMasterConfig) != HAL_OK)
    //    {
    //        Error_Handler();
    //    }

    //    sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
    //    sConfigOC.Pulse = 0;
    //    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    //    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    //    if (HAL_TIM_OC_ConfigChannel(&PWM_Tx_TIMER, &sConfigOC, PWM_TX_TIM_OC_CHANNEL) != HAL_OK)
    //    {
    //        Error_Handler();
    //    }

    //    HAL_TIM_MspPostInit(&PWM_Tx_TIMER);
}

void inline pwm_start_output_compare(void) {
    //pwm_init_output_compare();
    __HAL_TIM_CLEAR_FLAG(&PWM_Tx_TIMER, TIM_SR_CC2OF);
    //__HAL_TIM_SET_COUNTER(&PWM_Tx_TIMER, 0);
    HAL_TIM_OC_Start_IT(&PWM_Tx_TIMER, PWM_TX_TIM_OC_CHANNEL);
}
void inline pwm_stop_output_compare(void) {
    HAL_TIM_OC_Stop_IT(&PWM_Tx_TIMER, PWM_TX_TIM_OC_CHANNEL);
    //HAL_TIM_OC_Stop(&PWM_Tx_TIMER, PWM_TX_TIM_OC_CHANNEL);
}

void inline pwm_stop_input_capture(void)
{
    //u16PWMRxBitCount = 0;
    HAL_TIM_IC_Stop_IT(&PWM_Rx_TIMER, PWM_RX_TIM_IC_RISING_CHANEL);
    HAL_TIM_IC_Stop_IT(&PWM_Rx_TIMER, PWM_RX_TIM_IC_FALLING_CHANEL);
}
void pwm_start_input_capture(void)
{
    //pwm_u16RxPulseCount = 0;
    //pwm_u16RxIFRPulseCount = 0;
    HAL_TIM_IC_Start_IT(&PWM_Rx_TIMER, PWM_RX_TIM_IC_RISING_CHANEL);
    HAL_TIM_IC_Start_IT(&PWM_Rx_TIMER, PWM_RX_TIM_IC_FALLING_CHANEL);
}

void j1850_pwm_setup(void) {
    // tx pulse buffer
    pwm_pu8TxPulseBuf = (uint8_t*)u16PulseToSAE;
    // rx buffer
    pwm_pu8RxPulseBuf = (uint8_t*)j1850_RX_Pulse_Buf;
//    pwm_pu8RxBitBuf = (uint8_t*) j1850_Rx_Bits_Buf;
    pwm_pu8RxByteBuf = (uint8_t*) j1850_RX_Byte_Buf;
    static uint8_t u8IFRByte = 0xF1;
    // precalcualte ifr buf
    pwm_calculate_pulse(&u8IFRByte, 1, pwm_ifr_buf);
}



void pwm_start_err_timer(uint16_t u16Timeout)
{
    //    PWM_ERR_TIMER.Instance = TIM4;
    //    PWM_ERR_TIMER.Init.Prescaler = 63;
    //    PWM_ERR_TIMER.Init.CounterMode = TIM_COUNTERMODE_UP;
    //    PWM_ERR_TIMER.Init.Period = u16Timeout;
    //    PWM_ERR_TIMER.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    //    __HAL_TIM_CLEAR_FLAG(&PWM_Tx_TIMER, TIM_SR_UIF);
    //    if (HAL_TIM_Base_Init(&PWM_Tx_TIMER) != HAL_OK)
    //    {
    //        Error_Handler();
    //    }
    __HAL_TIM_CLEAR_FLAG(&PWM_ERR_TIMER, TIM_SR_UIF);
    pwm_err_timer_on = true;
    __HAL_TIM_SET_COUNTER(&PWM_ERR_TIMER,65535 - u16Timeout);
    HAL_TIM_Base_Start_IT(&PWM_ERR_TIMER);
}

void pwm_stop_err_timer() {
    //pwm_err_timer_on = false;
    HAL_TIM_Base_Stop(&PWM_ERR_TIMER);
    HAL_TIM_Base_Stop_IT(&PWM_ERR_TIMER);
}
inline void j1850_pwm_err_timer_handler(void)
{
    pwm_stop_err_timer();
    //DebugPutChar('Z');
    //		sprintf(tmpStr, "\r\nu16Counter=%d\r\n", u16Counter);
    //		DebugPutString(tmpStr);
    if (pwm_mode == PWM_MODE_RECEIVING_DATA) {
        // stop timer, stop input capture, mode = send ifr, start send ifr
        //pwm_stop_err_timer();
        pwm_stop_input_capture();
        pwm_pu8RxPulseBuf[pwm_u16RxPulseCount++] = 255;
        pwm_mode = PWM_MODE_SENDING_IFR;
        pwm_start_sending_ifr();
        //DebugPutChar('T');
        //            sprintf(tmpStr, "\r\nu16Counter=%d\r\n", u16Counter);
        //            DebugPutString(tmpStr);
    }
    pwm_err_timer_on = false;
}
inline void pwm_reset_input_capture_counter(void) {
    //__HAL_TIM_SetCounter(&PWM_Rx_TIMER,0);
}
uint16_t pwm_get_input_capture_pulse(bool rising) {
    if (rising) {
			return 0;
        //return __HAL_TIM_GetCompare(&PWM_Rx_TIMER, PWM_RX_TIM_IC_RISING_CHANEL);
    } else {
			return 0;
        //return __HAL_TIM_GetCompare(&PWM_Rx_TIMER, PWM_RX_TIM_IC_FALLING_CHANEL);
    }
}
void inline j1850_pwm_output_compare_handler(void) {
    if (pwm_u16TxPulseSent == (pwm_u16TxPulseCount - 1)) {
        pwm_stop_output_compare();

        if (pwm_mode == PWM_MODE_SENDING_DATA) {
            pwm_mode = PWM_MODE_RECEIVING;
            j1850_pwm_start_receive();
            //DebugPutChar('&');
        }
        if (pwm_mode == PWM_MODE_SENDING_IFR) {
            pwm_mode = PWM_MODE_RECEIVING;
            j1850_pwm_start_receive();
            //DebugPutChar('*');
        }
        //        if (pwm_bus_status == PWM_BUS_ACTIVE) {
        //            //DebugPutChar('-');
        //            //pwm_bus_status = PWM_BUS_PASSIVE;
        //        }
        //        else  {
        //            //DebugPutChar('+');
        //            //pwm_bus_status = PWM_BUS_ACTIVE;
        //        }

    } else {
        // set new pulse to toggle output
        pwm_set_output_compare_pulse(pwm_pu8TxPulseBuf[pwm_u16TxPulseSent++]);
        //DebugPutChar('*');
    }
    if (pwm_bus_status == PWM_BUS_ACTIVE) {
        //DebugPutChar('-');
        pwm_bus_status = PWM_BUS_PASSIVE;
    }
    else  {
        //DebugPutChar('+');
        pwm_bus_status = PWM_BUS_ACTIVE;
    }
}
void j1850_pwm_input_capture_falling_handler(void) {
    static uint16_t u16Delta;
    u16Delta = 0;//__HAL_TIM_GetCompare(&PWM_Rx_TIMER, PWM_RX_TIM_IC_FALLING_CHANEL);
    if (u16Delta > 255) u16Delta = 255;
    //DebugPutChar('A');
    //pwm_reset_input_capture_counter();
    // PWM_MODE_RECEIVING
    // PWM_MODE_RECEIVING_DATA
    if (pwm_mode == PWM_MODE_RECEIVING_DATA) {
        //DebugPutChar('E');

        if (pwm_u16RxPulseCount < J1850_RX_MAX_COUNT) {
            pwm_pu8RxPulseBuf[pwm_u16RxPulseCount++] = u16Delta;
        }

    }
    else if (pwm_mode == PWM_MODE_RECEIVING) {
        //DebugPutChar('B');
        //        pwm_mode = PWM_MODE_RECEIVING_ERROR;
    }
    // PWM_MODE_RECEIVING_IFR
    else if (pwm_mode == PWM_MODE_RECEIVING_IFR) {
        //DebugPutChar('C');
        // TODO: read ifr byte ifr[idx++] = u16Delta
        pwm_ifr_buf_read[pwm_u16RxIFRPulseCount++] = u16Delta;
    }
    // PWM_MODE_RECEIVING_SOF
    else if (pwm_mode == PWM_MODE_RECEIVING_SOF) {
        //DebugPutChar('D');
        if (u16Delta >= TP7_RX_MIN - 3 && u16Delta <= TP7_RX_MAX) {
            pwm_mode = PWM_MODE_RECEIVING_SOF;
        } else {
            pwm_mode = PWM_MODE_RECEIVING_ERROR;
        }
    }

    // PWM_MODE_SENDING_DATA
    else if (pwm_mode == PWM_MODE_SENDING_DATA) {
#if 0 // check for sending bit error
        if (pwm_bus_status != PWM_BUS_PASSIVE) {
            pwm_mode = PWM_MODE_SENDING_DATA_ERROR;
            DebugPutChar('F');
            // TODO: reset sending data
        } else {
            DebugPutChar('G');
        }
#endif
    } else if (pwm_mode == PWM_MODE_SENDING_IFR) {
        DebugPutChar('H');
        // TODO:
    }
}

void j1850_pwm_input_capture_rising_handler(void) {

    static uint16_t u16Delta;
    u16Delta =0;// __HAL_TIM_GetCompare(&PWM_Rx_TIMER, PWM_RX_TIM_IC_RISING_CHANEL);
    //DebugPutChar('1');
    //	pwm_get_input_capture_pulse(true);
    //pwm_reset_input_capture_counter();
    if (u16Delta > 255) u16Delta = 255;
    
    // PWM_MODE_RECEIVING
    //	sprintf(tmpStr, "\r\nu16Delta = %d", u16Delta);
    //	DebugPutString(tmpStr);
    if (pwm_mode == PWM_MODE_RECEIVING) {
        //DebugPutChar('2');
        //sprintf(tmpStr, "\r\ndelta=%d\r\n", u16Delta);
        //DebugPutString(tmpStr);
        if (u16Delta >= (TP5_TX_MIN - TP2_TX_NOM)) {
            pwm_mode = PWM_MODE_RECEIVING_SOF;
        } else if (u16Delta <= (TP4_RX_MAX - TP2_TX_NOM) && u16Delta >= 15) {
            pwm_mode = PWM_MODE_RECEIVING_IFR;
        }
    }
    // PWM_MODE_RECEIVING_IFR
    else if (pwm_mode == PWM_MODE_RECEIVING_IFR) {
        pwm_ifr_buf_read[pwm_u16RxIFRPulseCount++] = u16Delta;
        if (u16Delta > TP5_TX_MIN) {
            //DebugPutChar('3');
            pwm_mode = PWM_MODE_RECEIVING_SOF;
            //pwm_u16RxPulseCount = 0;// reset pwm_u16RxPulseCount
        }
    }
    // PWM_MODE_RECEIVING_SOF
    else if (pwm_mode == PWM_MODE_RECEIVING_SOF) {
        //DebugPutChar('4');
        // TODO:
        pwm_mode = PWM_MODE_RECEIVING_DATA;
    }
    // PWM_MODE_RECEIVING_DATA
    else if (pwm_mode == PWM_MODE_RECEIVING_DATA) {
        pwm_stop_err_timer();
        pwm_start_err_timer(TP4_RX_MIN-u16Delta);
        if (pwm_u16RxPulseCount < J1850_RX_MAX_COUNT) {
            pwm_pu8RxPulseBuf[pwm_u16RxPulseCount++] = u16Delta;
        }
        //        DebugPutChar('5');
        //        if (u16Delta > (TP5_TX_MIN - TP2_TX_NOM)) {
        //            DebugPutChar('\r');
        //            DebugPutChar('\n');
        //        }
        // TODO: start timer to check EOD (timeout = TP3)
    }
    // PWM_MODE_SENDING_DATA
    else if (pwm_mode == PWM_MODE_SENDING_DATA) {
#if 0 // check for sending bit error
        if (pwm_bus_status != PWM_BUS_ACTIVE) {
            DebugPutChar('6');
            pwm_mode = PWM_MODE_SENDING_DATA_ERROR;
            // TODO: reset sending data
        } else {
            DebugPutChar('7');
        }
#endif
    }
    // PWM_MODE_SENDING_IFR
    else if (pwm_mode == PWM_MODE_SENDING_IFR) {
        DebugPutChar('8');
        // TODO: check if sending IFR
    }
}

void inline pwm_start_sending_ifr(void)
{
    j1850_pwm_start_sending(J1850_IFR_START_PULSE, pwm_ifr_buf, 16, false);
    
}

uint16_t pwm_calculate_pulse(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf) { //do dai toi da cua ban tin la 12 bytes
    if (u16Leng>0&&u16Leng<=12) {
        for (int i=0;i<u16Leng;i++) {
            pwm_calculate_pulse_byte(pu8Data[i], &pu8PulseBuf[i*16]);
        }
        return u16Leng*16;
    }
}

uint16_t pwm_calculate_pulse_with_sof(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf) {//do dai toi da cua ban tin la 12 bytes
    pu8PulseBuf[0] = TP7_TX_NOM;
    pu8PulseBuf[1]= TP4_TX_NOM - TP7_TX_NOM;
    uint16_t u16Ret;
    u16Ret = pwm_calculate_pulse(pu8Data, u16Leng, &pu8PulseBuf[2]);
    return u16Ret + 2;
}
void pwm_calculate_pulse_byte(uint8_t u8Data, uint8_t *pu8Buf) {
    int a;uint8_t BitCheck;
    for (a=0;a<8;a++) {
        BitCheck=u8Data&(0x80>>a);
        pwm_bit_to_pulse(BitCheck, &pu8Buf[a*2]);
    }
}
void pwm_bit_to_pulse(uint8_t BitValue, uint8_t* pu8Buf) {
    if (BitValue==0) {
        pu8Buf[0]= TP2_TX_NOM;
        pu8Buf[1]= TP3_TX_NOM - pu8Buf[0];
    }
    else {
        pu8Buf[0]=TP1_TX_NOM;
        pu8Buf[1]=TP3_TX_NOM - pu8Buf[0];
    }
}

uint8_t pwm_calculate_bit_value1(uint16_t u16Pulse1) {
    //DebugPutChar('$');
    if (TP1_RX_MIN<=u16Pulse1&&u16Pulse1<=TP1_RX_MAX) return 0x01;
    if (TP2_RX_MIN<=u16Pulse1&&u16Pulse1<=TP2_RX_MAX) return 0x00;
    // EOF
    if (TP7_RX_MIN<= u16Pulse1 && u16Pulse1 > TP7_RX_MAX)
        return PWM_SOF_BIT_VAL;
    return PWM_ERR_BIT_VAL;
}

uint8_t pwm_calculate_bit_value(uint16_t u16Pulse1, uint16_t u16Pulse2)
{
    //DebugPutChar('$');
    if (TP1_RX_MIN<=u16Pulse1&&u16Pulse1<=TP1_RX_MAX && TP2_RX_MIN<=u16Pulse2&&u16Pulse2<=TP2_RX_MAX) return 0x01;
    if (TP2_RX_MIN<=u16Pulse1&&u16Pulse1<=TP2_RX_MAX && TP1_RX_MIN<=u16Pulse2&&u16Pulse2<=TP1_RX_MAX) return 0x00;
    // EOF
    if (TP5_TX_MIN<= u16Pulse2 && u16Pulse2 > TP4_RX_MAX)
        return PWM_EOF_BIT_VAL;
    if (u16Pulse2 >= TP4_RX_MIN && u16Pulse2 <= TP4_RX_MAX)
    {
        if (u16Pulse1 >= TP7_RX_MAX && u16Pulse1 <= TP7_RX_MAX) {
            return PWM_SOF_BIT_VAL;
        }
        else {
            return PWM_EOD_BIT_VAL;
        }
    }
    return PWM_ERR_BIT_VAL;
}

inline void j1850_pwm_start_sending(uint16_t u16FirstPulse, uint8_t* pu8PulseBuf, uint16_t u16TxPulseCount, bool isFrame) {
    //    sprintf(tmpStr, "\r\n j1850_pwm_start_sending %d pulses", u16TxPulseCount);
    //		DebugPutString(tmpStr);
    //u8FirstPulse);
    pwm_pu8TxPulseBuf = pu8PulseBuf;
    pwm_u16TxPulseCount = u16TxPulseCount;
    pwm_u16TxPulseSent = 0;
    if (isFrame) {
        pwm_mode = PWM_MODE_SENDING_DATA;
        pwm_u16RxIFRPulseCount = 0;
    }
    else{
        pwm_mode = PWM_MODE_SENDING_IFR; // inframe response
    }
    pwm_stop_err_timer(); // for sure
    pwm_stop_input_capture(); // for sure
    pwm_set_output_compare_pulse(u16FirstPulse);
    pwm_start_output_compare();
}

void j1850_pwm_start_receive(void)
{
    pwm_start_input_capture();
    pwm_reset_input_capture_counter();
}
void j1850_pwm_stop_receive(void)
{
    pwm_stop_input_capture();
}

void j1850_pwm_scan(void) {
    // prepare pwm init scan
    j1850_pwm_setup();
    pwm_u8TxByteBuf[5] = j1850_crc(pwm_u8TxByteBuf,5);
    pwm_u16TxPulseCount = pwm_calculate_pulse_with_sof(pwm_u8TxByteBuf, 6, pwm_pu8TxPulseBuf);
    pwm_u16RxPulseCount = 0;
		taskENTER_CRITICAL();
		pwm_send(pwm_pu8TxPulseBuf,pwm_u16TxPulseCount);
		taskEXIT_CRITICAL();
    //j1850_pwm_start_sending(200, pwm_pu8TxPulseBuf, pwm_u16TxPulseCount, true);//TP5_TX_MIN
    //j1850_pwm_start_receive();
}



static void pwm_setPIDMode(uint8_t u8Mode, uint8_t u8Pid) {
    pwm_u8TxByteBuf[3]= u8Mode;
    pwm_u8TxByteBuf[4] = u8Pid;
}

void j1850_pwm_transmit(uint8_t mode, uint8_t pid, uint8_t u8PidBytes)
{		
    pwm_pu8TxPulseBuf = (uint8_t*)u16PulseToSAE;
    int i;
    DebugPutString("\r\nj1850_pwm_transmit");
    pwm_setPIDMode(mode, pid);
    if (u8PidBytes > 1)
    {
        sprintf(tmpStr,"\r\n PWM unsupported number of bytes %d", u8PidBytes );
        return;
    }
    uint8_t u8Len = 4 + u8PidBytes;
    pwm_u8TxByteBuf[u8Len]=j1850_crc(pwm_u8TxByteBuf,u8Len);
    pwm_u16TxPulseCount = pwm_calculate_pulse_with_sof(pwm_u8TxByteBuf, u8Len + 1,pwm_pu8TxPulseBuf);
    //    sprintf(tmpStr,"\r\nj1850_pwm_scan start to send %d pulses", pwm_u16TxPulseCount);
    //    DebugPutString(tmpStr);
    //    for (i = 0; i < pwm_u16TxPulseCount; i++) {
    //        sprintf(tmpStr, "\r\npwm_pu8TxPulseBuf[%d] = %d", i, pwm_pu8TxPulseBuf[i]);
    //        DebugPutString(tmpStr);
    //    }


    //    sprintf(tmpStr,"\r\nj1850_pwm_scan ifr: %d pulses", 16);
    //    DebugPutString(tmpStr);
    //    DebugPutString("\r\n");
    //    for (i = 0; i < 16; i++) {
    //        sprintf(tmpStr, "\r\npwm_ifr_buf[%d] = %d", i, pwm_ifr_buf[i]);
    //        DebugPutString(tmpStr);
    //    }
    //    DebugPutString("\r\n");
    pwm_u16RxPulseCount = 0;
    u8j1850_Rx_Frame_Count = 0;
    pwm_stop_output_compare();
    j1850_pwm_start_sending(70, pwm_pu8TxPulseBuf, pwm_u16TxPulseCount, true);

}

static uint8_t pwm_bits_to_bytes(uint8_t* pu8Value)
{
    uint8_t FinalValue=0;
    for (int i=0;i<8;i++)
    {
        FinalValue+=(pu8Value[i]<<(7-i));
    }
    return FinalValue;
}
void j1850_pwm_print_rx_pulse(void) {
    uint16_t i, j;
    uint8_t u8Bit, u8Byte;
    uint8_t u8Bits[9];
    j = 0;
    uint16_t u16ByteCount = 0;
    sprintf(tmpStr, "\r\npwm_u16RxPulseCount = %d\r\n", pwm_u16RxPulseCount);
    DebugPutString(tmpStr);
    for (i = 0; i < pwm_u16RxPulseCount; i++) {
        sprintf(tmpStr, " %d, ", pwm_pu8RxPulseBuf[i]);
        DebugPutString(tmpStr);
        if (i&0x01) {
            u8Bit = pwm_calculate_bit_value1(pwm_pu8RxPulseBuf[i-1]);
            u8Bits[j++] = u8Bit;
            sprintf(tmpStr, "====>byte[%d] = %d\r\n", i/2, u8Bit);
            DebugPutString(tmpStr);
            if (j >= 8)  {
                j = 0;
                u8Byte = pwm_bits_to_bytes(u8Bits);
                sprintf(tmpStr, "\r\n >>>>>> Byte[%d] = 0x%02x <<<<<\r\n", u16ByteCount++, u8Byte);
                DebugPutString(tmpStr);
            }
        }
    }
    DebugPutString("\r\n==========");
    sprintf(tmpStr, "\r\npwm_u16RxIFRPulseCount = %d\r\n", pwm_u16RxIFRPulseCount);
    DebugPutString(tmpStr);
    j = 0;
    u16ByteCount = 0;
    for (i = 0; i < pwm_u16RxIFRPulseCount; i++) {
        sprintf(tmpStr, "\r\n %d, ", pwm_ifr_buf_read[i]);
        DebugPutString(tmpStr);
        if (i&0x01) {
            //            u8Bit = pwm_calculate_bit_value(pwm_ifr_buf_read[i-1], pwm_ifr_buf_read[i]);
            u8Bit = pwm_calculate_bit_value1(pwm_ifr_buf_read[i-1]);
            sprintf(tmpStr, "====>pwm_ ifr rx bit[%d] = %d\r\n", i/2, u8Bit);
            u8Bits[j++] = u8Bit;
            DebugPutString(tmpStr);
            if (j >= 8)  {
                j = 0;
                u8Byte = pwm_bits_to_bytes(u8Bits);
                sprintf(tmpStr, "\r\n >>>>>> pwm_ ifr rx byte[%d] = 0x%02x <<<<<\r\n", u16ByteCount++, u8Byte);
                DebugPutString(tmpStr);
            }
        }
    }
}
/**
 * pwm_pu8RxPulseBuf = (uint8_t*)j1850_RX_Pulse_Buf;
 * pwm_pu8RxBitBuf = (uint8_t*) j1850_Rx_Bits_Buf;
 * pwm_pu8RxByteBuf = (uint8_t*) j1850_RX_Byte_Buf;
 * @brief from pulses in pwm_pu8RxPulseBuf convert to byte in pwm_pu8RxByteBuf
 */
uint16_t j1850_pwm_process_rx_task(void) {
    uint16_t i, j;
    uint8_t u8Bit, u8Byte;
    //uint16_t u16ByteIdx;
    uint16_t u16ByteCount = 0;
    pwm_u16RxByteCount = 0;
    uint8_t u8Bits[9];
    j = 0;
    sprintf(tmpStr, "\r\npwm_u16RxPulseCount = %d\r\n", pwm_u16RxPulseCount);
    DebugPutString(tmpStr);
    //
    u8j1850_Rx_Frame_Count = 0;
    u8j1850_Rx_Start_Byte[0] = 0;

    for (i = 0; i < pwm_u16RxPulseCount; i++) {
        sprintf(tmpStr, " %d, ", pwm_pu8RxPulseBuf[i]);
        DebugPutString(tmpStr);
        if (i&0x01) {
            u8Bit = pwm_calculate_bit_value1(pwm_pu8RxPulseBuf[i-1]);
            // if pwm_pu8RxPulseBuf[i] > TP3_TX_MAX
            if (pwm_pu8RxPulseBuf[i] ==  255)
            {
                u8j1850_Rx_Stop_Byte[u8j1850_Rx_Frame_Count++] = (i)/16;
                u8j1850_Rx_Start_Byte[u8j1850_Rx_Frame_Count] = (i)/16+1;
            }
            u8Bits[j++] = u8Bit;
            sprintf(tmpStr, "====>byte[%d] = %d\r\n", i/2, u8Bit);
            DebugPutString(tmpStr);
            if (j >= 8)  {
                j = 0;
                u8Byte = pwm_bits_to_bytes(u8Bits);
                pwm_pu8RxByteBuf[pwm_u16RxByteCount++]  = u8Byte;
                sprintf(tmpStr, "\r\n >>>>>> Byte[%d] = 0x%02x <<<<<\r\n", pwm_u16RxByteCount-1, u8Byte);
                DebugPutString(tmpStr);
            }
        }
    }
    DebugPutString("\r\n==========");
    sprintf(tmpStr, "\r\npwm_u16RxIFRPulseCount = %d\r\n", pwm_u16RxIFRPulseCount);
    DebugPutString(tmpStr);
    j = 0;
    for (i = 0; i < pwm_u16RxIFRPulseCount; i++) {
//        sprintf(tmpStr, "\r\n %d, ", pwm_ifr_buf_read[i]);
//        DebugPutString(tmpStr);
        if (i&0x01) {
            //            u8Bit = pwm_calculate_bit_value(pwm_ifr_buf_read[i-1], pwm_ifr_buf_read[i]);
            u8Bit = pwm_calculate_bit_value1(pwm_ifr_buf_read[i-1]);
//            sprintf(tmpStr, "====>pwm received ifr bit[%d] = %d\r\n", i/2, u8Bit);
            u8Bits[j++] = u8Bit;
//            DebugPutString(tmpStr);
            if (j >= 8)  {
                j = 0;
                u8Byte = pwm_bits_to_bytes(u8Bits);
//                sprintf(tmpStr, "\r\n >>>>>> Byte[%d] = 0x%02x <<<<<\r\n", u16ByteCount++, u8Byte);
//                DebugPutString(tmpStr);
            }
        }
    }
    return pwm_u16RxByteCount;
}
///////////////////////////////////////// brutal pwm ///////////////////////////////////////////////////////////////
void brutalPwmInit(void){
	GPIO_InitTypeDef GPIO_InitStruct;
	
  /*Configure GPIO pins : PB0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN; //GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void brutalPwmDeinit(void){
	HAL_GPIO_DeInit(GPIOB,GPIO_PIN_0);
}

PWM_SEND pwm_send(uint8_t* pu8PulseBuff, uint16_t u16PulseCount){
	int i=0;
	int count;
//	for(i=0;i<u16PulseCount;i++){
//		sprintf(tmpStr,"\r\npulse %u = %u",i,pu8PulseBuff[i]);
//		DebugPutString(tmpStr);
//	}
	i=0;
	while (i<u16PulseCount){
		SAE_OUT_HIGH();
		__delay_us(pu8PulseBuff[i++]);
		if (HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
			return PWM_SEND_NOK;
		}
		SAE_OUT_LOW();
		__delay_us(pu8PulseBuff[i++]);
		if (HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_SET){
			return PWM_SEND_OVER_WRITE;
		}
	}
	count=0;
	while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
		__delay_us(1);
		if(count++>TP4_TX_NOM) {
			return PWM_SEND_BUT_NO_ACK;
		}
	}
	ackTime = count;
	for(i=0;i<8;i++){
		count=0;
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
			__delay_us(1);
			if(count++>10){
				return PWM_SEND_BUT_ACK_WRONG;
			}
		}
		count=0;
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_SET){
			__delay_us(1);
			if(count++>10){
				return PWM_SEND_BUT_ACK_WRONG;
			}
		}
		ackPulse[i] = count;
	}
	return PWM_SEND_OK;
}

PWM_SEND pwm_send_cmd(uint8_t u8mode, uint8_t u8pid){
//	j1850_pwm_setup();
	pwm_u16TxPulseCount = 0;
	memset(pwm_pu8TxPulseBuf,0x00,255);
	pwm_u8TxByteBuf[3] = u8mode;
	pwm_u8TxByteBuf[4] = u8pid;
	pwm_u8TxByteBuf[5] =  j1850_crc(pwm_u8TxByteBuf,5);
	pwm_u16TxPulseCount = pwm_calculate_pulse_with_sof(pwm_u8TxByteBuf, 6, pwm_pu8TxPulseBuf);
	return pwm_send(pwm_pu8TxPulseBuf,pwm_u16TxPulseCount);
}

PWM_SEND j1850_pwm_brutal_transmit(uint8_t u8Mode, uint8_t u8Pid, uint8_t u8PidBytes) {
		if (u8PidBytes > 1) {
        return PWM_SEND_NOK;
    }
		pwm_u16TxPulseCount = 0;
		memset(pwm_pu8TxPulseBuf,0x00,255);
		pwm_u8TxByteBuf[3] = u8Mode;
		pwm_u8TxByteBuf[4] = u8Pid;
		uint8_t u8Len = 4 + u8PidBytes;
		pwm_u8TxByteBuf[u8Len] =  j1850_crc(pwm_u8TxByteBuf,u8Len);
		pwm_u16TxPulseCount = pwm_calculate_pulse_with_sof(pwm_u8TxByteBuf, u8Len+1, pwm_pu8TxPulseBuf);
		taskENTER_CRITICAL();
    return pwm_send(pwm_pu8TxPulseBuf,pwm_u16TxPulseCount);
}


PWM_RECEIVE pwm_read_pulse(void){
	// changer receivePulse to pwm_pu8RxPulseBuf & receivePulseCount to pwm_u16RxPulseCount
	int count = 0;
	memset (pwm_pu8RxPulseBuf,0x00,1700);
	u8j1850_Rx_Frame_Count=0;
	__delay_us(20);
	while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
		__delay_us(1);
		if(count++>4000){
			return PWM_RECEIVE_NOTHING;
		}
	}
	//u8j1850_Rx_Frame_Count = 1;
	pwm_u16RxPulseCount = 0;
	while(1){
		GET_FRAME:
		count=0;
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_SET){
			__delay_us(1);
			if(count++>TP7_TX_NOM){
				return PWM_RECEIVE_NOK;
			}
		}
		//receivePulse[receivePulseCount++]=count;
		pwm_pu8RxPulseBuf[pwm_u16RxPulseCount++] = count;
		if (count<=12){
			while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
				count++;
				__delay_us(1);
				if(count>23){
					pwm_send_ack();
					goto AFTER_ACK;
				}
			}
		} else {
			while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
				__delay_us(1);
				if(count++>30){
					return PWM_RECEIVE_NOK;
				}
			}
		}
		// take more than 1 frame
		AFTER_ACK:
		count = 0;
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==GPIO_PIN_RESET){
			__delay_us(1);
			if(count++>100){
				return PWM_RECEIVE_OK;
			}
		}
		//pwm_pu8RxPulseBuf[pwm_u16RxPulseCount++] = 100;
		//u8j1850_Rx_Frame_Count++;
		goto GET_FRAME;
	}
}

void pwm_read_byte(void){
	int i = 0;
	u8j1850_Rx_Frame_Count = 0;
	pwm_u16RxByteCount = 0;
	memset (pwm_pu8RxByteBuf,0x00,255);
	sprintf (tmpStr,"\r\nPWM read %u pulse\r\n",pwm_u16RxPulseCount);
	DebugPutString(tmpStr);
//	for(i=0;i<pwm_u16RxPulseCount;i++){
//		sprintf(tmpStr,"\r\n\tPWM pulse %u = %u",i,pwm_pu8RxPulseBuf[i]);
//		DebugPutString(tmpStr);
//	}
	if (pwm_pu8RxPulseBuf[0]<15) {
		DebugPutString("\r\nMessage received had wrong SOF");
		return;
	} 
	i=0;
	while (i<pwm_u16RxPulseCount){
		if (pwm_pu8RxPulseBuf[i]>=15){
			i++;
			u8j1850_Rx_Frame_Count++;
			u8j1850_Rx_Start_Byte[u8j1850_Rx_Frame_Count] = pwm_u16RxByteCount;
			DebugPutString("\r\n");
		} else {
			pwm_pu8RxByteBuf[pwm_u16RxByteCount] = pwm_pulse_to_byte(&pwm_pu8RxPulseBuf[i]);
			sprintf(tmpStr,"0x%02x\t",pwm_pu8RxByteBuf[pwm_u16RxByteCount]);
			DebugPutString(tmpStr);
			u8j1850_Rx_Stop_Byte[u8j1850_Rx_Frame_Count] = pwm_u16RxByteCount;
			pwm_u16RxByteCount++;
			i += 8;
		}
	}
	sprintf(tmpStr,"\r\nPWM read %u byte\r\n", pwm_u16RxByteCount);
	DebugPutString(tmpStr);
	return;
//	pwm_u16RxByteCount = pwm_u16RxPulseCount/8;
//	sprintf(tmpStr,"\r\nPWM read %u byte\r\n", pwm_u16RxByteCount);
//	DebugPutString(tmpStr);
//	for (int i=0;i<pwm_u16RxByteCount;i++){
//		pwm_pu8RxByteBuf[i] = pwm_pulse_to_byte(&pwm_pu8RxPulseBuf[1+i*8]);
//		sprintf(tmpStr,"0x%02x\t",pwm_pu8RxByteBuf[i]);
//		DebugPutString(tmpStr);
//	}
	return;
}

void inline pwm_send_ack(void){
//	int i=0;
//	while(i<16){
//		SAE_OUT_HIGH();
//		__delay_us(pwm_pu8ReceiveAckPulse[i++]);
//		SAE_OUT_LOW();
//		__delay_us(pwm_pu8ReceiveAckPulse[i++]);
//	}
//	return;
	//{8,16, 8,16, 8,16, 8,16, 16,8, 16,8, 16,8, 8,16};
	SAE_OUT_HIGH();
	__delay_us(8);
	SAE_OUT_LOW();
	__delay_us(16);
	SAE_OUT_HIGH();
	__delay_us(8);
	SAE_OUT_LOW();
	__delay_us(16);
	SAE_OUT_HIGH();
	__delay_us(8);
	SAE_OUT_LOW();
	__delay_us(16);
	SAE_OUT_HIGH();
	__delay_us(8);
	SAE_OUT_LOW();
	__delay_us(16);
	
	SAE_OUT_HIGH();
	__delay_us(16);
	SAE_OUT_LOW();
	__delay_us(8);
	SAE_OUT_HIGH();
	__delay_us(16);
	SAE_OUT_LOW();
	__delay_us(8);
	SAE_OUT_HIGH();
	__delay_us(16);
	SAE_OUT_LOW();
	__delay_us(8);
	SAE_OUT_HIGH();
	__delay_us(8);
	SAE_OUT_LOW();
	__delay_us(16);
	return;
}

uint8_t pwm_pulse_to_byte(uint8_t* pu8PusleBuf) {
	uint8_t ret =0;
	for(int i=0; i<8; i++){
		ret <<= 1;
		if (pu8PusleBuf[i]<6){
			ret |= 0x01;
		}else {/* just ignore */}
	}
	return ret;
}


uint16_t pwm_rx_byte_number(void){
	PWM_RECEIVE iRead = pwm_read_pulse();
	taskEXIT_CRITICAL();
	if (iRead==PWM_RECEIVE_OK){
		pwm_read_byte();
		return pwm_u16RxByteCount;
	} else return 0;
}
