/**
 * @file kwp.h
 * @brief KWP2000 & ISO9141 via uart for stm32f1 (odbi adapter)
 * @date May 04, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */

#include "kwp.h"
#include "debug.h"

uint8_t FastStartRequest[5]={0xC1,0x33,0xF1,0x81,0x66};
uint8_t KeepContactRequest[5]={0xC1,0x33,0xF1,0x3E,0x23}; //Ban tin Keep alive  C1 33 F1 3E 23 
uint8_t ToyotaFastStartRequest[5]={0x81,0x13,0xF0,0x81,0x05};
//uint8_t KeepContactRequest[5]={0x81,0x13,0xF0,0x3E,0xC2};
uint8_t ToyotaKeepContactRequest[1] = {0x07};
uint8_t PID0100Request9141[6]={0x68,0x6A,0xF1,0x01,0x00,0xC4}; //Ban tin Keep alive 9141
//uint8_t PID0100RequestKWP[6]={0xC2,0x33,0xF1,0x01,0x00,0xE7};
uint8_t PID0100RequestKWP[6]={0x81,0x13,0xF0,0x01,0x00,0x85};
uint8_t* KWP_ISO_KeepAliveRequest = NULL;
uint8_t ISORxData,DebugRxData;
uint8_t KB1, KB2;
uint8_t PATTERN, KB2_BACK, ADDRESS;
static char tmpStr[256];
extern osSemaphoreId g_ODB2_Semid;
extern osSemaphoreId g_BLESemid;
extern ODB2Message g_ODB2Msg;
extern ODB2_PROTOCOL g_ODB2Protocol;
static uint8_t KWP_ISO_TX_Buf[64];
static uint8_t KWP_ISO_RX_Buf[128];
bool bKWP_Alive = false;

uint8_t ToyotaPidStart[2] = {0x10,0x5F};
uint8_t ToyotaKeepContact[1] = {0x07};
uint8_t ToyotaTest1[2] = {0x09,0x00};

extern bool g_ToyotaMode01Increase;

/* USART3 init function */
bool ISO_USART3_UART_Init(int baudrate)
{
    huart3.Instance = USART3;
    huart3.Init.BaudRate = baudrate;
    huart3.Init.WordLength = UART_WORDLENGTH_8B;
    huart3.Init.StopBits = UART_STOPBITS_1;
    huart3.Init.Parity = UART_PARITY_NONE;
    huart3.Init.Mode = UART_MODE_TX_RX;
    huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart3.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart3) != HAL_OK)
    {
        //Error_Handler();
        return false;
    }
    return true;
}

bool ODB2_Fast_Init()
{
    DebugPutString("\r\nODB2_Fast_Init\r\n");
    uint8_t StartCommunicationRequestResponse[ISO_KWP_DATA_LEN+1];
    ISO_UART_DISABLE();
    GPIO_InitTypeDef GPIO_InitStruct;
    HAL_GPIO_WritePin(ISO_UART_GPIO,ISO_UART_PIN_TX, GPIO_PIN_SET);
    GPIO_InitStruct.Pin=ISO_UART_PIN_TX;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(ISO_UART_GPIO, &GPIO_InitStruct);
    // Wakeup Pattern
    //HAL_GPIO_WritePin(ISO_UART_GPIO,ISO_UART_PIN_TX,GPIO_PIN_RESET);
    ISO_TX_RESET();
    HAL_Delay(25);
    //HAL_GPIO_WritePin(ISO_UART_GPIO,ISO_UART_PIN_TX,GPIO_PIN_SET);
    ISO_TX_SET();
    HAL_Delay(25);
    ISO_UART_ENABLE(UART_ISO9141_BAUDRATE);
    HAL_UART_Transmit(&UART_ISO,FastStartRequest,5,0xff);
    HAL_Delay(KWP_P2_MIN);
    uint32_t u32Len = 0;
    uint32_t u32TotalLen = 0;
    while (true) {
        while (HAL_UART_Receive(&UART_ISO, &KWP_ISO_RX_Buf[u32Len], 1, 0xFF)==HAL_OK) {
            u32Len++;
        }
        u32TotalLen += u32Len;
        if (u32Len > 0) {
            //DebugPutString("\r\nKWP Alive Response: ");
            dumpHex(KWP_ISO_RX_Buf, u32Len);
            // reset u32Len to receivce other ECU response
            u32Len = 0;
            HAL_Delay(KWP_P2_MIN);
        } else {
            //DebugPutString("\r\n KWP Not received data");
            HAL_Delay(KWP_P3_MIN);
            break;
        }
    }
    if (u32TotalLen >= 7) {
        return true;
    } else return false;
}

bool ODB2_Slow_Init()
{
    DebugPutString("\r\nODB2_Slow_Init\r\n");
		__HAL_RCC_USART3_FORCE_RESET();
    ISO_UART_DISABLE();
		__HAL_RCC_USART3_RELEASE_RESET();
    // Setup GPIO for sending 5-baud

    //uint8_t u8Buff;
    //GPIO_InitTypeDef GPIO_InitStruct;
    ISO_TX_SET();
    GPIO_KlinePinInit();
		HAL_Delay(2000);
    //Truyen 0x33 5 baund
    ISO_TX_RESET();
    HAL_Delay(200);
    ISO_TX_SET();
    HAL_Delay(400);
    ISO_TX_RESET();
    HAL_Delay(400);
    ISO_TX_SET();
    HAL_Delay(400);
    ISO_TX_RESET();
    HAL_Delay(400);
    ISO_TX_SET();
    HAL_Delay(200);
		// try this
    // Reinit UART and receive pattern bytes
    // should be 0x55 following by 2 bytes protocol specified pattern
		HAL_GPIO_DeInit(ISO_UART_GPIO,ISO_UART_PIN_TX);
		
    ISO_UART_ENABLE(UART_ISO9141_BAUDRATE);
    //HAL_UART_Receive(&UART_ISO,&u8Buff,1,1); // try this (comment)
    if (HAL_UART_Receive(&UART_ISO,&PATTERN,1,0xfff)==HAL_OK) {
        if (PATTERN != 0x55) {
            sprintf(tmpStr,"Pattern: 0x%02x is wrong\r\n",PATTERN);
            DebugPutString(tmpStr);
            return false;
        }
    }
    else {
        return false;
    }
    if (HAL_UART_Receive(&UART_ISO,&KB1,1,0xff)==HAL_OK) {
        //sprintf(temp,"KB1:0x%02x\r\n",KB1);
        //DebugPutString(temp);
    } else {
        return false;
    }
    if (HAL_UART_Receive(&UART_ISO,&KB2,1,0xff)==HAL_OK) {
        //sprintf(temp,"KB2:0x%02x\r\n",KB2);
        //DebugPutString(temp);
    } else {
        return false;
    }
    KB2_BACK = 0xFF & (~KB2);
    HAL_Delay(25);
    HAL_UART_Transmit(&UART_ISO,&KB2_BACK,1,0xff);
    if (HAL_UART_Receive(&UART_ISO,&ADDRESS,1,0xff)==HAL_OK) {
        //sprintf(temp,"ADDRESS:0x%02x\r\n",ADDRESS);
        //DebugPutString(temp);
    } else {
        return false;
    } 
    sprintf(tmpStr, "\r\nSlow Init Success, pattern: 0x%02x, kb1: 0x%02x, kb2: 0x%02x, addr = 0x%02x \r\n", PATTERN, KB1, KB2, ADDRESS);
    DebugPutString(tmpStr);
    HAL_Delay(KWP_P3_MIN);
    return true;
}
int ODB2_ISO_KWP_Init()
{
    int ret = ISO_KWP_NONE;
		if (g_ODB2Protocol == ODB2_PROTOCOL_START || g_ODB2Protocol == ODB2_PROTOCOL_ISO9141 || g_ODB2Protocol == ODB2_PROTOCOL_KWP2000_SLOW){
			if (ODB2_Slow_Init())	{
					// ISO9141
					if ((KB1 == 0x08 && KB2 == 0x08) || (KB1== 0x94 && KB2 == 0x94)) {
							KWP_ISO_KeepAliveRequest = PID0100Request9141;
							bKWP_Alive = true;
							return ISO9141;
					}
					// KWP_2000_SLOW
					if (KB1 == 0x8F || KB2 == 0x8F){ // hoi lao
							//            if (KB2 == 0xE9 || KB2 == 0x6B || KB2 == 0x6D || KB2 == 0xEF)
							KWP_ISO_KeepAliveRequest = KeepContactRequest;
							bKWP_Alive = true;
							return KWP_2000_SLOW;
					}
			}
			sprintf(tmpStr,"Pattern: 0x%02x, KB1: 0x%02x, KB2: 0x%02x, wrong\r\n",PATTERN, KB1, KB2);
			DebugPutString(tmpStr);
			
			HAL_Delay(2000);
		}
		if (g_ODB2Protocol == ODB2_PROTOCOL_START || g_ODB2Protocol == ODB2_PROTOCOL_KWP2000_FAST){
			if (ODB2_Fast_Init()) {
					KWP_ISO_KeepAliveRequest = KeepContactRequest;//PID0100RequestKWP;
					bKWP_Alive = true;
					return KWP_2000_FAST;
			}
			HAL_Delay(2000);
		}
		if (g_ODB2Protocol == ODB2_PROTOCOL_START || g_ODB2Protocol == ODB2_PROTOCOL_TOYOTA_KLINE){
			if(ODB2_Toyota_Fast_Init_Custom(25,25,0x13,UART_ISO9141_BAUDRATE)){
					bKWP_Alive = true;
					return KWP_FAST_TOYOTA;
			}
			if(ODB2_Toyota_Fast_Init_Custom(35,15,0x13,UART_KLINE_BAUDRATE_9600)){
					bKWP_Alive = true;
					return KWP_FAST_TOYOTA;
			}
			HAL_Delay(2000);
		}
		return ret;
}
/**
 * @brief ODB2_KWP_ProcessBleRequest
 *        - request ODB2 mode and pid in g_ODB2Msg
 *        - if received correct response
 *          + set correct response length data g_ODB2Msg.u8RespLen
 *          + copy response data to g_ODB2Msg.pu8Data
 *          + set g_ODB2Msg.u8Status b = ODB2_STATUS_PROCESS_END
 *
 * @return
 */
int ODB2_KWP_ProcessBleRequest()
{			
    // TODO: process g_ODB2Msg
    uint8_t u8PIDBytes = 0;
    uint8_t u8Mode = g_ODB2Msg.u8Mode;
    uint8_t u8PID = g_ODB2Msg.u8PID;
    uint32_t u32Len = 0;
    uint32_t u32TotalLen = 0;
    uint32_t u32Idx = 0;
    uint32_t u32Idx2;
    uint8_t u8FirstByte;
    uint8_t u8StopByte;
		uint32_t u32Start;
		uint8_t u8CopiedBytes;
    // Check for PID mode 0x03
    if (g_ODB2Msg.u8Mode == 0x03 || g_ODB2Msg.u8Mode == 0x04)
    {
        u8PIDBytes = 0;
    } else u8PIDBytes = 1;
    if (kwp_send_mode_pid(u8Mode, u8PID, u8PIDBytes))
    {
				//__HAL_UART_FLUSH_DRREGISTER(&UART_ISO);
        while (true) {
            while (HAL_UART_Receive(&UART_ISO, &KWP_ISO_RX_Buf[u32Len], 1, 0xFF)==HAL_OK) {
                u32Len++;
                if (u32Len > 127) break;
            }
            u32TotalLen += u32Len;
            if (u32Len > 3) { 
                DebugPutString("\r\nProcessBleRequest: ");
                dumpHex(KWP_ISO_RX_Buf, u32Len);
                // 0x68 0x48 0x6B 0x10 0x41 0x00 0xBF 0xBF 0xA8 0x91 0xBB
							for(u32Start = 0; u32Start<u32Len; u32Start++){
								if(KWP_ISO_RX_Buf[u32Start]==0x10) {
									break;
								}
							}
							if(u32Start==u32Len) break;
							u32Start -= 2;
							if (g_ODB2Msg.u8Mode != 0x03){
                 if (check_kwp_full_crc(&KWP_ISO_RX_Buf[u32Start], u32Len-u32Start)) {
											if (KWP_ISO_RX_Buf[4+u32Start] == g_ODB2Msg.u8PID) {
                            g_ODB2Msg.u8RespLen = (u32Len-4-u32Start) & 0xFF;
														sprintf(tmpStr,"\r\n g_ODB2Msg.u8RespLen = %u", g_ODB2Msg.u8RespLen);
														DebugPutString(tmpStr);
                            memcpy(g_ODB2Msg.pu8Data, &KWP_ISO_RX_Buf[3+u32Start], g_ODB2Msg.u8RespLen);
                            g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
                            break;
                       } else {
                            break;
                       }
                  }//add break here so that if check_kwp_full_crc is false program will not be into the infinity loop
									DebugPutString("Receiver wrong crc\r\n");
									break; 
							}
							else {// Process for 0x03
							/*ProcessBleRequest ISO9141-2 example
								68 48 6B 10 43 01 43 01 96 02 34 17
                48 6B 10 43 02 CD 03 57 0A 24 5D
                48 6B 10 43 04 43 00 00 00 00 4D */
							/* Process KWP2000 example
								0x03 0xE8 0x87 0xF1 0x10 0x43 0x01 0x43
								0x01 0x96 0x02 0x34 0xDC 0x87 0xF1 0x10
								0x43 0x02 0xCD 0x03 0x57 0x0A 0x24 0x22
								0x87 0xF1 0x10 0x43 0x04 0x43 0x00 0x00
								0x00 0x00 0x12 */ 
                u32TotalLen = 0;
                g_ODB2Msg.u8RespLen = 0;
                sprintf(tmpStr, "\r\nProcess KWP DTC mode0x03, u32Len = %d", u32Len);
                DebugPutString(tmpStr);
                while (u32Idx < u32Len ) {
										if (g_ODB2Protocol == ODB2_PROTOCOL_ISO9141) {
													if (KWP_ISO_RX_Buf[u32Idx] == 0x48 && KWP_ISO_RX_Buf[u32Idx +1] == 0x6B
																&& KWP_ISO_RX_Buf[u32Idx+2] == 0x10)	{
															u8FirstByte = u32Idx;
                              u32Idx2 = u32Idx + 4;
                              while (u32Idx2 < u32Len ) {
                                    if (KWP_ISO_RX_Buf[u32Idx2] == 0x48 && KWP_ISO_RX_Buf[u32Idx2 +1] == 0x6B
                                            && KWP_ISO_RX_Buf[u32Idx2+2] == 0x10) {
                                        break;
                                    } else {
                                        u32Idx2++;
                                    }
                              }
                              u8StopByte = u32Idx2;
                              sprintf(tmpStr, "\r\n firstByte = %d, stopByte = %d", u8FirstByte, u8StopByte);
                              DebugPutString(tmpStr);
                              if (check_kwp_full_crc(&KWP_ISO_RX_Buf[u8FirstByte], u8StopByte-u8FirstByte))
                              {
																//48 6B 10 43 01 43 01 96 02 34 17 48
															  //         43 is sid (mode 03)     48 is stopbyte(next frame start byte) 
															  //            01 43 01 96 02 34 17 ==> DTC datas
																if (u8StopByte > (u8FirstByte+5)) {
																	u8CopiedBytes = u8StopByte-u8FirstByte-5;
																	memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], &KWP_ISO_RX_Buf[u8FirstByte+4],
																					u8CopiedBytes);
																	g_ODB2Msg.u8RespLen+=u8CopiedBytes;
																	sprintf(tmpStr, "\r\n Correct crc len = %d", u8StopByte-u8FirstByte-4);
																	DebugPutString(tmpStr);
																	u32TotalLen = g_ODB2Msg.u8RespLen;
																}
                              } else {
                                  break;// stop
                              }
                              u32Idx = u8StopByte;

                            } else {
                                u32Idx++;
                            }

                        } else { // KWP 2000
                            if ( KWP_ISO_RX_Buf[u32Idx +1] == 0xF1
                                 && KWP_ISO_RX_Buf[u32Idx+2] == 0x10)
                            {
                                u8FirstByte = u32Idx;
                                u32Idx2 = u32Idx + 4;
                                while (u32Idx2 < u32Len ) {
                                    if ( KWP_ISO_RX_Buf[u32Idx2 +1] == 0xF1
                                         && KWP_ISO_RX_Buf[u32Idx2+2] == 0x10) {
                                        break;
                                    }
                                    else {
                                        u32Idx2++;
                                    }
                                }
                                u8StopByte = u32Idx2;
                                sprintf(tmpStr, "\r\n firstByte = %d, stopByte = %d", u8FirstByte, u8StopByte);
                                DebugPutString(tmpStr);
                                if (check_kwp_full_crc(&KWP_ISO_RX_Buf[u8FirstByte], u8StopByte-u8FirstByte))
                                {
																	  //0x87 0xF1 0x10 0x43 0x01 0x43	0x01 0x96 0x02 0x34 0xDC 0x87
																	  //0x87 is frame start byte
																	  //     0xF1 is ECU addr
																	  //          0x10 is IVG addr
																		// 							 0x43 is sid
																		//              			0x01 0x43	0x01 0x96 0x02 0x34 0xDC=> DTC data
																	  //																												0x87 is stop byte (next frame start byte)
																		if (u8StopByte > (u8FirstByte+5)) {
																			u8CopiedBytes = u8StopByte-u8FirstByte-5;
																			memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], &KWP_ISO_RX_Buf[u8FirstByte+4],
																							u8CopiedBytes);
																			g_ODB2Msg.u8RespLen+=u8CopiedBytes;
																			sprintf(tmpStr, "\r\n Correct crc len = %d", u8StopByte-u8FirstByte-4);
																			DebugPutString(tmpStr);
																			u32TotalLen = g_ODB2Msg.u8RespLen;
																		}
                                }
                                else {
                                    break;// stop
                                }
                                u32Idx = u8StopByte;

                            } else {
                                u32Idx++;
                            }
                        }// end of KWP 2000

                    }
                    if (u32TotalLen > 0) {
                        sprintf(tmpStr, "\r\nKline response 03 Mode len = %d", u32TotalLen);
                        DebugPutString(tmpStr);
                        g_ODB2Msg.u8RespLen = u32TotalLen;
                        g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
                        break;
                    }
                }
                HAL_Delay(KWP_P2_MIN);
            } else {
                DebugPutString("\r\n KWP Not received data");
                //bKWP_Alive = false;
                HAL_Delay(KWP_P3_MIN);
                break;
            }
        }
    } else {

    }
    DebugPutString("\r\nODB2_KWP_ProcessBleRequest Done");
}

int ODB2_KWP_CheckForKeepAlive()	{
    if (bKWP_Alive) {
        ODB2_KWP_KeepAlive();
    } else {
        // Try to reinit
        if (g_ODB2Protocol == ODB2_PROTOCOL_KWP2000_FAST) {
            while (!ODB2_Fast_Init()) {
                osDelay(300);
            }
            bKWP_Alive = true;
        } else if(g_ODB2Protocol == ODB2_PROTOCOL_TOYOTA_KLINE){
					while (!ODB2_Toyota_Fast_Init_Custom(25,25,0x13,UART_ISO9141_BAUDRATE)){
						osDelay(500);
					}
					bKWP_Alive = true;
				} else {
            while (1) {
								if(ODB2_Slow_Init()){
									if(g_ODB2Protocol == ODB2_PROTOCOL_ISO9141){
										if((KB1 == 0x08 && KB2 == 0x08) || (KB1== 0x94 && KB2 == 0x94))
											break;
									} else {
										if (KB1 == 0x8F || KB2 == 0x8F) break;
									}
								}
                osDelay(2000);
            }
            bKWP_Alive = true;
        }
    }
}

int ODB2_KWP_KeepAlive()
{
    // TODO: Send and receive KeepAlive Message
    DebugPutString("\r\nODB2_KWP_KeepAlive");
    if (g_ODB2Protocol == ODB2_PROTOCOL_ISO9141)
        HAL_UART_Transmit(&UART_ISO,KWP_ISO_KeepAliveRequest,6,0xff);
    else if(g_ODB2Protocol == ODB2_PROTOCOL_TOYOTA_KLINE){
			toyota_send(ToyotaKeepContactRequest,1);
			//return bKWP_Alive;
		} else {
			HAL_UART_Transmit(&UART_ISO,KWP_ISO_KeepAliveRequest,5,0xff);
		}
    uint32_t u32Len = 0;
    uint32_t u32TotalLen = 0;
    HAL_Delay(KWP_P2_MIN);
    while (true) {
        while (HAL_UART_Receive(&UART_ISO, &KWP_ISO_RX_Buf[u32Len], 1, 0xFF)==HAL_OK)
        {
            u32Len++;
        }
        u32TotalLen += u32Len;
        if (u32Len > 0)
        {
            //bKWP_Alive = true;
            DebugPutString("\r\nKWP Alive Response: ");
            dumpHex(KWP_ISO_RX_Buf, u32Len);
            // reset u32Len to receivce other ECU response
            u32Len = 0;
            HAL_Delay(KWP_P2_MIN);
        } else {
            DebugPutString("\r\n KWP Not received data");
            //bKWP_Alive = false;
            HAL_Delay(KWP_P3_MIN);
            break;
        }
    }
    if (u32TotalLen > 5)
    {
        bKWP_Alive = true;
    } else {
        bKWP_Alive = false;
    }
    return bKWP_Alive;
}

uint8_t kwp_CalculateCRC(uint8_t *pu8Data, uint8_t u8Len)
{
    uint8_t i;
    uint16_t u16CRC = pu8Data[0];
    for (i = 1; i < u8Len; i++)
    {
        u16CRC = ((u16CRC+ pu8Data[i]) % 256);
    }
    return (u16CRC & 0xFF);
}


bool check_kwp_crc(uint8_t* pu8Data, uint8_t u8Len)
{
    if (pu8Data[u8Len] == kwp_CalculateCRC(pu8Data, u8Len))
    {
        return true;
    }
    else { return false;}
}

bool check_kwp_full_crc(uint8_t* pu8Data, uint8_t u8Len)
{
    if (pu8Data[u8Len-1] == kwp_CalculateCRC(pu8Data, u8Len-1)) {
        return true;
    } else { return false; }
}

//uint8_t PID0100Request9141[6]={0x68,0x6A,0xF1,0x01,0x00,0xC4}; //Ban tin Keep alive 9141
//uint8_t PID0100RequestKWP[6]={0xC2,0x33,0xF1,0x01,0x00,0xE7};
bool kwp_send_mode_pid(uint8_t u8Mode, uint8_t pid, uint8_t u8PIDBytes) {
    sprintf(tmpStr, "kwp_send_mode %02d, pid %02d", u8Mode, pid);
    DebugPutString(tmpStr);
    uint8_t u8Len = 4 + u8PIDBytes;
    //int i = 0;
    if (u8PIDBytes > 1)
    {
        DebugPutString("\r\n Unsupported number of PIDBytes");
        return false;
    }
    if (g_ODB2Protocol == ODB2_PROTOCOL_ISO9141) {
        KWP_ISO_TX_Buf[0] = 0x68;
				KWP_ISO_TX_Buf[1] = 0x6A;
				KWP_ISO_TX_Buf[2] = 0xF1;
				KWP_ISO_TX_Buf[3] = u8Mode;
    } else if (g_ODB2Protocol == ODB2_PROTOCOL_KWP2000_FAST || g_ODB2Protocol == ODB2_PROTOCOL_KWP2000_SLOW){
				KWP_ISO_TX_Buf[0] = 0xC1 + u8PIDBytes;
        KWP_ISO_TX_Buf[1] = 0x33;
				KWP_ISO_TX_Buf[2] = 0xF1;
				KWP_ISO_TX_Buf[3] = u8Mode;
		}else if (g_ODB2Protocol == ODB2_PROTOCOL_TOYOTA_KLINE) {
        KWP_ISO_TX_Buf[0] = 0x81 + u8PIDBytes;
        KWP_ISO_TX_Buf[1] = 0x13;
				KWP_ISO_TX_Buf[2] = 0xF0;
				if(u8Mode==0x01) {
					KWP_ISO_TX_Buf[3] = u8Mode+0x20;
				} 
    }
    
    if (u8PIDBytes > 0)
        KWP_ISO_TX_Buf[4] = pid;
    // CRC byte
    KWP_ISO_TX_Buf[u8Len] = kwp_CalculateCRC(KWP_ISO_TX_Buf, u8Len);
    u8Len++;// plus one CRC byte
		for(int i=0;i<u8Len;i++){
			HAL_UART_Transmit(&UART_ISO,&KWP_ISO_TX_Buf[i], 1,0xff);
			HAL_Delay(4);
		}
		//
    //if (HAL_UART_Transmit(&UART_ISO,KWP_ISO_TX_Buf, u8Len,0xff) == HAL_OK)
    //    return true;
    //else return false;
		return true;
}
bool toyota_send(uint8_t* pu8Data, uint8_t u8Len){
    HAL_StatusTypeDef ret;
	KWP_ISO_TX_Buf[0] = 0x80 + u8Len;
  KWP_ISO_TX_Buf[1] = 0x13;
	KWP_ISO_TX_Buf[2] = 0xF0;
	for(int i=0; i<u8Len;i++){
		KWP_ISO_TX_Buf[3+i] = pu8Data[i];
	}
	KWP_ISO_TX_Buf[3+u8Len] = kwp_CalculateCRC(KWP_ISO_TX_Buf,u8Len+3);
	for(int i=0;i<u8Len+4;i++){
			ret = HAL_UART_Transmit(&UART_ISO,&KWP_ISO_TX_Buf[i], 1,0xff);
			HAL_Delay(5);
	}
	if(ret == HAL_OK) return true;
	else return false;
}

int ODB2_Toyota_ProcessBleRequest(void){
	uint8_t u8PIDBytes = 0;
  uint8_t u8Mode = g_ODB2Msg.u8Mode;
  uint8_t u8PID = g_ODB2Msg.u8PID;
  uint32_t u32Len = 0;
  uint32_t u32TotalLen = 0;
  uint32_t u32Idx = 0;
	uint32_t u32Start;
	uint8_t pu8TxBuff[12];
	uint8_t u8CopiedBytes;
	g_ODB2Msg.u8RespLen = 0;
	bool ret = false; 
	if (g_ODB2Msg.u8Mode == 0x03){
		DebugPutString("\r\nODB2_Toyota_ProcessBleRequest mode 03: ");
		//osDelay(100);
		DebugPutString("\r\nChassis check");
		ret = ODB2_Toyota_Fast_Init_Custom(35,15,0x29,UART_KLINE_BAUDRATE_9600);
		if(!ret) {
			osDelay(1);
			ret = ODB2_Toyota_Fast_Init_Custom(25,25,0x29,UART_KLINE_BAUDRATE_10400);
		}
		if(ret){
			pu8TxBuff[0] = 0x13;
			u32Len = ODB_Toyota_SendReceive(0x29,pu8TxBuff,1,KWP_ISO_RX_Buf);
			if(u32Len>3){
				dumpHex(KWP_ISO_RX_Buf, u32Len);
				ToyotaDtcTxMessageProcess_noQuantity(0x29,KWP_ISO_RX_Buf,u32Len);
			}
			sprintf(tmpStr,"\r\nChassis g_ODB2Msg.u8RespLen = %u ",g_ODB2Msg.u8RespLen);
			DebugPutString(tmpStr);
		} else {
			DebugPutString("\r\nChassis is not response");
		}
		
		osDelay(1);
		DebugPutString("\r\nElectrical Body check");
		ret = ODB2_Toyota_Fast_Init_Custom(25,25,0x58,UART_KLINE_BAUDRATE_10400);
		if(!ret){
			osDelay(1);
			ret = ODB2_Toyota_Fast_Init_Custom(35,15,0x58,UART_KLINE_BAUDRATE_9600);
		}
		if(ret){ //electrical body
			pu8TxBuff[0] = 0x13;
			pu8TxBuff[1] = 0x82;
			u32Len = ODB_Toyota_SendReceive(0x58,pu8TxBuff,2,KWP_ISO_RX_Buf);
			if(u32Len>3){
				dumpHex(KWP_ISO_RX_Buf, u32Len);
				ToyotaDtcTxMessageProcess(0x58,KWP_ISO_RX_Buf,u32Len);
			}
			sprintf(tmpStr,"\r\nElectrical g_ODB2Msg.u8RespLen = %u ",g_ODB2Msg.u8RespLen);
			DebugPutString(tmpStr);
		} else {
			DebugPutString("\r\nElectrical Body is not response");
		}
		osDelay(1);

		DebugPutString("\r\nEngine check");
		ret = ODB2_Toyota_Fast_Init_Custom(25,25,0x13,UART_KLINE_BAUDRATE_10400);
		if(!ret){
			ret = ODB2_Toyota_Fast_Init_Custom(35,15,0x13,UART_KLINE_BAUDRATE_9600);
		}
		if(ret){ //engine
			pu8TxBuff[0] = 0x13; //mode 03
			u32Len = ODB_Toyota_SendReceive(0x13,pu8TxBuff,1,KWP_ISO_RX_Buf);
			if(u32Len>3){
				dumpHex(KWP_ISO_RX_Buf, u32Len);
				ToyotaDtcTxMessageProcess(0x13,KWP_ISO_RX_Buf,u32Len);
			}
			sprintf(tmpStr,"\r\nEngine g_ODB2Msg.u8RespLen = %u ",g_ODB2Msg.u8RespLen);
			DebugPutString(tmpStr);
		} else {
			DebugPutString("\r\nEngine is not response");
		}
		
		g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
		return 0;
	} else {
		u8PIDBytes = 1;
    if (Toyota_send_mode_pid(u8Mode, u8PID, u8PIDBytes)){
			u32Len = 0;
			if(HAL_UART_Receive(&UART_ISO, &KWP_ISO_RX_Buf[u32Len], 1, 0xFF)==HAL_OK){
				u32Len = 1;
				while (HAL_UART_Receive(&UART_ISO, &KWP_ISO_RX_Buf[u32Len], 1, 0xFF)==HAL_OK){
					u32Len++;
          if (u32Len > 127) break;
				}
			}
			if (u32Len > 3) {
				DebugPutString("\r\nODB2_Toyota_ProcessBleRequest: ");
				dumpHex(KWP_ISO_RX_Buf, u32Len);
				for(u32Start = 0; u32Start<u32Len; u32Start++){
					if(KWP_ISO_RX_Buf[u32Start]==0x13) {
					break;
					}
				}
				if(u32Start==u32Len) return -1;
				u32Start -= 2;
				if (check_kwp_full_crc(&KWP_ISO_RX_Buf[u32Start], u32Len-u32Start)) {
					if (KWP_ISO_RX_Buf[4+u32Start] == g_ODB2Msg.u8PID) {
						g_ODB2Msg.u8RespLen = (u32Len-4-u32Start) & 0xFF;
						sprintf(tmpStr,"\r\n g_ODB2Msg.u8RespLen = %u", g_ODB2Msg.u8RespLen);
						DebugPutString(tmpStr);
            memcpy(g_ODB2Msg.pu8Data, &KWP_ISO_RX_Buf[3+u32Start], g_ODB2Msg.u8RespLen);
            g_ODB2Msg.u8Status = ODB2_STATUS_PROCESS_END;
            return 0;
          } else {
							DebugPutString("\r\nReceive Wrong PID");
              return -1;
          }
        }
				DebugPutString("\r\nReceiver wrong crc");
				return -1;
			} else {
				DebugPutString("\r\nReceive no data");
				bKWP_Alive = false;
				return -1;
			}
			
		}
	}
	return 0;
}

uint8_t ODB_Toyota_SendReceive(uint8_t ECU_Phisical_Address,uint8_t* pu8TxBuff, uint8_t u8TxLen, uint8_t* pu8RxBuff){
	KWP_ISO_TX_Buf[0] = 0x80 + u8TxLen;
	KWP_ISO_TX_Buf[1] = ECU_Phisical_Address;
	KWP_ISO_TX_Buf[2] = 0xF0;
	for(int i=0; i<u8TxLen;i++){
		KWP_ISO_TX_Buf[3+i] = pu8TxBuff[i];
	}
	KWP_ISO_TX_Buf[3+u8TxLen] = kwp_CalculateCRC(KWP_ISO_TX_Buf,u8TxLen+3);
	for(int i=0;i<u8TxLen+4;i++){
			HAL_UART_Transmit(&UART_ISO,&KWP_ISO_TX_Buf[i], 1,0xff);
			HAL_Delay(4);
	}
	uint8_t u8RxLen = 0;
	if(HAL_UART_Receive(&UART_ISO, &pu8RxBuff[u8RxLen], 1, 0xFF)==HAL_OK){
		u8RxLen=1;
		while(HAL_UART_Receive(&UART_ISO, &pu8RxBuff[u8RxLen], 1, 0xFF)==HAL_OK){
			if(u8RxLen++>126) break;
		}
	}
	return u8RxLen;
}

bool ODB2_Toyota_Fast_Init_Custom(uint8_t lowTime, uint8_t highTime, uint8_t ECU_Phisical_Address, int baudrate){
	memset(KWP_ISO_RX_Buf,0,128);
	uint8_t u8InitRequest = 0x81;
	uint8_t u8Start;
	ISO_UART_DISABLE();
  GPIO_KlinePinInit();
    // Wakeup Pattern
  ISO_TX_RESET();
  HAL_Delay(lowTime);
  ISO_TX_SET();
  HAL_Delay(highTime);
  ISO_UART_ENABLE(baudrate);
	uint8_t u8Len = ODB_Toyota_SendReceive(ECU_Phisical_Address,&u8InitRequest,1,KWP_ISO_RX_Buf);
	if(u8Len>=7){
		dumpHex(KWP_ISO_RX_Buf, u8Len);
		for(u8Start = 2; u8Start<u8Len; u8Start++){
			if(KWP_ISO_RX_Buf[u8Start]==ECU_Phisical_Address) {
				break;
			}
		}
		if(u8Len-u8Start<4) {
			DebugPutString("\r\nError u8Len-u8Start<4");
			return false;
		}
		u8Start-=2;
		if(KWP_ISO_RX_Buf[u8Start+3]!=0xC1) {
			DebugPutString("\r\nError 0xC1");
			return false;
		}
		return true;
	}
	return false;
}

void GPIO_KlinePinInit(void){
	GPIO_InitTypeDef GPIO_InitStruct;
  HAL_GPIO_WritePin(ISO_UART_GPIO,ISO_UART_PIN_TX, GPIO_PIN_SET);
  GPIO_InitStruct.Pin=ISO_UART_PIN_TX;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(ISO_UART_GPIO, &GPIO_InitStruct);
}

void ToyotaDtcTxMessageProcess(uint8_t ECU_Phisical_Address, uint8_t* pu8RxBuff, uint8_t u8RxLen){
	uint8_t u8Start;
	uint8_t u8CopiedBytes;
	//dumpHex(KWP_ISO_RX_Buf, u8Start);
	for(u8Start = 0; u8Start<u8RxLen; u8Start++){
		if(pu8RxBuff[u8Start]==ECU_Phisical_Address) {
			break;
		}
	}
	if(u8Start==u8RxLen){
	} else {
		u8Start -= 2;
		if (check_kwp_full_crc(&KWP_ISO_RX_Buf[u8Start], u8RxLen-u8Start)){
			if(KWP_ISO_RX_Buf[u8Start+3]==0x53){
				u8CopiedBytes = pu8RxBuff[u8Start+4]*2;
				memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], &pu8RxBuff[u8Start+5], u8CopiedBytes);
				g_ODB2Msg.u8RespLen+=u8CopiedBytes;
			} else {
				DebugPutString("\r\nNo DTC");
			}
		} else {
			DebugPutString("\r\nReceive Wrong CRC");
		}
	}
}
void ToyotaDtcTxMessageProcess_noQuantity(uint8_t ECU_Phisical_Address, uint8_t* pu8RxBuff, uint8_t u8RxLen){
	uint8_t u8Start;
	uint8_t u8CopiedBytes;
	//dumpHex(KWP_ISO_RX_Buf, u8Start);
	for(u8Start = 0; u8Start<u8RxLen; u8Start++){
		if(pu8RxBuff[u8Start]==ECU_Phisical_Address) {
			break;
		}
	}
	if(u8Start==u8RxLen){
	} else {
		u8Start -= 2;
		if (check_kwp_full_crc(&KWP_ISO_RX_Buf[u8Start], u8RxLen-u8Start)){
			if(KWP_ISO_RX_Buf[u8Start+3]==0x53){
				u8CopiedBytes = u8RxLen - 5;
				memcpy(&g_ODB2Msg.pu8Data[g_ODB2Msg.u8RespLen], &pu8RxBuff[u8Start+4], u8CopiedBytes);
				g_ODB2Msg.u8RespLen+=u8CopiedBytes;
			} else {
				DebugPutString("\r\nNo DTC");
			}
		} else {
			DebugPutString("\r\nReceive Wrong CRC");
		}
	}
}
void KWP_Reinit(void){
	DebugPutString("\r\nKWP_Reinit");
	if (g_ODB2Protocol == ODB2_PROTOCOL_KWP2000_FAST) {
		if(ODB2_Fast_Init()) bKWP_Alive = true;
	} else if(g_ODB2Protocol == ODB2_PROTOCOL_TOYOTA_KLINE){
		if (ODB2_Toyota_Fast_Init_Custom(25,25,0x13,UART_ISO9141_BAUDRATE)) {
			bKWP_Alive = true;	return;
		}
		if (ODB2_Toyota_Fast_Init_Custom(35,15,0x13,UART_KLINE_BAUDRATE_9600)) {
			bKWP_Alive = true;	return;
		}
	} else {
		if(ODB2_Slow_Init()){
			if(g_ODB2Protocol == ODB2_PROTOCOL_ISO9141){
				if((KB1 == 0x08 && KB2 == 0x08) || (KB1== 0x94 && KB2 == 0x94))
					bKWP_Alive = true;
			} else {
				if (KB1 == 0x8F || KB2 == 0x8F) bKWP_Alive = true;
			}
		}
	}
}

bool	Toyota_send_mode_pid(uint8_t u8Mode, uint8_t pid, uint8_t u8PIDBytes){
	HAL_StatusTypeDef ret;
	sprintf(tmpStr, "Toyota send mode pid %02d, pid %02d", u8Mode, pid);
	DebugPutString(tmpStr);
	uint8_t u8Len = 4 + u8PIDBytes;
	KWP_ISO_TX_Buf[0] = 0x81 + u8PIDBytes;
	KWP_ISO_TX_Buf[1] = 0x13;
	KWP_ISO_TX_Buf[2] = 0xF0;
	if(u8Mode==0x01) {
		KWP_ISO_TX_Buf[3] = u8Mode;//+0x20;
	}
	if(g_ToyotaMode01Increase) KWP_ISO_TX_Buf[3]+=0x20;
		
	if (u8PIDBytes > 0)
        KWP_ISO_TX_Buf[4] = pid;
	KWP_ISO_TX_Buf[u8Len] = kwp_CalculateCRC(KWP_ISO_TX_Buf, u8Len);
  u8Len++;// plus one CRC byte
	for(int i=0;i<u8Len;i++){
		ret = HAL_UART_Transmit(&UART_ISO,&KWP_ISO_TX_Buf[i], 1,0xff);
		HAL_Delay(5);
	}
	if(ret == HAL_OK) return true;
	else return false;
}
bool checkToyotaMode01(void) {
	bool ret = false;
	uint32_t u32Len;
	uint32_t u32Start;
	uint8_t pu8Tx[5];	
	
	osDelay(1000);
	
	pu8Tx[0] = 0x01;
	pu8Tx[1] = 0x00;
	
	if(toyota_send(pu8Tx,2)){
		u32Len = 0;
		if(HAL_UART_Receive(&UART_ISO, &KWP_ISO_RX_Buf[u32Len], 1, 0xFF)==HAL_OK){
			u32Len = 1;
			while (HAL_UART_Receive(&UART_ISO, &KWP_ISO_RX_Buf[u32Len], 1, 0xFF)==HAL_OK){
				u32Len++;
				if (u32Len > 127) break;
			}
		}
		if(u32Len > 3) {
			DebugPutString("\r\nOBD2_toyota check 0x01 mode:");
			dumpHex(KWP_ISO_RX_Buf, u32Len);
			for(u32Start = 0; u32Start<u32Len; u32Start++){
					if(KWP_ISO_RX_Buf[u32Start]==0x13) {
					break;
					}
			}
			if(u32Start!=u32Len){
				u32Start -= 2;
				if(check_kwp_full_crc(&KWP_ISO_RX_Buf[u32Start], u32Len-u32Start)){
					if(KWP_ISO_RX_Buf[3+u32Start] == 0x41 && KWP_ISO_RX_Buf[4+u32Start] == 0x00 
							&& (u32Len-u32Start) >= 9 
							&& (KWP_ISO_RX_Buf[5+u32Start] || KWP_ISO_RX_Buf[6+u32Start] || KWP_ISO_RX_Buf[7+u32Start] || KWP_ISO_RX_Buf[8+u32Start]) ){
							DebugPutString("\r\nOBD2 toyota mode 01 normal");
							g_ToyotaMode01Increase = false;
							return true;
					}
				}
			}
		}
	}
	
	osDelay(1000);
	
	pu8Tx[0] = 0x21;
	pu8Tx[1] = 0x00;
	
	if(toyota_send(pu8Tx,2)){
		u32Len = 0;
		if(HAL_UART_Receive(&UART_ISO, &KWP_ISO_RX_Buf[u32Len], 1, 0xFF)==HAL_OK){
			u32Len = 1;
			while (HAL_UART_Receive(&UART_ISO, &KWP_ISO_RX_Buf[u32Len], 1, 0xFF)==HAL_OK){
				u32Len++;
				if (u32Len > 127) break;
			}
		}
		if(u32Len > 3) {
			DebugPutString("\r\nOBD2_toyota check 0x01 mode:");
			dumpHex(KWP_ISO_RX_Buf, u32Len);
			for(u32Start = 0; u32Start<u32Len; u32Start++){
					if(KWP_ISO_RX_Buf[u32Start]==0x13) {
					break;
					}
			}
			if(u32Start!=u32Len){
				u32Start -= 2;
				if(check_kwp_full_crc(&KWP_ISO_RX_Buf[u32Start], u32Len-u32Start)){
					if(KWP_ISO_RX_Buf[3+u32Start] == 0x61 && KWP_ISO_RX_Buf[4+u32Start] == 0x00 
							&& (u32Len-u32Start) >= 9 
							&& (KWP_ISO_RX_Buf[5+u32Start] || KWP_ISO_RX_Buf[6+u32Start] || KWP_ISO_RX_Buf[7+u32Start] || KWP_ISO_RX_Buf[8+u32Start]) ){
							DebugPutString("\r\nOBD2 toyota mode 01 increase 0x20");
							g_ToyotaMode01Increase = true;
							return true;
					}
				}
			}
		}
	}
	
	return ret;
}

