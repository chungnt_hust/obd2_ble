How to build iVG1.1.2_STM32F303

Step 1: Go to tools folder, extract KeilC V5.17.zip, install tool
Step 2: Crack for profesional version
        Note: Target mdk arm (not c51), Prof Developers Kit/ReadView MDK. Paste CID into CID textbox.

Step 3: Open project with iVG1.1.2_STM32F303\MDK-ARM\iVG1.2ARM.uvprojx
Note: if can't load  stm32f3 series then download package in https://www.keil.com/dd2/pack/
      select STMicroelectronics STM32F3 Series Device Support and Examples
      install this package

Step 4: Build

Solution for problem
Error: iVG1.2ARM\iVG1.2ARM: error: L6050U: The code size of this image (49636 bytes) exceeds the maximum allowed for this version of the linker.