/**
 * @file j1850.h
 * @brief J1850 VPW & PWM for stm32f1 (odbi adapter)
 * @date May 04, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */
#ifndef J1850_H
#define J1850_H
//
// SAE J1850 timeouts definition
//
#include "stm32f3xx_hal.h"
#include "cmsis_os.h"
#include "utils.h"
#include "protocol.h"

#define ENABLE_PWM() 			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET)
#define ENABLE_VPW() 			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET)
#define SAE_OUT_HIGH()		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET)
#define SAE_OUT_LOW()			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET)

#define J1850_VPW_TRX_TIMEOUT 200 // miliseconds
#define J1850_PWM_TRX_TIMEOUT 300 // miliseconds


typedef enum _J1850_PROTOCOL {
    J1850_PROTOCOL_NONE = 0,
    J1850_PROTOCOL_VPW,
    J1850_PROTOCOL_PWM
} J1850_PROTOCOL;

enum J1850Limits {
    J1850_BYTES_MIN =  1,
    J1850_BYTES_MAX = 12,
    OBD2_BYTES_MIN  =  5, // 3(header) + 1(data) + 1(checksum)
    OBD2_BYTES_MAX  = 11  // 3(header) + 7(data) + 1(checksum)
};
    
// J1850 Timeouts
//
enum J1850Timeouts {
    P2_J1850 = 100 // in msec
};

// VPW Timeouts, in usec
//
enum VpwTimeouts {
    TV1_TX_NOM  =   64,
    TV2_TX_NOM  =  128,
    TV3_TX_NOM  =  200, 
    TV4_TX_MIN  =  261, 
    TV6_TX_MIN  =  280,
    TV5_TX_NOM  =  300,
    TV5_TX_MAX  =  5000,
    TV6_TX_NOM  =  300,
    TV1_TX_ADJ  =   64,
    TV2_TX_ADJ  =  128,
    TV1_RX_MIN  =   34,
    TV2_RX_MAX  =  163,
    TV3_RX_MIN  =  163,
    TV3_RX_MAX  =  239,
    TV5_RX_MIN  =  239,
    TV6_RX_MIN  =  280,
    VPW_RX_MID  =   96
};


#define J1850_MAX_RX_FRAME_COUNT 4

typedef enum
{
    BUS_IDLING,
    BUS_READING,
    BUS_WRITING,
    BUS_BUSY
} SAE_BUS_STATUS;

extern SAE_BUS_STATUS CPU_STATUS;

typedef enum
{
    SAE_TRANSMIT_OVER,
    SAE_TRANSMIT_CONTINUE,
    SAE_TRANSMIT_STOP
} SAE_TRANSMIT_FRAME_STATUS;

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
#define TxTIMER htim4
#define RxTIMER htim3
#define EOFTIMER htim4


//extern uint8_t j1850_RX_Buf[512];

extern volatile SAE_TRANSMIT_FRAME_STATUS CHECK_EOF_BIT;
/**
 * @brief j1850_crc calculate crc for sae j1850
 * @param msg_buf
 * @param nbytes
 * @return
 */
uint8_t j1850_crc(uint8_t *msg_buf, int8_t nbytes);
/**
 * @brief HAL_TIM_OC_DelayElapsedCallback
 * @param htim
 */
void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim);
/**
 * @brief HAL_TIM_IC_CaptureCallback
 * @param htim
 */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim);

/**
 * @brief j1850_init
 * @return J1850_PROTOCOL_NONE if inited faile
 *         J1850_PROTOCOL_VPW if vpw inited OK
 *         J1850_PROTOCOL_PWM if pwm inited OK
 */
int j1850_init(void);
/**
 * @brief j1850_vpw_init
 * @return 1 if success, 0 if failed
 */
int j1850_vpw_init(void);
/**
 * @brief j1850_pwm_init
 * @return 1 if sucess, 0 if failed
 */
int j1850_pwm_init(void);

/**
 * @brief j1850_check_recv_status
 */
void j1850_check_recv_status(void);

int ODB2_j1850_ProcessBleRequest(void);

void ODB2_j1850_Transmit(uint8_t u8Mode, uint8_t u8Pid, uint8_t u8PidBytes);
uint16_t ODB2_j1850_RxProcessTask(void);

void j1850_brutal_init(void);
uint16_t OBD2_j1850_brutal_transmit(uint8_t u8Mode, uint8_t u8Pid, uint8_t u8PidBytes);
//uint16_t ODB2_j1850_brutal_RxProcessTask(void);

#endif // J1850_H
