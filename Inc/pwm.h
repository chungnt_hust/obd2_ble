#ifndef __SAE_PWM_H
#define __SAE_PWM_H

#include "stm32f3xx_hal.h"
#include "cmsis_os.h"
#include "debug.h"
#include "j1850.h"


//cac ham truyen 
#define Tp1			8
#define Tp2			16
#define Tp3			24
#define Tp4			48
#define Tp5			72
#define Tp6			96
#define Tp7			32
#define Tp8			40
#define Tp9			120
#define Tp10		48
#define Tp11		6

void SAE_PWM_TxProcessTask(void);
void SAE_PWM_Transmit(uint8_t mode, uint8_t pid, uint8_t u8PidBytes);
void PWM_CalPulseToSAEBus(uint8_t* pu8Data,uint16_t u16Leng);
void PWM_CalPulseByte(uint8_t u8Data);
void PWM_BitToPulse(uint8_t BitValue);
//cac ham nhan
// Index of mode and pid bytes in sae vpw sent data
#define SAE_PWM_MODE_IDX 3
#define SAE_PWM_PID_IDX 4

#define MIN_TP1			1
#define MAX_TP1			10
#define MIN_TP2			12
#define MAX_TP2			16		
#define MIN_TP3			17		
#define MAX_TP3			27		
#define MIN_TP4     44
#define MAX_TP4     51
#define MIN_TP5			70		
#define MAX_TP5						//NA
#define MIN_TP7			22		
#define MAX_TP7			35		
#define MIN_TP10		45		
#define MAX_TP10		52	

#define PWM_EOF_BIT_VAL 0xFF
#define PWM_SOF_BIT_VAL 0xFE
#define PWM_EOD_BIT_VAL 0xFD
#define PWM_ERR_BIT_VAL 0xFC // unknown



void SAE_PWM_StopReceive(void);
void SAE_PWM_StartReceive(void);
uint16_t SAE_PWM_RxProcessTask(void);
void SAE_PWM_SetPIDMode(uint8_t mode, uint8_t pid);
static uint8_t PWM_CalBitValue(uint16_t u16Pulse);
static uint8_t PWM_BitsToByte(uint8_t* pu8Value);
static inline uint8_t PWM_CalBitValue2(uint16_t u16Pulse1, uint16_t u16Pulse2);

extern uint8_t* pu8PWMRxByteValue;
extern uint16_t* pu16PWMRxBitPulse;
extern uint8_t* pu8PWMRxBitValue;


#endif
