/**
 * @file debug.h
 * @brief debug task via uart for stm32f1 (odbi adapter)
 * @date April 20, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */

/* USER CODE BEGIN Includes */
#include "stm32f3xx_hal.h"
#include "cmsis_os.h"
#include "string.h"

/* USER CODE END Includes */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DEBUG_H
#define __DEBUG_H

/* User definition */
#define UART_DEBUG huart2
#define DEBUG_UART_INSTANCE USART2
#define DEBUG_RX_BUF_SIZE 16
#define DEBUG_TX_BUF_SIZE 16

#define DEBUG(buffer,msg, ...) do {\
													snprintf(buffer,sizeof(buffer),msg,##__VA_ARGS__);\
													DebugPutString(buffer);\
												}while(0)

/* External Defintion */
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern char debugRx[];
extern uint8_t count_debugRx;
extern uint8_t request_debug;

/* User Private functions */
void DebugPutString(char *str);
void DebugPutChar(char c);
HAL_StatusTypeDef DebugReadChar(char* c);
void DebugProcessReceiveByte(uint8_t c);
void DebugProcessTask(void);
void inline DebugEndLine(void);
#endif // __DEBUG_H
