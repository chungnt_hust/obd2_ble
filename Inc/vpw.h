#ifndef __SAE_VPW_H
#define __SAE_VPW_H

#include "stm32f3xx_hal.h"
#include "cmsis_os.h"
#include "debug.h"
#include "j1850.h"

#define MIN_SOF 		163
#define MAX_SOF			239
#define MIN_SHORT		34
#define MAX_SHORT		96
#define MIN_LONG		96
#define MAX_LONG		163
#define	MIN_EOF			239
#define LONG 				128
#define SHORT 			64
#define StartOF			200
#define EndOF				280

#define VPW_SHORT_PULSE			64
#define VPW_LONG_PULSE			128
#define VPW_SOF							200

// Index of mode and pid bytes in sae vpw sent data
#define SAE_VPW_MODE_IDX 3
#define SAE_VPW_PID_IDX 4

//cac ham truyen
void SAE_VPW_TxProcessTask(void);
void SAE_VPW_Transmit(uint8_t mode, uint8_t pid, uint8_t u8PidBytes);
void SAE_VPW_SetPIDMode(uint8_t mode, uint8_t pid);
static void CalPulseToSAEBus(uint8_t* pu8Data,uint16_t u16Leng);//do dai toi da cua ban tin la 12 bytes
static void CalPulseByte(uint8_t u8Data);
static uint8_t BitToPulse(uint8_t u8BitCheck, int index);

//cac ham nhan
void SAE_VPW_StopReceive(void);
void SAE_VPW_StartReceive(void);
//void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim);
uint16_t SAE_VPW_RxProcessTask(void);
static uint8_t CheckEvenBit(uint16_t u16Pulse);
static uint8_t CheckOddBit(uint16_t u16Pulse);
static uint8_t BitsToByte(uint8_t* pu8Value);

extern uint8_t* pu8VPWRxByteValue;
extern uint16_t* pu16RxBitPulse;
extern uint8_t* pu8RxBitValue;

typedef enum __VPW_SEND {
	VPW_SEND_OK,
	VPW_SEND_NOK,
	VPW_SEND_OVER_WRITE
} VPW_SEND;

typedef enum _VPW_RECEIVE {
	VPW_RECEIVE_OK,
	VPW_RECEIVE_NOTHING,
	VPW_RECEIVE_WRONG_CRC,
	VPW_RECEIVE_NOK
} VPW_RECEIVE;

uint16_t vpw_calculate_pulse_with_sof(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf);
uint16_t vpw_calculate_pulse(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf);
void vpw_calculate_pulse_byte(uint8_t u8ByteData, uint8_t *pu8PulseBuf);
void vpw_bit_to_pulse(uint8_t u8BitCheck, int index, uint8_t* pu8PulseBuf);

VPW_SEND vpw_send(uint8_t* pu8PusleBuf, uint16_t u16Leng);
VPW_SEND vpw_send_command(uint8_t u8Mode, uint8_t u8PID);

VPW_RECEIVE vpw_read_pulse(void);
void vpw_read_byte(void);
uint8_t vpw_pulse_to_byte(uint16_t* pu8PulseBuf);
void vpw_read_inform_to_debug(void);

void brutalVpwInit(void);
void brutalVpwDeinit(void);

VPW_SEND SAE_VPW_brutal_transmit(uint8_t mode, uint8_t pid, uint8_t u8PidBytes);
uint16_t vpw_rx_byte_number(void);

#endif
