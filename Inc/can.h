/**
 * @file can.h
 * @brief can task via uart for stm32f1 (odbi adapter)
 * @date April 20, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */

#include "stm32f3xx_hal.h"
#include "cmsis_os.h"
#include "stdbool.h"
#ifndef CAN_H
#define CAN_H

/* CAN Definition */
#define CAN_HIGHSPEED 1
#define CAN_LOWSPEED 0

#define CAN_STANDARD 1
#define CAN_EXTENDED 2
#define CAN_TOYOTA   3
#define CAN_REQUEST_BURNIN	4

#define CAN_ODB_STDID 0x700
#define CAN_ODB_EXTID 0x18DB0000

#define ODB_STD_RESPONSE_ID 0x7E0
#define ODB_EXT_RESPONSE_ID 0x1800F100
#define ODB_STD_RESPONSE_MASK 0x7F0
#define ODB_EXT_RESPONSE_MASK 0x1800F100

//#define CAN_FILTER_ANY_BANK_NUMBER 12
//#define CAN_ANY_FILTER_NUMBER  0
//#define CAN_ANY_FILTER_SCALE CAN_FILTERSCALE_16BIT
//#define CAN_ANY_FILTER_ID_HIGH ((0X7D0 << 5) | (0x01 << 4) | 0x06)
//#define CAN_ANY_FILTER_ID_LOW ((0X7D0 << 5) | (0x01 << 4) | 0x06)
//#define CAN_ANY_FILTER_MASK_HIGH ((0X7D0 << 5) | (0x01 << 4) | 0x06)
//#define CAN_ANY_FILTER_MASK_LOW ((0X7D0 << 5) | (0x01 << 4) | 0x06)

#define CAN_FILTER_ANY_BANK_NUMBER 12
#define CAN_ANY_FILTER_NUMBER  0
#define CAN_ANY_FILTER_SCALE CAN_FILTERSCALE_32BIT
#define CAN_ANY_FILTER_ID_HIGH 0x0000
#define CAN_ANY_FILTER_ID_LOW 0x0000
#define CAN_ANY_FILTER_MASK_HIGH 0x0000
#define CAN_ANY_FILTER_MASK_LOW 0x0000

#define CAN_11BIT_FILTER_BANK_NUMBER 14
#define CAN_11BIT_FILTER_NUMBER  0
#define CAN_11BIT_FILTER_SCALE 				CAN_FILTERSCALE_16BIT

#if 1
#define CAN_11BIT_FILTER_ID_HIGH 			(ODB_STD_RESPONSE_ID << 5)	//(ODB_STD_RESPONSE_ID << 5)
#define CAN_11BIT_FILTER_ID_LOW 			(ODB_STD_RESPONSE_ID << 5)	//(ODB_STD_RESPONSE_ID << 5)//(ODB_STD_RESPONSE_MASK << 5)
#define CAN_11BIT_FILTER_MASK_HIGH 		(ODB_STD_RESPONSE_MASK << 5)//(ODB_STD_RESPONSE_ID << 5)
#define CAN_11BIT_FILTER_MASK_LOW 		(ODB_STD_RESPONSE_MASK << 5)//(ODB_STD_RESPONSE_MASK << 5)//(ODB_STD_RESPONSE_MASK << 5)
#else 
#define CAN_11BIT_FILTER_ID_HIGH 			(ODB_STD_RESPONSE_ID << 5)
#define CAN_11BIT_FILTER_ID_LOW 			(ODB_STD_RESPONSE_MASK << 5)
#define CAN_11BIT_FILTER_MASK_HIGH 		(ODB_STD_RESPONSE_ID << 5)
#define CAN_11BIT_FILTER_MASK_LOW 		(ODB_STD_RESPONSE_MASK << 5)
#endif

#define CAN_29BIT_FILTER_BANK_NUMBER 14
#define CAN_29BIT_FILTER_NUMBER  0
#define CAN_29BIT_FILTER_SCALE 				CAN_FILTERSCALE_32BIT
#define CAN_29BIT_FILTER_ID_HIGH 			(((ODB_EXT_RESPONSE_ID<<3)>>16) & 0xFFFF)
#define CAN_29BIT_FILTER_ID_LOW 			(((ODB_EXT_RESPONSE_ID <<3) | CAN_ID_EXT) & 0xFFFF)
#define CAN_29BIT_FILTER_MASK_HIGH 		(((ODB_EXT_RESPONSE_MASK <<3)>>16) & 0xFFFF)
#define CAN_29BIT_FILTER_MASK_LOW 		((ODB_EXT_RESPONSE_MASK << 3| CAN_ID_EXT) & 0xFFFF)


#define ISO_TP_TYPE_SINGLE 0
#define ISO_TP_TYPE_FIRST 1
#define ISO_TP_TYPE_CONSEC 2
#define ISO_TP_TYPE_CTRL_FLOW 3


/* External functions */
//extern void Error_Handler(void);
/* ODB2 CAN's Prototype function */
int ODB2_CAN_Transmit(uint32_t stdID, uint8_t u8Mode, uint8_t u8PID, uint32_t u32IDE);
int ODB2_CAN_Init(int speed);
void ODB2_CAN_Setup_Speed(int speed, uint32_t u32Can_Mode);
void ODB2_CAN_DeInit(void);
void printCANmsg(CAN_HandleTypeDef can);
void printCANmsg1(CAN_HandleTypeDef can);
void printCANmsgTx(CAN_HandleTypeDef can);
void ODB2_CAN_InitFilters(uint8_t enabled);
int ODB2_CAN_InitFilterAny(uint8_t enabled, uint8_t FIFO_Number);
int ODB2_CAN_InitFilter11bit(uint8_t enabled, uint8_t FIFO_Number);
int ODB2_CAN_InitFilter29bit(uint8_t enabled, uint8_t FIFO_Number);

int ODB2_CAN_ProcessBleRequest(void);
int ODB2_CAN_Send_TP_ISO_FlowControl(uint32_t msgID, uint32_t u32IDE);
int ODB2_TOYOTA_CAN_Init(void);
int ODB2_CAN_TransmitMessage(uint32_t MessageId, uint32_t u32IDE, uint8_t* pu8Data, uint8_t u8Len);
int ODB2_TOYOTA_CAN_ProcessBleRequest(void);
int ODB2_TOYOTA_CAN_ISO_TP_TransmitFlowControlFrame(uint32_t MessageId, uint32_t u32IDE);
int ODB2_TOYOTA_CAN_ISO_TP_Receive(uint32_t MessageId, uint32_t u32IDE, uint8_t u8Mode, uint8_t u8PID, bool bPID, uint8_t* pRxData);
int ODB2_TOYOTA_CAN_ISO_TP_Mode03_Receive(uint32_t MessageId, uint32_t u32IDE, uint8_t u8Mode, uint8_t* pRxData, bool bErrCount);
int ODB2_CAN_Init_Safe(int speed);

#endif // CAN_H
