/**
 * @file ble.h
 * @brief debug task via uart for stm32f1 (odbi adapter)
 * @date April 20, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */

/* USER CODE BEGIN Includes */
#include "stm32f3xx_hal.h"
#include "cmsis_os.h"
#include "utils.h"
/* USER CODE END Includes */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLE_H
#define __BLE_H

/* User definition */
#define UART_BLE huart1
#define BLE_UART_INSTANCE USART1
#define BLE_RX_BUF_SIZE 128
#define BLE_MAX_LOOP 8
#define BLE_MESSAGE_MAX_BYTES 128
#define BLE_MESSAGE_HANDLE_FAIL 0
#define BLE_MESSAGE_HANDLE_OK 1
#define BLE_MESSAGE_HANDLE_CMD 2
#define BLE_MESSAGE_HANDLE_SPECIAL_CMD 3

#define USE_00_TERMINATE 1

#define BOOT_START_ADDRESS 0x08000000
#define		BOOT_SIZE				0x4000
#define CONFIG_START_ADDRESS 0x08004000
#define CONFIG_SIZE 0x800
#define APP_START_ADDRESS 0x08004800
#define JUMP_TO_APP(addr) {__set_MSP(*(__IO uint32_t*) addr); ((pFunction) *(__IO uint32_t*) (addr + 4))();}


#define MCU_POWER_TURN_ON()					//HAL_GPIO_WritePin(MCU_POWER_PORT,MCU_POWER_PIN,GPIO_PIN_RESET)
#define MCU_POWER_TURN_OFF()				//HAL_GPIO_WritePin(MCU_POWER_PORT,MCU_POWER_PIN,GPIO_PIN_SET)
#define MCU_LTE_ON()								//HAL_GPIO_WritePin(MCU_LTE_ON_PORT, MCU_LTE_ON_PIN, GPIO_PIN_SET);
#define MCU_LTE_OFF()								//HAL_GPIO_WritePin(MCU_LTE_ON_PORT, MCU_LTE_ON_PIN, GPIO_PIN_RESET);
#define MCU_LTE_RST()								//HAL_GPIO_WritePin(MCU_LTE_RST_PORT, MCU_LTE_RST_PIN, GPIO_PIN_RESET);

typedef enum _EBLEMODE {
	BLE_MODE_NORMAL = 0,
	BLE_MODE_AUTO_LOOP = 1,
	BLE_MODE_POWER_OFF = 2,
//	BLE_MODE_TRIP_MESUARE = 3,
} BLEMode;

//#define ATZ_RETURN "atz\r\n\r\rELM327\n V2.1\r>\n"//"UConnect0.1\r\n>\n"
#define ATZ_RETURN "atz\r\n\r\rUConnect0.1\r\n>\n"
/* External Defintion */
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern char bleRx[];
extern uint8_t count_bleRx;
extern uint8_t request_ble;
extern bool g_sleep;
extern uint8_t HLK_MAC[6];
extern bool b_MAC;
extern uint8_t SIMCOM_TMP;
void inline BleEndLine(void);

extern float f_TripDistance;	// in meters
extern float f_TripStopTime; // in seconds
extern uint32_t u32TripDistance; // in meters
extern uint32_t u32TripStopTime; // in seconds
extern bool b_TripStart;
extern uint8_t u8Speed;
extern uint32_t u32LastTickRecord;
extern float f_TripDistanceSended;
extern float f_TripStopTimeSended;
extern float f_TripDistanceSaved;

/* User Private functions */
void BlePutString(char *str);
void BlePutChar(char c);
HAL_StatusTypeDef BleReadChar(char* c);
void BleProcessReceiveByte(uint8_t c);
void BleProcessTask(void);
void BleReplyNoData(void);
void BleReplySuccessRequest(void);

int BleMessageHandleFull(uint8_t* pu8Data, uint32_t u32Size);
int BleMessageHandleAT(uint8_t* pu8Data, uint16_t u16Size);
int BleMessageHandlePID(uint8_t* pu8Data, uint16_t u16Size);

int BleMessageHandleF4( uint8_t* pu8Data, uint16_t u16Size );

int BleMessageHandleReset(void);
int BleMessageHandleProtocol_CAN11_500(void);
int BleMessageHandleProtocol_CAN11_250(void);
int BleMessageHandleProtocol_CAN29_500(void);
int BleMessageHandleProtocol_CAN29_250(void);
int BleMessageHandleProtocol_ISO9141(void);
int BleMessageHandleProtocol_KWP2000_FAST(void);
int BleMessageHandleProtocol_KWP2000_SLOW(void);
int BleMessageHandleProtocol_VPW(void);
int BleMessageHandleProtocol_PWM(void);
int BleMessageHandleProtocol_START(void);

void BleResponseSpecialMessageAA(uint8_t cmd);

extern BLEMode g_BleMode;

extern osThreadId canTaskHandle;
extern osThreadId saeTaskHandle;
extern osThreadId kwpTaskHandle;
extern void CanTask(void const * argument);
extern void SaeTask(void const* argument);
extern void KwpTask(void const* argument);

void hexToString(const uint8_t* hexInput,int dlen ,char* strOutput);
int stringToHex(const char* strInput, uint8_t* hexOutput);

void AuthenFirstStep(uint8_t* pu8Data, uint16_t u16Size);
void AuthenSecondStep(uint8_t* pu8Data, uint16_t u16Size);
void PongResponseHandle(uint8_t* pu8Data);
void PingRequestHandle(void);
void NotAuthenResponse(void);
void BleResponseSpecialMessageMode00(uint8_t cmd, uint8_t* buffer, uint8_t dlen);

void BleHandleAddPidToAutoLoop(uint8_t* pu8Data, uint16_t u16Size);
void BleHandleStopAutoLoop(uint8_t* pu8Data, uint16_t u16Size);
void BleHandleStartAutoLoop(uint8_t* pu8Data, uint16_t u16Size);
void BleHandleStopAndResetAutoLoop(uint8_t* pu8Data, uint16_t u16Size);
void BleHandlePowerSave(uint8_t* pu8Data, uint16_t u16Size);

void BleHandleStartTrip(uint8_t* pu8Data, uint16_t u16Size);
void BleHandleStopTrip(uint8_t* pu8Data, uint16_t u16Size);
void BleHandleTripUpdate(uint8_t* pu8Data, uint16_t u16Size);
void BleHandleSetSpeedRatio(uint8_t* pu8Data, uint16_t u16Size);
void BleHandleTripIndex(uint8_t* pu8Data, uint16_t u16Size);


void Ble_FixPutString(char *str, uint16_t len);
#endif // __DEBUG_H
