/**
 * @file protocol.h
 * @brief define const, type of odbii protocols for stm32f1 (odbi adapter)
 * @date April 20, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */
#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "stdint.h"

typedef enum _ISO_TYPE {
    ISO_KWP_NONE,
    KWP_2000_FAST,
    KWP_2000_SLOW,
    ISO9141,
		KWP_FAST_TOYOTA,
} ISO_TYPE;

typedef enum {
    ODB2_PROTOCOL_START = 0x00,
    ODB2_PROTOCOL_CAN11_500 = 0x01, //1
    ODB2_PROTOCOL_CAN11_250 = 0x02, //2
    ODB2_PROTOCOL_CAN29_500 = 0x03, //3
    ODB2_PROTOCOL_CAN29_250 = 0x04, //4
    ODB2_PROTOCOL_ISO9141 = 0x05, //5 ==> 5baud only
    ODB2_PROTOCOL_KWP2000_FAST = 0x06, //6
    ODB2_PROTOCOL_KWP2000_SLOW = 0x07, //7 ==> 5baud
    ODB2_PROTOCOL_VPW = 0x08, //8
    ODB2_PROTOCOL_PWM = 0x09, //9
		ODB2_PROTOCOL_TOYOTA_KLINE = 0x0A, //10
		ODB2_PROTOCOL_TOYOTA_CAN = 0x0B,
		ODB2_BURNIN_TEST,
    ODB2_PROTOCOL_END
} ODB2_PROTOCOL;


typedef enum _ODB2_MESSAGE_STATUS {
    ODB2_STATUS_START,				//0
    ODB2_STATUS_PROCESSING,		//1
    ODB2_STATUS_PROCESSED_ERROR,	//2
    ODB2_STATUS_PROCESS_END,			//3
} ODB2_MESSAGE_STATUS;


typedef struct _ODB2_Message {
    uint8_t u8Status; // ODB2_MESSAGE_STATUS
    uint8_t u8Mode; // mode (standard range: 01->09
    uint8_t u8PID;
    uint8_t u8PidBytes; // 0 for mode e.g 03, 1 for standard, 2 for custom
    uint8_t u8ExtraBytes;
    uint8_t u8ODB2Protocol;
    uint8_t u8RespLen;
    uint8_t pu8Data[64];
} ODB2Message;  

#define RESULT_OK 1
#define RESULT_FAILED 0

#endif // PROTOCOL_H
