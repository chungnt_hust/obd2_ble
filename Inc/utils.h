/**
 * @file utils.h
 * @brief useful functions for stm32f1 (odbi adapter)
 * @date May 04, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */
#ifndef UTILS_H
#define UTILS_H

#include "stm32f3xx_hal.h"
#include "cmsis_os.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


#define LED_ON() HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_SET)
#define LED_OFF() HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, GPIO_PIN_RESET)

void DelayMs(uint32_t period);

extern osTimerId Timer1Handle;
void timerOnceStart(uint32_t timeout);
void Utils_Timer1_Callback  (void const *arg);
void stopTimerOnce(void);
bool isTimeOut(void);
uint8_t toLowerCase(uint8_t x);
uint8_t isValidHex(uint8_t x);
uint8_t validHex2Value(uint8_t x);
uint8_t hextoByte(uint8_t* hex, uint8_t* byte);
void hex2char(uint8_t* pu8Data, uint8_t data);
uint8_t byte2char(uint8_t byte);
void dumpHex(uint8_t* puData, uint32_t u32Len);
#endif // UTILS_H
