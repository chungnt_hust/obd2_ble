/**
 * @file kwp.h
 * @brief KWP2000 & ISO9141 via uart for stm32f1 (odbi adapter)
 * @date May 04, 2017
 * @author Thinh Nguyen - thinhn.ahg@gmail.com
 */
#ifndef KWP_H
#define KWP_H

#include "stm32f3xx_hal.h"
#include "cmsis_os.h"
#include "utils.h"
#include "protocol.h"


/* External Defintion */
extern UART_HandleTypeDef huart3;

/* USART3 init function */
bool ISO_USART3_UART_Init(int  baudrate);

/* User definition */
#define UART_ISO huart3
#define ISO_UART_INSTANCE USART3
#define ISO_RX_BUF_SIZE 16

#define ISO_KWP_DATA_LEN 7
#define ISO_KWP_MAX_LEN 12

/*
 * define timing constants for 5baud init
 *      |    Five Baud        | 10.400 bps
 * Idle | ->Addr(0x33)| W1 | <-0x55 | W2 | <-KB1 | W3 | <-KB2 | W4 | ->~KB2 | W4 | ~Addr
*/
#define W1_MIN 60
#define W1_MAX 300
#define W2_MIN 5
#define W2_MAX 20
#define W3_MIN 0
#define W3_MAX 20
#define W4_MIN 25
#define W4_MAX 50

/*
 * Define time constats for Fast Init
 */
#define FAST_TINIT 25
#define KWP_P2_MIN 25
#define KWP_P2_MAX 50
#define KWP_P3_MIN 55
#define KWP_P3_MAX 4000
#define KWP_TIME_OUT KWP_P3_MAX




/**USART3 GPIO Configuration
PB10     ------> USART3_TX
PB11     ------> USART3_RX
*/
#define ISO_UART_GPIO GPIOB
#define ISO_UART_PIN_RX GPIO_PIN_11
#define ISO_UART_PIN_TX GPIO_PIN_10

#define ISO_UART_DISABLE() HAL_UART_DeInit(&UART_ISO)
#define ISO_UART_ENABLE(__BAUDRATE__) ISO_USART3_UART_Init(__BAUDRATE__)
#define ISO_TX_SET() HAL_GPIO_WritePin(ISO_UART_GPIO,ISO_UART_PIN_TX, GPIO_PIN_SET)
#define ISO_TX_RESET() HAL_GPIO_WritePin(ISO_UART_GPIO,ISO_UART_PIN_TX, GPIO_PIN_RESET)

/* User function prototype */
bool ODB2_Fast_Init(void);
bool ODB2_Slow_Init(void);
int ODB2_ISO_KWP_Init(void);
int ODB2_KWP_ProcessBleRequest(void);
int ODB2_KWP_CheckForKeepAlive(void);
int ODB2_KWP_KeepAlive(void);

/* Private function */
static bool kwp_send_mode_pid(uint8_t u8Mode, uint8_t pid, uint8_t u8PIDBytes);
static uint8_t kwp_CalculateCRC(uint8_t* pu8Data, uint8_t u8Len);
static bool check_kwp_crc(uint8_t* pu8Data, uint8_t u8Len);
static bool check_kwp_full_crc(uint8_t* pu8Data, uint8_t u8Len);

int ODB2_Toyota_ProcessBleRequest(void);

//bool ODB2_Toyota_Fast_Init(void);
uint8_t ODB_Toyota_SendReceive(uint8_t ECU_Phisical_Address,uint8_t* pu8TxBuff, uint8_t u8TxLen, uint8_t* pu8RxBuff);
bool ODB2_Toyota_Fast_Init_Custom(uint8_t lowTime, uint8_t highTime, uint8_t ECU_Phisical_Address, int baudrate);
void GPIO_KlinePinInit(void);
void ToyotaDtcTxMessageProcess(uint8_t ECU_Phisical_Address, uint8_t* pu8RxBuff, uint8_t u8RxLen);
bool toyota_send(uint8_t* pu8Data, uint8_t u8Len);
void KWP_Reinit(void);
void ToyotaDtcTxMessageProcess_noQuantity(uint8_t ECU_Phisical_Address, uint8_t* pu8RxBuff, uint8_t u8RxLen);
static bool	Toyota_send_mode_pid(uint8_t u8Mode, uint8_t pid, uint8_t u8PIDBytes);
bool checkToyotaMode01(void);
//void KWP_UART_BAUDRATE_CHANGE()

extern bool bKWP_Alive;
extern uint8_t KB1, KB2;
#endif // KWP_H
