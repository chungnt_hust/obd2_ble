#include "stm32f3xx_hal.h"
#include "cmsis_os.h"
#include "utils.h"
#include "protocol.h"

typedef enum _PWM_MODE {
	PWM_MODE_SENDING_DATA,
	PWM_MODE_SENDING_IFR,
	PWM_MODE_RECEIVING,
	PWM_MODE_RECEIVING_IFR,
	PWM_MODE_RECEIVING_SOF,
	PWM_MODE_RECEIVING_DATA,
	PWM_MODE_RECEIVING_ERROR,
	PWM_MODE_SENDING_IFR_ERROR,
	PWM_MODE_SENDING_DATA_ERROR,
} PWM_MODE;


#define PWM_BUS_PASSIVE 0
#define PWM_BUS_ACTIVE 1


#define TxTIMER htim4
#define RxTIMER htim3
#define EOFTIMER htim4

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;

#define PWM_Tx_TIMER htim4
#define PWM_Rx_TIMER htim3
#define PWM_ERR_TIMER htim4

#define PWM_TX_TIM_OC_CHANNEL TIM_CHANNEL_2
#define PWM_RX_TIM_IC_RISING_CHANEL TIM_CHANNEL_3
#define PWM_RX_TIM_IC_FALLING_CHANEL TIM_CHANNEL_4


//typedef enum _PWM_TIMEOUT {
//    TP1_TX_NOM  =   8,
//    TP2_TX_NOM  =  16,
//    TP3_TX_NOM  =  24,
//    TP4_TX_NOM  =  46,
//    TP5_TX_MIN  =  70,
//    TP6_TX_NOM  =  96,
//    TP7_TX_NOM  =  32,
//    TP8_TX_NOM  =  40,
//    TP9_TX_NOM  = 120,
//    TP1_RX_MIN  = 4,
//    TP1_RX_MAX  =  9,
//    TP2_RX_MIN  =  10,
//    TP2_RX_MAX  =  19,
//    TP3_RX_MAX  =  27,
//    TP4_RX_MIN  =  46, 
//    TP4_RX_MAX  =  63,
//    TP7_RX_MIN  =  30,
//    TP7_RX_MAX  =  35,
//    TP8_RX_MAX  =  43
//} PWM_TIMEOUT;

#define    TP1_TX_NOM  7		//8
#define    TP2_TX_NOM  16		//16
#define    TP3_TX_NOM  23		//24
#define    TP4_TX_NOM  49		//46
#define    TP5_TX_MIN  70
#define    TP6_TX_NOM  96
#define    TP7_TX_NOM  34		//32
#define    TP8_TX_NOM  40
#define    TP9_TX_NOM  120
#define    TP1_RX_MIN  4
#define    TP1_RX_MAX  9
#define    TP2_RX_MIN  10
#define    TP2_RX_MAX  19
#define    TP3_RX_MAX  27
#define    TP4_RX_MIN  46 
#define    TP4_RX_MAX  63
#define    TP7_RX_MIN  30
#define    TP7_RX_MAX  35
#define    TP8_RX_MAX  43

typedef enum _PWM_SEND {
	PWM_SEND_OK,
	PWM_SEND_NOK,
	PWM_SEND_OVER_WRITE,
	PWM_SEND_BUT_NO_ACK,
	PWM_SEND_BUT_ACK_WRONG
} PWM_SEND;

typedef enum _PWM_RECEIVE {
	PWM_RECEIVE_OK,
	PWM_RECEIVE_NOTHING,
	PWM_RECEIVE_WRONG_CRC,
	PWM_RECEIVE_NOK
} PWM_RECEIVE;

#define PWM_EOF_BIT_VAL 0xFF
#define PWM_SOF_BIT_VAL 0xFE
#define PWM_EOD_BIT_VAL 0xFD
#define PWM_ERR_BIT_VAL 0xFC // unknown

#define J1850_RX_MAX_COUNT 1700
#define J1850_IFR_START_PULSE 1
#define J1850_EOD_TIMEOUT TP4_RX_MIN
#define J1850_MAX_RX_FRAME_COUNT 4

// from .c
extern PWM_MODE pwm_mode;
extern int pwm_bus_status;
// Tx variables
extern uint16_t pwm_u16TxPulseCount;
extern uint16_t pwm_u16TxPulseSent;
extern uint8_t  pwm_u8TxByteBuf[12];//={0x61,0x6A,0xF1,0x01,0x00};
extern uint8_t* pwm_pu8TxPulseBuf;
// other variable

extern uint16_t u16PulseToSAE[255];
// rx buffer
extern uint16_t pwm_u16RxPulseCount;
extern uint16_t pwm_u16RxIFRPulseCount;
extern uint16_t u8j1850_Rx_Frame_Count;
extern uint16_t u8j1850_Rx_Start_Byte[J1850_MAX_RX_FRAME_COUNT + 1];
extern uint16_t u8j1850_Rx_Stop_Byte[J1850_MAX_RX_FRAME_COUNT + 1];
extern uint8_t j1850_RX_Pulse_Buf[1700];
//extern uint8_t j1850_Rx_Bits_Buf[1700];
extern uint8_t j1850_RX_Byte_Buf[255];

extern void inline pwm_set_output_compare_pulse(uint16_t u16Pulse);
extern void inline pwm_init_output_compare(void);
extern void inline pwm_start_output_compare(void);
extern void inline pwm_stop_output_compare(void);
extern void inline pwm_start_input_capture(void);
extern void inline pwm_stop_input_capture(void);
extern void inline j1850_pwm_output_compare_handler(void);
extern void inline j1850_pwm_input_capture_rising_handler(void);
extern void inline j1850_pwm_input_capture_falling_handler(void);
extern inline void j1850_pwm_start_sending(uint16_t u16FirstPulse, uint8_t* pu8PulseBuf, uint16_t u16BitCount, bool isFrame);
void pwm_start_sending_ifr(void);

uint16_t pwm_calculate_pulse(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf);
uint16_t pwm_calculate_pulse_with_sof(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf);
void pwm_calculate_pulse_byte(uint8_t u8Data, uint8_t *pu8Buf);
void pwm_bit_to_pulse(uint8_t BitValue, uint8_t* pu8Buf);

uint16_t pwm_get_input_capture_pulse(bool rising);
inline void pwm_reset_input_capture_counter(void);
void pwm_start_err_timer(uint16_t u16Timeout);
void pwm_stop_err_timer(void);
uint8_t pwm_calculate_bit_value1(uint16_t u16Pulse1);
uint8_t pwm_calculate_bit_value(uint16_t u16Pulse1, uint16_t u16Pulse2);
static uint8_t pwm_bits_to_bytes(uint8_t* pu8Value);
static void pwm_setPIDMode(uint8_t u8Mode, uint8_t u8Pid);
extern inline void j1850_pwm_err_timer_handler(void);

void j1850_pwm_transmit(uint8_t mode, uint8_t pid, uint8_t u8PidBytes);
void j1850_pwm_setup(void);
void j1850_pwm_start_receive(void);
void j1850_pwm_stop_receive(void);
void j1850_pwm_scan(void);
void j1850_pwm_print_rx_pulse(void);
uint16_t j1850_pwm_process_rx_task(void);

void brutalPwmInit(void);
void brutalPwmDeinit(void);
PWM_SEND pwm_send_cmd(uint8_t u8mode, uint8_t u8pid);
PWM_SEND pwm_send(uint8_t* pu8PulseBuff, uint16_t u16PulseCount);
PWM_SEND j1850_pwm_brutal_transmit(uint8_t u8Mode, uint8_t u8Pid, uint8_t u8PidBytes);

PWM_RECEIVE pwm_read_pulse(void);
void pwm_send_ack(void);
uint8_t pwm_pulse_to_byte(uint8_t* pu8PusleBuf);
void pwm_read_byte(void);
uint16_t pwm_rx_byte_number(void);

