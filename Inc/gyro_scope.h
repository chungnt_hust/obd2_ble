/*
 * gyro_scope.h
 *
 *  Created on: Feb 19, 2016
 *      Author: Sina Darvishi
 */

#ifndef DRIVERS_MYLIB_GYROSCOPE_H_
#define DRIVERS_MYLIB_GYROSCOPE_H_

/*
 C++ detection
#ifdef __cplusplus
extern "C" {
#endif
*/

#include "stm32f3xx_hal.h"

/*
 * Debug MPU enable
 */
#define DB_EN 1

/**
 * @defgroup GYROSENSOR_Macros
 * @brief    Library defines
 * @{
 */

/* Default I2C clock */
#ifndef GYROSENSOR_I2C_CLOCK
#define GYROSENSOR_I2C_CLOCK              400000            /*!< Default I2C clock speed */
#endif

/**
 * @brief  Data rates predefined constants
 * @{
 */
#define GYROSENSOR_DataRate_8KHz       0   /*!< Sample rate set to 8 kHz */
#define GYROSENSOR_DataRate_4KHz       1   /*!< Sample rate set to 4 kHz */
#define GYROSENSOR_DataRate_2KHz       3   /*!< Sample rate set to 2 kHz */
#define GYROSENSOR_DataRate_1KHz       7   /*!< Sample rate set to 1 kHz */
#define GYROSENSOR_DataRate_500Hz      15  /*!< Sample rate set to 500 Hz */
#define GYROSENSOR_DataRate_250Hz      31  /*!< Sample rate set to 250 Hz */
#define GYROSENSOR_DataRate_125Hz      63  /*!< Sample rate set to 125 Hz */
#define GYROSENSOR_DataRate_100Hz      79  /*!< Sample rate set to 100 Hz */
/**
 * @}
 */

/**
 * @}
 */

/**
 * @defgroup GYROSENSOR_Typedefs
 * @brief    Library Typedefs
 * @{
 */

/**
 * @brief  GYROSENSOR can have 2 different slave addresses, depends on it's input AD0 pin
 *         This feature allows you to use 2 different sensors with this library at the same time
 */
typedef enum  {
	GYROSENSOR_Device_0 = 0x00, /*!< AD0 pin is set to low */
	GYROSENSOR_Device_1 = 0x02  /*!< AD0 pin is set to high */
} GYROSENSOR_Device;

/**
 * @brief  GYROSENSOR result enumeration
 */
typedef enum  {
	GYROSENSOR_Result_Ok = 0x00,          /*!< Everything OK */
	GYROSENSOR_Result_Error,              /*!< Unknown error */
	GYROSENSOR_Result_DeviceNotConnected, /*!< There is no device with valid slave address */
	GYROSENSOR_Result_DeviceInvalid       /*!< Connected device with address is not GYROSENSOR */
} GYROSENSOR_Result;

/**
 * @brief  Parameters for accelerometer range
 */
typedef enum  {
	GYROSENSOR_Accelerometer_2G = 0x00, /*!< Range is +- 2G */
	GYROSENSOR_Accelerometer_4G = 0x01, /*!< Range is +- 4G */
	GYROSENSOR_Accelerometer_8G = 0x02, /*!< Range is +- 8G */
	GYROSENSOR_Accelerometer_16G = 0x03 /*!< Range is +- 16G */
} GYROSENSOR_Accelerometer;

/**
 * @brief  Parameters for gyroscope range
 */
typedef enum {
	GYROSENSOR_Gyroscope_250s = 0x00,  /*!< Range is +- 250 degrees/s */
	GYROSENSOR_Gyroscope_500s = 0x01,  /*!< Range is +- 500 degrees/s */
	GYROSENSOR_Gyroscope_1000s = 0x02, /*!< Range is +- 1000 degrees/s */
	GYROSENSOR_Gyroscope_2000s = 0x03  /*!< Range is +- 2000 degrees/s */
} GYROSENSOR_Gyroscope;

/**
 * @brief  Main GYROSENSOR structure
 */
typedef struct  {
	/* Private */
	uint8_t Address;         /*!< I2C address of device. */
	float Gyro_Mult;         /*!< Gyroscope corrector from raw data to "degrees/s". Only for private use */
	float Acce_Mult;         /*!< Accelerometer corrector from raw data to "g". Only for private use */
	/* Public */
	int16_t Accelerometer_X; /*!< Accelerometer value X axis */
	int16_t Accelerometer_Y; /*!< Accelerometer value Y axis */
	int16_t Accelerometer_Z; /*!< Accelerometer value Z axis */
	int16_t Gyroscope_X;     /*!< Gyroscope value X axis */
	int16_t Gyroscope_Y;     /*!< Gyroscope value Y axis */
	int16_t Gyroscope_Z;     /*!< Gyroscope value Z axis */
	float   Temperature;     /*!< Temperature in degrees */
	int16_t accelXoffs;			 /*!< Accelerometer offset value X axis */
	int16_t accelYoffs;      /*!< Accelerometer offset value Y axis */
	int16_t accelZoffs;			 /*!< Accelerometer offset value Z axis */
	int16_t gyrXoffs;				 /*!< Gyroscope offset value X axis */
	int16_t gyrYoffs;				 /*!< Gyroscope offset value Y axis */
	int16_t gyrZoffs;				 /*!< Gyroscope offset value Z axis */
	float accX;							 /*!< Accelerometer data X axis "g" */
	float accY;              /*!< Accelerometer data Y axis "g" */
	float accZ;              /*!< Accelerometer data Z axis "g" */
	float gyrX;							 /*!< Gyroscope data X axis "degrees/s" */
	float gyrY;              /*!< Gyroscope data Y axis  "degrees/s" */
	float gyrZ;              /*!< Gyroscope data Z axis  "degrees/s" */
	double angX;						 /*!< Angle data X axis  "degrees" */
	double angY;						 /*!< Angle data Y axis  "degrees" */
	double angZ;						 /*!< Angle data Z axis  "degrees" */

	double offsAngX;						 /*!< Angle data X axis  "degrees" */
	double offsAngY;						 /*!< Angle data Y axis  "degrees" */
	double offsAngZ;						 /*!< Angle data Z axis  "degrees" */
	double rollCalib;
	double pitchCalib;

	int32_t newAccX;
	int32_t newAccY;
	int32_t newAccZ;

	int32_t firstAccX;
	int32_t firstAccY;
	int32_t firstAccZ;
	//I2C_HandleTypeDef* I2Cx;
} GYROSENSOR;

/**
 * @brief  Interrupts union and structure
 */
typedef union {
	struct {
		uint8_t DataReady:1;       /*!< Data ready interrupt */
		uint8_t reserved2:2;       /*!< Reserved bits */
		uint8_t Master:1;          /*!< Master interrupt. Not enabled with library */
		uint8_t FifoOverflow:1;    /*!< FIFO overflow interrupt. Not enabled with library */
		uint8_t reserved1:1;       /*!< Reserved bit */
		uint8_t MotionDetection:1; /*!< Motion detected interrupt */
		uint8_t reserved0:1;       /*!< Reserved bit */
	} F;
	uint8_t Status;
} GYROSENSOR_Interrupt;


/**
 * @}
 */

/**
 * @defgroup GYROSENSOR_Functions
 * @brief    Library Functions
 * @{
 */

/**
 * @brief  Initializes GYROSENSOR and I2C peripheral
 * @param  *DataStruct: Pointer to empty @ref GYROSENSOR_t structure
 * @param  DeviceNumber: GYROSENSOR has one pin, AD0 which can be used to set address of device.
 *          This feature allows you to use 2 different sensors on the same board with same library.
 *          If you set AD0 pin to low, then this parameter should be GYROSENSOR_Device_0,
 *          but if AD0 pin is high, then you should use GYROSENSOR_Device_1
 *
 *          Parameter can be a value of @ref GYROSENSOR_Device_t enumeration
 * @param  AccelerometerSensitivity: Set accelerometer sensitivity. This parameter can be a value of @ref GYROSENSOR_Accelerometer_t enumeration
 * @param  GyroscopeSensitivity: Set gyroscope sensitivity. This parameter can be a value of @ref GYROSENSOR_Gyroscope_t enumeration
 * @retval Initialization status:
 *            - GYROSENSOR_Result_t: Everything OK
 *            - Other member: in other cases
 */
GYROSENSOR_Result GYROSENSOR_Init(GYROSENSOR* DataStruct, GYROSENSOR_Device DeviceNumber, GYROSENSOR_Accelerometer AccelerometerSensitivity, GYROSENSOR_Gyroscope GyroscopeSensitivity);

/**
 * @brief  Sets gyroscope sensitivity
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure indicating GYROSENSOR device
 * @param  GyroscopeSensitivity: Gyro sensitivity value. This parameter can be a value of @ref GYROSENSOR_Gyroscope_t enumeration
 * @retval Member of @ref GYROSENSOR_Result_t enumeration
 */
GYROSENSOR_Result GYROSENSOR_SetGyroscope(GYROSENSOR* DataStruct, GYROSENSOR_Gyroscope GyroscopeSensitivity);

/**
 * @brief  Sets accelerometer sensitivity
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure indicating GYROSENSOR device
 * @param  AccelerometerSensitivity: Gyro sensitivity value. This parameter can be a value of @ref GYROSENSOR_Accelerometer_t enumeration
 * @retval Member of @ref GYROSENSOR_Result_t enumeration
 */
GYROSENSOR_Result GYROSENSOR_SetAccelerometer(GYROSENSOR* DataStruct, GYROSENSOR_Accelerometer AccelerometerSensitivity);

/**
 * @brief  Sets output data rate
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure indicating GYROSENSOR device
 * @param  rate: Data rate value. An 8-bit value for prescaler value
 * @retval Member of @ref GYROSENSOR_Result_t enumeration
 */
GYROSENSOR_Result GYROSENSOR_SetDataRate(GYROSENSOR* DataStruct, uint8_t rate);

/**
 * @brief  Sets Digital Low Pass Filter (DLPF) for both the gyroscopes and accelerometers.
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure indicating GYROSENSOR device
 * @param  rate: Data rate value. An 8-bit value for prescaler value
 * @retval Member of @ref GYROSENSOR_Result_t enumeration
 */
GYROSENSOR_Result GYROSENSOR_configLPFilter(GYROSENSOR* DataStruct);

/**
 * @brief  Enables interrupts
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure indicating GYROSENSOR device
 * @retval Member of @ref GYROSENSOR_Result_t enumeration
 */
GYROSENSOR_Result GYROSENSOR_EnableInterrupts(GYROSENSOR* DataStruct);

/**
 * @brief  Disables interrupts
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure indicating GYROSENSOR device
 * @retval Member of @ref GYROSENSOR_Result_t enumeration
 */
GYROSENSOR_Result GYROSENSOR_DisableInterrupts(GYROSENSOR* DataStruct);

/**
 * @brief  Reads and clears interrupts
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure indicating GYROSENSOR device
 * @param  *InterruptsStruct: Pointer to @ref GYROSENSOR_Interrupt_t structure to store status in
 * @retval Member of @ref GYROSENSOR_Result_t enumeration
 */
GYROSENSOR_Result GYROSENSOR_ReadInterrupts(GYROSENSOR* DataStruct, GYROSENSOR_Interrupt* InterruptsStruct);

/**
 * @brief  Reads accelerometer data from sensor
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_ReadAccelerometer(GYROSENSOR* DataStruct);

/**
 * @brief  Reads gyroscope data from sensor
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_ReadGyroscope(GYROSENSOR* DataStruct);

/**
 * @brief  Reads temperature data from sensor
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_ReadTemperature(GYROSENSOR* DataStruct);

/**
 * @brief  Reads accelerometer, gyroscope and temperature data from sensor
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_ReadAll(GYROSENSOR* DataStruct);

/**
 * @brief  Convert accelerometer data to gravitation 
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_ProcessAccelerometer(GYROSENSOR* DataStruct);

/**
 * @brief  Convert gyroscope data to rotation 
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_ProcessGyroscope(GYROSENSOR* DataStruct);

/**
 * @brief  Convert all data
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_ProcessAll(GYROSENSOR* DataStruct);

/**
 * @brief  Set offset Accelerometer X
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @param  offset: Offset value 
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_SetOffsAccelerometer_X(GYROSENSOR* DataStruct, int16_t offset);

/**
 * @brief  Set offset Accelerometer Y
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @param  offset: Offset value 
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_SetOffsAccelerometer_Y(GYROSENSOR* DataStruct, int16_t offset);

/**
 * @brief  Set offset Accelerometer Z
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @param  offset: Offset value
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_SetOffsAccelerometer_Z(GYROSENSOR* DataStruct, int16_t offset);

/**
 * @brief  Set offset Accelerometer 
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @param  offsetX, Y, Z: Offset value
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_SetOffsAccelerometer(GYROSENSOR* DataStruct, int16_t offsetX, int16_t offsetY, int16_t offsetZ);

/**
 * @brief  Set offset Gyroscope X
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @param  offset: Offset value
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_SetOffsGyroscope_X(GYROSENSOR* DataStruct, int16_t offset);

/**
 * @brief  Set offset Gyroscope Y
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @param  offset: Offset value
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_SetOffsGyroscope_Y(GYROSENSOR* DataStruct, int16_t offset);

/**
 * @brief  Set offset Gyroscope Z
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @param  offset: Offset value
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_SetOffsGyroscope_Z(GYROSENSOR* DataStruct, int16_t offset);

/**
 * @brief  Set offset Gyroscope 
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @param  offsetX, Y, Z: Offset value
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_SetOffsGyroscope(GYROSENSOR* DataStruct, int16_t offsetX, int16_t offsetY, int16_t offsetZ);

/**
 * @brief  Update angle in 3-Axis 
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_Update(GYROSENSOR* DataStruct);

/**
 * @brief  Update Z angle 
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_Mini_Update(GYROSENSOR* DataStruct);

/**
 * @brief  calibrate gyroscope offset
 * @param  *DataStruct: Pointer to @ref GYROSENSOR_t structure to store data to
 * @retval Member of @ref GYROSENSOR_Result_t:
 *            - GYROSENSOR_Result_Ok: everything is OK
 *            - Other: in other cases
 */
GYROSENSOR_Result GYROSENSOR_DeviceCalibration(GYROSENSOR* DataStruct);
GYROSENSOR_Result GYROSENSOR_UpdateRotation(GYROSENSOR* DataStruct);
GYROSENSOR_Result GYROSENSOR_DetectDanger(GYROSENSOR* DataStruct);
/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */


#endif /* DRIVERS_MYLIB_GYROSCOPE_H_ */
