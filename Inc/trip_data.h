#ifndef __TRIP_DATA_H
#define __TRIP_DATA_H
#include "embedded_flash.h"

/*
||==========||=======||================||=============||===============||===========||=======||============||=========||
|| sys time || state || empty distance || pickup time || trip distance || wait time || speed || trip index || active  ||
||==========||=======||================||=============||===============||===========||=======||============||=========||
||    6B    ||   1B  ||       2B       ||     6B      ||      4B       ||     4B    ||   1B  ||    2B      ||  1B     ||
||==========||=======||================||=============||===============||===========||=======||============||=========||
*/
/*
||====||====||====||====||=====||=====||
|| yy || mm || dd || hh || min || sec ||
||====||====||====||====||=====||=====||
|| 1B || 1B || 1B || 1B || 1B  || 1B  ||
||====||====||====||====||=====||=====||
*/
#define TRIP_SYS_TIME_YEAR			0
#define TRIP_SYS_TIME_MONTH			1
#define TRIP_SIS_TIME_DAY				2
#define TRIP_SIS_TIME_HOUR			3
#define TRIP_SIS_TIME_MIN				4
#define TRIP_SIS_TIME_SEC				5
#define TRIP_STATE							6
#define TRIP_EMTY_DISTANCE			7
#define TRIP_PICKUP_TIME_YEAR		9
#define TRIP_PICKUP_TIME_MONTH	10
#define TRIP_PICKUP_TIME_DAY		11
#define TRIP_PICKUP_TIME_HOUR		12
#define TRIP_PICKUP_TIME_MIN		13
#define TRIP_PICKUP_TIME_SEC		14
#define TRIP_DISTANCE						15
#define TRIP_WAIT_TIME					19
#define TRIP_SPEED							23
#define TRIP_INDEX							24
#define TRIP_ACTIVE							26

#define ACTIVE			1
#define NOT_ACTIVE	0

#define TRIP_BUFFER_SIZE				27

extern uint8_t u8TripActive;
extern uint8_t	u16TripIndex;

void TRIP_Init(void);
void TRIP_SaveData(void);
void TRIP_ReadData(void);

#endif
