#ifndef __J1850_PWM_BRUTAL_H
#define __J1850_PWM_BRUTAL_H
#include "stm32f3xx_hal.h"
#include "brutal_timing.h"

#define ENALBE_PWM() 			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET)
#define ENALBE_VPW() 			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET)
#define PWM_OUT_HIGH()		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET)
#define PWM_OUT_LOW()			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_RESET)

#define    TP1_TX_NOM  6		//8
#define    TP2_TX_NOM  14		//16
#define    TP3_TX_NOM  24
#define    TP4_TX_NOM  50		//46
#define    TP5_TX_MIN  70
#define    TP6_TX_NOM  96
#define    TP7_TX_NOM  34		//32
#define    TP8_TX_NOM  40
#define    TP9_TX_NOM  120
#define    TP1_RX_MIN  4
#define    TP1_RX_MAX  9
#define    TP2_RX_MIN  10
#define    TP2_RX_MAX  19
#define    TP3_RX_MAX  27
#define    TP4_RX_MIN  46 
#define    TP4_RX_MAX  63
#define    TP7_RX_MIN  30
#define    TP7_RX_MAX  35
#define    TP8_RX_MAX  43

typedef enum _PWM_SEND {
	PWM_SEND_OK,
	PWM_SEND_NOK,
	PWM_SEND_OVER_WRITE,
	PWM_SEND_BUT_NO_ACK,
	PWM_SEND_BUT_ACK_WRONG
} PWM_SEND;

typedef enum _PWM_RECEIVE {
	PWM_RECEIVE_OK,
	PWM_RECEIVE_NOTHING,
	PWM_RECEIVE_WRONG_CRC,
	PWM_RECEIVE_WRONG
} PWM_RECEIVE;

void brutalPwmInit(void);
uint16_t pwm_calculate_pulse_with_sof(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf);
uint16_t pwm_calculate_pulse(uint8_t* pu8Data,uint16_t u16Leng, uint8_t* pu8PulseBuf);
void pwm_calculate_pulse_byte(uint8_t u8Data, uint8_t *pu8Buf);
void pwm_bit_to_pulse(uint8_t BitValue, uint8_t* pu8Buf);
PWM_SEND pwm_send_cmd(uint8_t u8mode, uint8_t u8pid);
uint8_t j1850_crc(uint8_t *msg_buf, int8_t nbytes);
PWM_SEND pwm_send(uint8_t* pu8PulseBuff, uint16_t u16PulseCount);
void pwm_communication(uint8_t u8mode, uint8_t u8pid);
PWM_RECEIVE pwm_read(void);
void pwm_send_ack(void);
uint8_t pwm_pulse_to_byte(uint8_t* pu8PusleBuf);
#endif
