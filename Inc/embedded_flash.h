#ifndef __EMBEDDED_FLASH_H
#define __EMBEDDED_FLASH_H

#include "stm32f3xx_hal.h"

/** 
  * @brief EFLASH Status  
  */ 
typedef enum { 
	EFLASH_OK,
	EFLASH_EMPTY,
	EFLASH_LEN_ERROR,
	EFLASH_WRITE_ERROR,
	EFLASH_ERASE_ERROR,
}EFLASH_Status;

void EF_Init(void);
uint8_t EF_WriteVariable(const uint16_t* Data, uint8_t Len);
uint8_t EF_ReadVariable(uint16_t* Data, uint8_t Len);

#endif
